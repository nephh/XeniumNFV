package com.ahghorab.xenonet.service;

import com.ahghorab.xenonet.domain.NetworkCard;
import com.ahghorab.xenonet.domain.Server;
import com.ahghorab.xenonet.service.dto.MustKeepTopology;
import com.ahghorab.xenonet.service.dto.TopologyTrinityStats;
import com.ahghorab.xenonet.service.util.TopologyDif;
import com.ahghorab.xenonet.service.util.ovsManagementCommand.*;
import com.ahghorab.xenonet.service.util.ssh.ExecuteCommanOverSsh;
import com.ahghorab.xenonet.web.rest.vm.*;
import com.ahghorab.xenonet.web.rest.wrapper.ServerIdIp;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class NetworkDiagramService {
    private final Logger log = LoggerFactory.getLogger(NetworkDiagramService.class);
    private DockerClient dockerClient;
    DefaultDockerClientConfig config;
    private final ServerService serverService;
    private String serverPrimeIp = "";
    private Server currentServer = null;
    private Map<String, TopologyVM> AllCurrenttopologiesMap = new HashMap<>();

    public NetworkDiagramService(ServerService serverService) {
        this.serverService = serverService;
    }

    // the input arg is => Map<ServerId(string) , ServerIp(string)>
    public void registerServersToInventory(List<ServerIdIp> serversIdIp) {
//        serversIdIp.forEach(idIp -> {
//            registerServerToOVSDbInventory(idIp.getServerId(), idIp.getServerPrimeIp());
//        });
        initDiagrams();
    }
    public void initDiagrams(){
        this.initiAllCurrentTopology();
    }

    public Integer parsTopology(TopologyVM topologyVM) {
        /*
        Create Docker Client by Finding ip address of given serverId
         */
        Optional<Server> server = this.serverService.getServerById(Long.parseLong(topologyVM.getServerId()));
        if (server.isPresent()) {
            this.currentServer = server.get();
            for (NetworkCard nic : server.get().getNetworkCards()) {
                if (nic.getIsPrimary().equals("1")) {
                    this.serverPrimeIp = nic.getIpAddress();
                    break;
                }
            }
        }
        if (this.serverPrimeIp.isEmpty()) {
            return -1;
        }
        if (this.currentServer == null) {
            return -1;
        }
        this.config
            = DefaultDockerClientConfig.createDefaultConfigBuilder()
            .withRegistryEmail("he.amirhossein@gmail.com")
            .withDockerHost("tcp://" + this.serverPrimeIp + ":2375").build();

        this.dockerClient = DockerClientBuilder.getInstance(this.config).build();
        /*
         * For Now All Groups Entity That received from ui is null coz we don't create
         * groups( that blue shape in the diagram ) on the server so for now we don't need that
         * but notice that all entities like vnf or switches or controller has a "GROUP" key part on
         * its own but topology groups[] is an empty array. i set it empty on "getTopologyDif" class last parts
         */
        TopologyTrinityStats topologyTrinityStats = TopologyDif.getTopologyDif(this.AllCurrenttopologiesMap.get(topologyVM.getServerId()), topologyVM);
        this.AllCurrenttopologiesMap.put(topologyVM.getServerId(), topologyVM);
        deleteTopology(topologyTrinityStats.getMustDeleteTopo());
        createTopology(topologyTrinityStats.getMustCreateTopo());
        updateTopology(topologyTrinityStats.getMustUpdateKeepTopo());
        return 1;
    }

    private TopologyVM createTopology(TopologyVM topology) {
        if (topology != null) {
            //Create Switches
            for (SwitchVM swt : topology.getSwitches()) {
                createBridge(
                    "ovsdb://HOST" + topology.getServerId(),
                    swt.getDescribtion().split("#")[1],
                    swt.getDescribtion().split("#")[1],
                    swt.getControllerIp(),
                    swt.getControllerPort(),
                    "ovsdb:ovsdb-bridge-protocol-openflow-13"
                );
            }
            //Create VNF
            for (VnfVM vnf : topology.getVnfs()) {
                createDockerVnf(
                    vnf.getImgName(),
                    vnf.getDescribtion().split("#")[1],
                    vnf.getExecutionCommand(),
                    vnf.getCpu(),
                    vnf.getRam(),
                    vnf.getIsDedicatedRes()
                );
            }
            //Create PORTS
            for (DiagramEntityConnectionsVM port : topology.getDiagramEntityConnections()) {

                //Create VNF <-> OVS port
                // VNF#XXXXXXXXX%Switch#XXXXXXXXX
                if ((port.getKey().split("%")[0].split("#")[0].equals("Switch") && port.getKey().split("%")[1].split("#")[0].equals("VNF")) ||
                    (port.getKey().split("%")[1].split("#")[0].equals("Switch") && port.getKey().split("%")[0].split("#")[0].equals("VNF"))) {
                    String vnfContainerName, switchName, vnfContainerInterfaceName, vnfContainerNicIp = null;
                    if (port.getKey().split("%")[0].split("#")[0].equals("Switch")) {
                        switchName = port.getKey().split("%")[0].split("#")[1].split("-")[1];
                        vnfContainerName = port.getKey().split("%")[1].split("#")[1].split("-")[1];
                        /*
                        Switch#XXXXXXXXX%VNF#XXXXXXXXX
                        always -> element.from + '%' + element.to
                        so VNF interface name is "element.toPort" = destination Interface
                         */
                        vnfContainerInterfaceName = port.getDestInterfaceName();

                    } else {
                        vnfContainerName = port.getKey().split("%")[0].split("#")[1].split("-")[1];
                        switchName = port.getKey().split("%")[1].split("#")[1].split("-")[1];
                        /*
                        VNF#XXXXXXXXX%Switch#XXXXXXXXX
                        always -> element.from + '%' + element.to
                        so VNF interface name is "element.fromPort" = source Interface
                         */
                        vnfContainerInterfaceName = port.getSrcInterfaceName();
                    }
                    // Loop for finding VNF IP address
                    /*
                    Assume you change topology after creation (delete one VNF<->Switch port and create it again..
                    in this case the "mustCreateTopology) has no VNF list, it has just one "DiagramConnection" entry,
                    in this case below "FOR Loop" does'nt work, so in this specific case (it happen just here because we
                    need IP ADDRESS of the vnf NIC (it never happen in "deleteDockerVnfOvsPort" coz in there we don't need
                    the IP Address"; So here we need to loop Over "CurrentTopology" instead of "MustCreateTopology"
                     */
                    for (VnfVM vnf : this.AllCurrenttopologiesMap.get(topology.getServerId()).getVnfs()) {
                        if (vnf.getDescribtion().split("#")[1].equals(vnfContainerName)) {
                            // Find IP address for specific interface name
                            if (vnfContainerNicIp == null) {
                                for (NodePortVM nodePort : vnf.getLeftArray()) {
                                    if (nodePort.getName().equals(vnfContainerInterfaceName)) {
                                        vnfContainerNicIp = nodePort.getIpAddress();
                                        break;
                                    }
                                }
                            }
                            if (vnfContainerNicIp == null) {
                                for (NodePortVM nodePort : vnf.getTopArray()) {
                                    if (nodePort.getName().equals(vnfContainerInterfaceName)) {
                                        vnfContainerNicIp = nodePort.getIpAddress();
                                        break;
                                    }
                                }
                            }
                            if (vnfContainerNicIp == null) {
                                for (NodePortVM nodePort : vnf.getBottomArray()) {
                                    if (nodePort.getName().equals(vnfContainerInterfaceName)) {
                                        vnfContainerNicIp = nodePort.getIpAddress();
                                        break;
                                    }
                                }
                            }
                            if (vnfContainerNicIp == null) {
                                for (NodePortVM nodePort : vnf.getRightArray()) {
                                    if (nodePort.getName().equals(vnfContainerInterfaceName)) {
                                        vnfContainerNicIp = nodePort.getIpAddress();
                                        break;
                                    }
                                }
                            }
                            createDockerVnfOvsPort(
                                this.serverPrimeIp,
                                switchName,
                                vnfContainerName,
                                vnfContainerInterfaceName,
                                vnfContainerNicIp);
                        }
                    }
                }

                //Create OVS <-> OVS Ports
                //Find Port Type by Checking port key
                // Switch#XXXXXXXXX%SwitchXXXXXXXXX
                else if (port.getKey().split("%")[0].split("#")[0].equals("Switch") &&
                    port.getKey().split("%")[1].split("#")[0].equals("Switch")) {
                    //Path Port Naming is like this:
                    /*
                    PortKey = Switch#011535b9-6895-44f5-97cf-7ecf521a7eb9%Switch#2834659e-a02f-416d-ad35-e8880c672127
                    Switch1.key = Switch#011535b9-6895-44f5-97cf-7ecf521a7eb9
                    Switch2.key = Switch#2834659e-a02f-416d-ad35-e8880c672127
                    so path0 should be = 6895_a02f
                    and path1 should be = a02f_6895
                    so with this kind of naming strategy we can determine switch names and path ports from port key
                    and no extra loop needed.
                    */
                    createBridgePort(
                        "ovsdb://HOST" + topology.getServerId(),
                        port.getKey().split("%")[0].split("#")[1].split("-")[1],
                        port.getKey().split("%")[0].split("#")[1].split("-")[1] + "_" + port.getKey().split("%")[1].split("#")[1].split("-")[1],
                        "peer",
                        port.getKey().split("%")[1].split("#")[1].split("-")[1] + "_" + port.getKey().split("%")[0].split("#")[1].split("-")[1],
                        "ovsdb:interface-type-patch"
                    );
                    createBridgePort(
                        "ovsdb://HOST" + topology.getServerId(),
                        port.getKey().split("%")[1].split("#")[1].split("-")[1],
                        port.getKey().split("%")[1].split("#")[1].split("-")[1] + "_" + port.getKey().split("%")[0].split("#")[1].split("-")[1],
                        "peer",
                        port.getKey().split("%")[0].split("#")[1].split("-")[1] + "_" + port.getKey().split("%")[1].split("#")[1].split("-")[1],
                        "ovsdb:interface-type-patch"
                    );
                }
                //Find Port Type by Checking port key
                // Switch#XXXXXXXXX%ControllerXXXXXXXXX
                else if ((port.getKey().split("%")[0].split("#")[0].equals("Switch") && port.getKey().split("%")[1].split("#")[0].equals("Controller")) ||
                    (port.getKey().split("%")[1].split("#")[0].equals("Switch") && port.getKey().split("%")[0].split("#")[0].equals("Controller"))) {

                    //TODO do something abt controller <-> switches
                }
                //Find Port Type by Checking port key
                /*
                // Switch#XXXXXXXXX%MainServerInterfaceXXXXXXXXX
                its always like this coz we always going to the GRE Tunnel so
                "MainServerInterfaceXXXXXXXXX" is always second part (TOOOO) part
                {"from":"Switch#30230fcf-b775-4dfa-aee7-807d7e54438a", "to":"MainServerInterface#e88a61ee-2b84-4da3-85ed-dbe6b971668f", "fromPort":"eth0", "toPort":"1",
                "points":[455.73333740234375,116.433349609375,455.73333740234375,126.433349609375,277.3666687011719,126.433349609375,277.3666687011719,-23,149.5,-23,139.5,-23]}

                ** Entity Topo is Like This**
                destEntityKey: "MainServerInterface#e88a61ee-2b84-4da3-85ed-dbe6b971668f"
                destInterfaceName: "2"   <-----------------  *********  this is the GRE tunnel that we are looking for *********
                id: -1
                key: "Switch#ff6148b8-7ecd-4b8f-9fae-1e780d8d0f05%MainServerInterface#e88a61ee-2b84-4da3-85ed-dbe6b971668f"
                srcEntityKey: "Switch#ff6148b8-7ecd-4b8f-9fae-1e780d8d0f05"
                srcInterfaceName: "eth0"
                 */
                else if (port.getKey().split("%")[1].split("#")[0].equals("MainServerInterface") && port.getKey().split("%")[0].split("#")[0].equals("Switch")) {
                    /*
                    Find GRE tunnel remote-ip
                     */
                    String remoteIp = "";
                    // Loop for finding VNF IP address
                    /*
                    Assume you change topology after creation (delete one Switch<->MainInterface port and create it again..
                    in this case the "mustCreateTopology" has no MainInterface list, it has just one "DiagramConnection" entry,
                    in this case below "FOR Loop" does'nt work, so in this specific case we need to loop Over "CurrentTopology" instead of "MustCreateTopology"
                     */
                    for (MainServerInterfaceVM mainServerInterface : this.AllCurrenttopologiesMap.get(topology.getServerId()).getMainServerInterface()) {
                        if (mainServerInterface.getKey().equals(port.getKey().split("%")[1])) {
                            for (NodePortVM nodePort : mainServerInterface.getRightArray()) {
                                if (nodePort.getName().equals(port.getDestInterfaceName())) {
                                    remoteIp = nodePort.getIpAddress();
                                    break;
                                }
                            }
                        }
                        if (!remoteIp.equals("")) {
                            break;
                        }
                    }
                    createBridgePortGre(
                        "ovsdb://HOST" + topology.getServerId(),
                        port.getKey().split("%")[0].split("#")[1].split("-")[1],
                        port.getKey().split("%")[1].split("#")[1].split("-")[1] + "_" + port.getKey().split("%")[0].split("#")[1].split("-")[1],
                        "remote_ip",
                        remoteIp,
                        "key",
                        port.getDestInterfaceName(),
                        "ovsdb:interface-type-gre"
                    );
                }
            }

            //Create Controller
            for (ControllerVM controller : topology.getControllers()) {
                /*
                odl3 -> add flood mode
                odl4 -> set idel timeout to 10 from 300
                 */
                createOdlDocker("nephilimboy/odl:carbon0.6.3", controller.getDescribtion().split("#")[1]);
            }
        }

        return topology;
    }

    public void deleteTopology(TopologyVM topology) {
        if (topology != null) {
            for (DiagramEntityConnectionsVM port : topology.getDiagramEntityConnections()) {
                // VNF#XXXXXXXXX%Switch#XXXXXXXXX
                if ((port.getKey().split("%")[0].split("#")[0].equals("Switch") && port.getKey().split("%")[1].split("#")[0].equals("VNF")) ||
                    (port.getKey().split("%")[1].split("#")[0].equals("Switch") && port.getKey().split("%")[0].split("#")[0].equals("VNF"))) {
                    String vnfContainerName, switchName, vnfContainerInterfaceName;
                    if (port.getKey().split("%")[0].split("#")[0].equals("Switch")) {
                        switchName = port.getKey().split("%")[0].split("#")[1].split("-")[1];
                        vnfContainerName = port.getKey().split("%")[1].split("#")[1].split("-")[1];
                        /*
                        Switch#XXXXXXXXX%VNF#XXXXXXXXX
                        always -> element.from + '%' + element.to
                        so VNF interface name is "element.toPort" = destination Interface
                         */
                        vnfContainerInterfaceName = port.getDestInterfaceName();
                    } else {
                        vnfContainerName = port.getKey().split("%")[0].split("#")[1].split("-")[1];
                        switchName = port.getKey().split("%")[1].split("#")[1].split("-")[1];
                        /*
                        VNF#XXXXXXXXX%Switch#XXXXXXXXX
                        always -> element.from + '%' + element.to
                        so VNF interface name is "element.fromPort" = source Interface
                         */
                        vnfContainerInterfaceName = port.getSrcInterfaceName();
                    }
                    deleteDockerVnfOvsPort(
                        this.serverPrimeIp,
                        switchName,
                        vnfContainerName,
                        vnfContainerInterfaceName);
                }
                //Find Port Type by Checking port key
                // Switch#XXXXXXXXX%SwitchXXXXXXXXX
                else if (port.getKey().split("%")[0].split("#")[0].equals("Switch") &&
                    port.getKey().split("%")[1].split("#")[0].equals("Switch")) {
                    //Path Port Naming is like this:
                    /*
                    PortKey = Switch#011535b9-6895-44f5-97cf-7ecf521a7eb9%Switch#2834659e-a02f-416d-ad35-e8880c672127
                    Switch1.key = Switch#011535b9-6895-44f5-97cf-7ecf521a7eb9
                    Switch2.key = Switch#2834659e-a02f-416d-ad35-e8880c672127
                    so path0 should be = 6895_a02f
                    and path1 should be = a02f_6895
                    so with this kind of naming strategy we can determine switch names and path ports from port key
                    and no extra loop needed.
                    deleteBridgePort("ovsdb://HOST1", 6895, 6895_a02f)
                    deleteBridgePort("ovsdb://HOST1", a02f, a02f_6895)
                     */
                    deleteBridgePort(
                        "ovsdb://HOST" + topology.getServerId(),
                        port.getKey().split("%")[0].split("#")[1].split("-")[1],
                        port.getKey().split("%")[0].split("#")[1].split("-")[1] + "_" + port.getKey().split("%")[1].split("#")[1].split("-")[1]
                    );
                    deleteBridgePort(
                        "ovsdb://HOST" + topology.getServerId(),
                        port.getKey().split("%")[1].split("#")[1].split("-")[1],
                        port.getKey().split("%")[1].split("#")[1].split("-")[1] + "_" + port.getKey().split("%")[0].split("#")[1].split("-")[1]
                    );
                }
                //Find Port Type by Checking port key
                // Switch#XXXXXXXXX%ControllerXXXXXXXXX
                else if ((port.getKey().split("%")[0].split("#")[0].equals("Switch") && port.getKey().split("%")[1].split("#")[0].equals("Controller")) ||
                    (port.getKey().split("%")[1].split("#")[0].equals("Switch") && port.getKey().split("%")[0].split("#")[0].equals("Controller"))) {

                    //TODO do something abt controller <-> switches
                }
                // Switch#XXXXXXXXX%MainServerInterfaceXXXXXXXXX
                else if (port.getKey().split("%")[1].split("#")[0].equals("MainServerInterface") && port.getKey().split("%")[0].split("#")[0].equals("Switch")) {
                    /*
                    delete GRE tunnel is like delete normal bridge port
                     */
                    deleteBridgePort(
                        "ovsdb://HOST" + topology.getServerId(),
                        port.getKey().split("%")[0].split("#")[1].split("-")[1],
                        port.getKey().split("%")[1].split("#")[1].split("-")[1] + "_" + port.getKey().split("%")[0].split("#")[1].split("-")[1]
                    );
                }

            }
        }


        //VNF PART
        List<String> mustDeleteContainerVnf = new ArrayList<>();
        for (VnfVM vnf : topology.getVnfs()) {
            mustDeleteContainerVnf.add(vnf.getDescribtion().split("#")[1]);
        }
        //Delete docker container(VNF)
        deleteDockerVnf(mustDeleteContainerVnf);

        //Bridge Part
        //Delete bridges
        for (SwitchVM swt : topology.getSwitches()) {
            deleteBridge(
                "ovsdb://HOST" + topology.getServerId(),
                swt.getDescribtion().split("#")[1]
            );
        }

        // Controller Part
        //Delete Controller
        List<String> mustDeleteContainerController = new ArrayList<>();
        for (ControllerVM controller : topology.getControllers()) {
            mustDeleteContainerController.add(controller.getDescribtion().split("#")[1]);
        }
        //Delete Controller Docker container
        deleteDockerVnf(mustDeleteContainerController);

    }

    public Integer deleteTopology(String serverId) {
        /*
        Create Docker Client by Finding ip address of given serverId
         */
        Optional<Server> server = this.serverService.getServerById(Long.parseLong(serverId));
        if (server.isPresent()) {
            this.currentServer = server.get();
            for (NetworkCard nic : server.get().getNetworkCards()) {
                if (nic.getIsPrimary().equals("1")) {
                    this.serverPrimeIp = nic.getIpAddress();
                    break;
                }
            }
        }
        if (this.serverPrimeIp.isEmpty()) {
            return -1;
        }
        if (this.currentServer == null) {
            return -1;
        }
        this.config
            = DefaultDockerClientConfig.createDefaultConfigBuilder()
            .withRegistryEmail("he.amirhossein@gmail.com")
            .withDockerHost("tcp://" + this.serverPrimeIp + ":2375").build();

        this.dockerClient = DockerClientBuilder.getInstance(this.config).build();
        this.deleteTopology(this.AllCurrenttopologiesMap.get(serverId));
        this.initiCurrentTopology(serverId);
        return 1;
    }

    public void updateTopology(TopologyVM topology) {
        //Update VNF
        this.updateDockerVnf(topology.getVnfs());
    }

    public void initiAllCurrentTopology() {
        this.AllCurrenttopologiesMap.clear();
        for (ServerVM serverVM : this.serverService.getAllServer(new PageRequest(0, Integer.MAX_VALUE))) {
            //Init Current Topology at first time
            TopologyVM topologyVM = new TopologyVM();
            List<ControllerVM> controllers = new ArrayList<>();
            List<SwitchVM> switches = new ArrayList<>();
            List<VnfVM> vnfs = new ArrayList<>();
            List<MainServerInterfaceVM> mainServerInterface = new ArrayList<>();
            List<DiagramEntityConnectionsVM> diagramEntityConnections = new ArrayList<>();
            topologyVM.setControllers(controllers);
            topologyVM.setSwitches(switches);
            topologyVM.setVnfs(vnfs);
            topologyVM.setMainServerInterface(mainServerInterface);
            topologyVM.setServerId(serverVM.getId().toString());
            topologyVM.setDiagramEntityConnections(diagramEntityConnections);

            this.AllCurrenttopologiesMap.put(serverVM.getId().toString(), topologyVM);
        }

    }

    public void initiCurrentTopology(String serverId) {
        TopologyVM topologyVM = new TopologyVM();
        List<ControllerVM> controllers = new ArrayList<>();
        List<SwitchVM> switches = new ArrayList<>();
        List<VnfVM> vnfs = new ArrayList<>();
        List<MainServerInterfaceVM> mainServerInterface = new ArrayList<>();
        List<DiagramEntityConnectionsVM> diagramEntityConnections = new ArrayList<>();
        topologyVM.setControllers(controllers);
        topologyVM.setSwitches(switches);
        topologyVM.setVnfs(vnfs);
        topologyVM.setMainServerInterface(mainServerInterface);
        topologyVM.setServerId(serverId);
        topologyVM.setDiagramEntityConnections(diagramEntityConnections);
        this.AllCurrenttopologiesMap.put(serverId, topologyVM);
    }

    public void setControllerForAllBridges(TopologyVM topology) {
        if (topology != null) {
            for (SwitchVM swt : topology.getSwitches()) {
                // TODO change this shit
                // Update controller by resetting its port
                createBridge(
                    "ovsdb://HOST" + topology.getServerId(),
                    swt.getDescribtion().split("#")[1],
                    swt.getDescribtion().split("#")[1],
                    swt.getControllerIp(),
                    swt.getControllerPort(),
                    "ovsdb:ovsdb-bridge-protocol-openflow-13"
                );
                createBridge(
                    "ovsdb://HOST" + topology.getServerId(),
                    swt.getDescribtion().split("#")[1],
                    swt.getDescribtion().split("#")[1],
                    swt.getControllerIp(),
                    swt.getControllerPort(),
                    "ovsdb:ovsdb-bridge-protocol-openflow-13"
                );
            }
        }
    }

    public Integer createBridge(String nodeId, String bridgeId, String bridgeName, String controllerIp, String controllerPort, String protocolVersion) {
        PutCreateBridge putCreateBridge = new PutCreateBridge(
            nodeId,
            bridgeId,
            bridgeName,
            controllerIp,
            controllerPort,
            protocolVersion
        );
        return putCreateBridge.execute();
    }

    public Integer createBridgePort(String nodeId, String bridgeName, String portName, String portOptionKey, String portOptionValue, String interfaceType) {
        PutCreateBridgePort putCreateBridgePort = new PutCreateBridgePort(
            nodeId,
            bridgeName,
            portName,
            portOptionKey,
            portOptionValue,
            interfaceType
        );
        return putCreateBridgePort.execute();
    }

    public Integer createBridgePortGre(String nodeId, String bridgeName, String portName, String portOptionKey, String portOptionValue, String portOption2Key, String portOption2Value, String interfaceType) {
        PutCreateBridgePortGre putCreateBridgePortGre = new PutCreateBridgePortGre(
            nodeId,
            bridgeName,
            portName,
            portOptionKey,
            portOptionValue,
            portOption2Key,
            portOption2Value,
            interfaceType
        );
        return putCreateBridgePortGre.execute();
    }

    public Integer deleteBridge(String nodeId, String bridgeId) {
        DeleteBridge deleteBridge = new DeleteBridge(
            nodeId,
            bridgeId
        );
        return deleteBridge.execute();
    }

    public Integer deleteBridgePort(String nodeId, String bridgeName, String portName) {
        DeleteBridgePort deleteBridgePort = new DeleteBridgePort(
            nodeId,
            bridgeName,
            portName
        );
        return deleteBridgePort.execute();
    }

    public TopologyVM getNetworkTopology() {
        return null;
    }

    public void createDockerVnf(String imgName, String containerName, String executionCommand, String cpu, String ram, String isDedicatedRes) {

        CreateContainerResponse container;
        if (isDedicatedRes.equals('0')) {
            container = dockerClient.createContainerCmd(imgName)
                .withCmd("/bin/bash", "-c", executionCommand)
                .withName(containerName)
                .withNetworkMode("none")
                .exec();
        } else {
            String cp, ra;
            cp = cpu.equals("0") ? "1" : cpu;
            ra = ram.equals("0") ? "1" : ram;
            container = dockerClient.createContainerCmd(imgName)
                .withCmd("/bin/bash", "-c", executionCommand)
                .withName(containerName)
                .withMemory((long) (Float.valueOf(ra) * 1024 * 1024 * 1024))
                .withCpuPeriod((int) (10000 * Float.valueOf(cp)))
                .withNetworkMode("none")
                .exec();
        }

        this.dockerClient.startContainerCmd(container.getId()).exec();
    }

    public Integer createDockerVnf(String serverId, String imgName, String containerName, String executionCommand, String cpu, String ram, String isDedicatedRes) {

        /*
        Create Docker Client by Finding ip address of given serverId
         */
        DockerClient dockerClient;
        DefaultDockerClientConfig config;
        String serverPrimeIp = "";
        Server currentServer = null;

        Optional<Server> server = this.serverService.getServerById(Long.parseLong(serverId));
        if (server.isPresent()) {
            currentServer = server.get();
            for (NetworkCard nic : server.get().getNetworkCards()) {
                if (nic.getIsPrimary().equals("1")) {
                    serverPrimeIp = nic.getIpAddress();
                    break;
                }
            }
        }
        if (serverPrimeIp.isEmpty()) {
            return 420;
        }
        if (currentServer == null) {
            return 400;
        }
        config
            = DefaultDockerClientConfig.createDefaultConfigBuilder()
            .withRegistryEmail("he.amirhossein@gmail.com")
            .withDockerHost("tcp://" + serverPrimeIp + ":2375").build();

        dockerClient = DockerClientBuilder.getInstance(config).build();

        CreateContainerResponse container;
        if (isDedicatedRes.equals('0')) {
            container = dockerClient.createContainerCmd(imgName)
                .withCmd("/bin/bash", "-c", executionCommand)
                .withName(containerName)
                .withNetworkMode("none")
                .exec();
        } else {
            String cp, ra;
            cp = cpu.equals("0") ? "1" : cpu;
            ra = ram.equals("0") ? "1" : ram;
            container = dockerClient.createContainerCmd(imgName)
                .withCmd("/bin/bash", "-c", executionCommand)
                .withName(containerName)
                .withMemory((long) (Float.valueOf(ra) * 1024 * 1024 * 1024))
                .withCpuPeriod((int) (10000 * Float.valueOf(cp)))
                .withNetworkMode("none")
                .exec();
        }

        this.dockerClient.startContainerCmd(container.getId()).exec();
        return 200;
    }

    public void createOdlDocker(String imgName, String containerName) {
        CreateContainerResponse container
            = dockerClient.createContainerCmd(imgName)
            /*
            New odl image is already self executing the karaf -> ./distribution-karaf-0.6.3-Carbon/bin/karaf
            also for accessing karaf CLI use " ssh -p 8101 karaf@localhost" default pass is "karaf"
             */
//            .withCmd("/opt/opendaylight/bin/karaf")
            .withName(containerName)
            .withPortBindings(PortBinding.parse("6633:6633"), PortBinding.parse("8181:8181"), PortBinding.parse("8101:8101"))
            .exec();
        this.dockerClient.startContainerCmd(container.getId()).exec();
    }

    public void deleteDockerVnf(List<String> containerName) {
        if (containerName.size() > 0) {
            for (Container container : dockerClient.listContainersCmd().withShowAll(true).exec()) {
                for (String str : containerName) {
                    //Regex for that "container.getNames()[0]" return "/containerName" so we do'nt need "/"
                    if (str.equals(container.getNames()[0].split("/")[1])) {
//                    dockerClient.killContainerCmd(container.getId()).exec();
                        dockerClient.removeContainerCmd(container.getId()).withForce(true).exec();
                    }
                }
            }
        }
    }

    public Integer deleteDockerVnf(String serverId, String containerName) {
        DockerClient dockerClient;
        DefaultDockerClientConfig config;
        String serverPrimeIp = "";
        Server currentServer = null;
        Optional<Server> server = this.serverService.getServerById(Long.parseLong(serverId));
        if (server.isPresent()) {
            currentServer = server.get();
            for (NetworkCard nic : server.get().getNetworkCards()) {
                if (nic.getIsPrimary().equals("1")) {
                    serverPrimeIp = nic.getIpAddress();
                    break;
                }
            }
        }
        if (serverPrimeIp.isEmpty()) {
            return 420;
        }
        if (currentServer == null) {
            return 400;
        }
        config
            = DefaultDockerClientConfig.createDefaultConfigBuilder()
            .withRegistryEmail("he.amirhossein@gmail.com")
            .withDockerHost("tcp://" + serverPrimeIp + ":2375").build();
        dockerClient = DockerClientBuilder.getInstance(config).build();
        for (Container container : dockerClient.listContainersCmd().withShowAll(true).exec()) {
            //Regex for that "container.getNames()[0]" return "/containerName" so we do'nt need "/"
            if (containerName.equals(container.getNames()[0].split("/")[1])) {
//                    dockerClient.killContainerCmd(container.getId()).exec();
                dockerClient.removeContainerCmd(container.getId()).withForce(true).exec();
                break;
            }
        }
        return 200;
    }

    public void updateDockerVnf(List<VnfVM> containers) {
        if (containers.size() > 0) {
            for (Container container : dockerClient.listContainersCmd().withShowAll(true).exec()) {
                for (VnfVM vnf : containers) {
                    //Regex for that "container.getNames()[0]" return "/containerName" so we do'nt need "/"
                    if (vnf.getDescribtion().split("#")[1].equals(container.getNames()[0].split("/")[1])) {
                        String cp, ra;
                        cp = vnf.getCpu().equals("0") ? "1" : vnf.getCpu();
                        ra = vnf.getRam().equals("0") ? "1" : vnf.getRam();
                        dockerClient.updateContainerCmd(container.getId())
//                              .withBlkioWeight(300)
//                              .withCpuShares(512)
                            .withCpuPeriod((int) (10000 * Float.valueOf(cp)))
//                              .withCpuQuota(50000)
//                              .withCpusetCpus("0") // depends on env
//                              .withCpusetMems("0")
                            .withMemory((long) (Float.valueOf(ra) * 1024 * 1024 * 1024))
//                              .withMemorySwap(514288000L) Your kernel does not support swap limit capabilities, memory limited without swap.
//                              .withMemoryReservation(209715200L)
//                              .withKernelMemory(52428800) Can not update kernel memory to a running container, please stop it first.
                            .exec();
                    }
                }
            }
        }
    }

    public Integer updateDockerVnf(String serverId, String containerName, String cpu, String ram) {
        DockerClient dockerClient;
        DefaultDockerClientConfig config;
        String serverPrimeIp = "";
        Server currentServer = null;
        Optional<Server> server = this.serverService.getServerById(Long.parseLong(serverId));
        if (server.isPresent()) {
            currentServer = server.get();
            for (NetworkCard nic : server.get().getNetworkCards()) {
                if (nic.getIsPrimary().equals("1")) {
                    serverPrimeIp = nic.getIpAddress();
                    break;
                }
            }
        }
        if (serverPrimeIp.isEmpty()) {
            return 420;
        }
        if (currentServer == null) {
            return 400;
        }
        config
            = DefaultDockerClientConfig.createDefaultConfigBuilder()
            .withRegistryEmail("he.amirhossein@gmail.com")
            .withDockerHost("tcp://" + serverPrimeIp + ":2375").build();
        dockerClient = DockerClientBuilder.getInstance(config).build();

        for (Container container : dockerClient.listContainersCmd().withShowAll(true).exec()) {
            //Regex for that "container.getNames()[0]" return "/containerName" so we do'nt need "/"
            if (containerName.equals(container.getNames()[0].split("/")[1])) {
                String cp, ra;
                cp = cpu.equals("0") ? "1" : cpu;
                ra = ram.equals("0") ? "1" : ram;
                dockerClient.updateContainerCmd(container.getId())
//                              .withBlkioWeight(300)
//                              .withCpuShares(512)
                    .withCpuPeriod((int) (10000 * Float.valueOf(cp)))
//                              .withCpuQuota(50000)
//                              .withCpusetCpus("0") // depends on env
//                              .withCpusetMems("0")
                    .withMemory((long) (Float.valueOf(ra) * 1024 * 1024 * 1024))
//                              .withMemorySwap(514288000L) Your kernel does not support swap limit capabilities, memory limited without swap.
//                              .withMemoryReservation(209715200L)
//                              .withKernelMemory(52428800) Can not update kernel memory to a running container, please stop it first.
                    .exec();
                break;
            }
        }
        return 200;
    }

    public void createDockerVnfOvsPort(String serverPrimeIp, String bridgeName, String containerName, String interfaceName, String ipAddress) {
        System.out.println("Ip Address");
        System.out.println(ipAddress);
        List<String> commandArray = new ArrayList<String>();
        String command = "ovs-docker add-port " + bridgeName + " " + interfaceName + " " + containerName + " --ipaddress=" + ipAddress + "/24" + " --mtu=1400";
        commandArray.add(command);
        ExecuteCommanOverSsh sshCommand = new ExecuteCommanOverSsh();
        if (sshCommand.openConnection(serverPrimeIp, 22, this.currentServer.getSshUsername(),
            this.currentServer.getSshPassword(), 12000000, null)) {
            System.out.println("Connected to the Server");
            sshCommand.sendCommand(commandArray);
            Map<String, String> result = sshCommand.recvData();
            sshCommand.close();
        } else {
            System.out.println("Cant connect to the server");
        }
    }

    public Integer createDockerVnfOvsPortByServerId(String serverId, String bridgeName, String containerName, String interfaceName, String ipAddress) {
        String serverPrimeIp = "";
        Server currentServer = null;

        Optional<Server> server = this.serverService.getServerById(Long.parseLong(serverId));
        if (server.isPresent()) {
            currentServer = server.get();
            for (NetworkCard nic : server.get().getNetworkCards()) {
                if (nic.getIsPrimary().equals("1")) {
                    serverPrimeIp = nic.getIpAddress();
                    break;
                }
            }
        }
        if (serverPrimeIp.isEmpty()) {
            return 420;
        }
        if (currentServer == null) {
            return 400;
        }
        List<String> commandArray = new ArrayList<String>();
        String command = "ovs-docker add-port " + bridgeName + " " + interfaceName + " " + containerName + " --ipaddress=" + ipAddress + "/24" + " --mtu=1400";
        commandArray.add(command);
        ExecuteCommanOverSsh sshCommand = new ExecuteCommanOverSsh();
        if (sshCommand.openConnection(serverPrimeIp, 22, currentServer.getSshUsername(),
            currentServer.getSshPassword(), 12000000, null)) {
            System.out.println("Connected to the Server");
            sshCommand.sendCommand(commandArray);
            Map<String, String> result = sshCommand.recvData();
            sshCommand.close();
        } else {
            System.out.println("Cant connect to the server");
            return 410;
        }
        return 200;
    }

    public void deleteDockerVnfOvsPort(String serverPrimeIp, String bridgeName, String containerName, String interfaceName) {
        System.out.println("delete vnf bridge port");
        String command = "ovs-docker del-port " + bridgeName + " " + interfaceName + " " + containerName;
        System.out.println(command);
        List<String> commandArray = new ArrayList<String>();
        commandArray.add(command);
        ExecuteCommanOverSsh sshCommand = new ExecuteCommanOverSsh();
        if (sshCommand.openConnection(serverPrimeIp, 22, this.currentServer.getSshUsername(),
            this.currentServer.getSshPassword(), 12000000, null)) {
            System.out.println("Connected to the Server");
            sshCommand.sendCommand(commandArray);
            Map<String, String> result = sshCommand.recvData();
            sshCommand.close();
        } else {
            System.out.println("Cant connect to the server");
        }

    }

    public Integer deleteDockerVnfOvsPortByServerId(String serverId, String bridgeName, String containerName, String interfaceName) {
        String serverPrimeIp = "";
        Server currentServer = null;

        Optional<Server> server = this.serverService.getServerById(Long.parseLong(serverId));
        if (server.isPresent()) {
            currentServer = server.get();
            for (NetworkCard nic : server.get().getNetworkCards()) {
                if (nic.getIsPrimary().equals("1")) {
                    serverPrimeIp = nic.getIpAddress();
                    break;
                }
            }
        }
        if (serverPrimeIp.isEmpty()) {
            return 420;
        }
        if (currentServer == null) {
            return 400;
        }
        System.out.println("delete vnf bridge port");
        String command = "ovs-docker del-port " + bridgeName + " " + interfaceName + " " + containerName;
        System.out.println(command);
        List<String> commandArray = new ArrayList<>();
        commandArray.add(command);
        ExecuteCommanOverSsh sshCommand = new ExecuteCommanOverSsh();
        if (sshCommand.openConnection(serverPrimeIp, 22, currentServer.getSshUsername(),
            currentServer.getSshPassword(), 12000000, null)) {
            System.out.println("Connected to the Server");
            sshCommand.sendCommand(commandArray);
            Map<String, String> result = sshCommand.recvData();
            sshCommand.close();
        } else {
            System.out.println("Cant connect to the server");
        }
        return 200;
    }

    public Integer registerServerToOVSDbInventory(String serverId, String serverIp) {
        String OvsDbNodeId = "ovsdb://HOST" + serverId;
        PutPassiveConnection putPassiveConnection = new PutPassiveConnection(
            OvsDbNodeId,
            "6640",
            serverIp
        );
        return putPassiveConnection.execute();
    }

    public Map<String, TopologyVM> getAllCurrenttopologiesMap() {
        return AllCurrenttopologiesMap;
    }

    public void setAllCurrenttopologiesMap(Map<String, TopologyVM> allCurrenttopologiesMap) {
        AllCurrenttopologiesMap = allCurrenttopologiesMap;
    }

    public void setTopologyToAllCurrentTopologiesMap(String nodeId, TopologyVM topologyVM) {
        this.getAllCurrenttopologiesMap().put(nodeId, topologyVM);
    }

    public TopologyVM getTopologyByNodeIdFromAllCurrenttopologiesMap(String nodeId) {
        return AllCurrenttopologiesMap.get(nodeId);
    }

}
