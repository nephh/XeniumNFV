package com.ahghorab.xenonet.service.util;

import com.ahghorab.xenonet.service.dto.MustKeepNodePort;
import com.ahghorab.xenonet.service.dto.MustKeepTopology;
import com.ahghorab.xenonet.service.dto.TopologyTrinityStats;
import com.ahghorab.xenonet.web.rest.vm.*;

import java.util.ArrayList;
import java.util.List;

public final class TopologyDif {
    public static TopologyTrinityStats getTopologyDif(TopologyVM oldTopo, TopologyVM newTopo) {

        // Controller Part
        //****************************************************************************
        List<ControllerVM> mustDeleteControlles = new ArrayList<>();
        List<ControllerVM> mustCreateControlles = new ArrayList<>();
        List<ControllerVM> mustUpdatekeepControlles = new ArrayList<>();
        // Find Must-Delete and Must-keep controller array list
        for (ControllerVM oldController : oldTopo.getControllers()) {
            boolean isMustDelete = true;
            for (ControllerVM newController : newTopo.getControllers()) {
                // old one is the same as new one So keep it
                if (oldController.getKey().equals(newController.getKey())) {
                    isMustDelete = false;
                    // the controller is same so add new one (new cpu / ram / ip add of controller)
                    if(!oldController.getRam().equals(newController.getRam()) || !oldController.getCpu().equals(newController.getCpu())) {
                        mustUpdatekeepControlles.add(newController);
                    }
                    break;
                }
            }
            if (isMustDelete) {
                // old one is not same az all new one's so delete it
                mustDeleteControlles.add(oldController);
            }
        }
        // Find Must-create controller array list
        for (ControllerVM newController : newTopo.getControllers()) {
            if (oldTopo.getControllers().size() > 0) {
                boolean isMustCreate = true;
                for (ControllerVM oldController : oldTopo.getControllers()) {
                    // new one is new so create it !!
                    if (oldController.getKey().equals(newController.getKey())) {
                        isMustCreate = false;
                        break;
                    }
                }
                if (isMustCreate) {
                    mustCreateControlles.add(newController);
                }
            } else {
                mustCreateControlles.add(newController);
            }
        }
        // Switch Part
        //****************************************************************************
        List<SwitchVM> mustDeleteSwitch = new ArrayList<>();
        List<SwitchVM> mustCreateSwitch = new ArrayList<>();
        List<SwitchVM> mustUpdateKeepSwitch = new ArrayList<>();
        // Find Must-Delete and Must-keep Switch array list
        for (SwitchVM oldSwitch : oldTopo.getSwitches()) {
            boolean isMustDelete = true;
            for (SwitchVM newSwitch : newTopo.getSwitches()) {
                // old one is not same az all new one's so delete it
                if (oldSwitch.getKey().equals(newSwitch.getKey())) {
                    isMustDelete = false;
                    // the switch is same so add new one
                    /*
                    well for now i don't think this array might be useful but i add this feature maybe
                    it become handily
                     */
                    mustUpdateKeepSwitch.add(newSwitch);
                    break;
                }
            }
            if (isMustDelete) {
                mustDeleteSwitch.add(oldSwitch);
            }
        }
        // Find Must-create Switch array list
        for (SwitchVM newSwitch : newTopo.getSwitches()) {
            if (oldTopo.getSwitches().size() > 0) {
                boolean isMustCreate = true;
                for (SwitchVM oldSwitch : oldTopo.getSwitches()) {
                    // new one is new so create it !!
                    if (oldSwitch.getKey().equals(newSwitch.getKey())) {
                        isMustCreate = false;
                        break;
                    }
                }
                if (isMustCreate) {
                    mustCreateSwitch.add(newSwitch);
                }
            } else {
                mustCreateSwitch.add(newSwitch);
            }
        }

        // VNF Part
        //****************************************************************************
        List<VnfVM> mustDeleteVnf = new ArrayList<>();
        List<VnfVM> mustCreateVnf = new ArrayList<>();
        List<VnfVM> mustUpdateKeepVnf = new ArrayList<>();
        // Find Must-Delete and Must-keep Vnf array list
        for (VnfVM oldVnf : oldTopo.getVnfs()) {
            boolean isMustDelete = true;
            for (VnfVM newVnf : newTopo.getVnfs()) {
                // old one is not same az all new one's so delete it
                if (oldVnf.getKey().equals(newVnf.getKey())) {
                    isMustDelete = false;
                    // the VNF is same so add new one (new cpu / ram / ip add of VNF)
                    if(!oldVnf.getRam().equals(newVnf.getRam()) || !oldVnf.getCpu().equals(newVnf.getCpu())) {
                        mustUpdateKeepVnf.add(newVnf);
                    }
                    break;
                }
            }
            if (isMustDelete) {
                mustDeleteVnf.add(oldVnf);
            }
        }
        // Find Must-create Vnf array list
        for (VnfVM newVnf : newTopo.getVnfs()) {
            if (oldTopo.getVnfs().size() > 0) {
                boolean isMustCreate = true;
                for (VnfVM oldVnf : oldTopo.getVnfs()) {
                    // new one is new so create it !!
                    if (oldVnf.getKey().equals(newVnf.getKey())) {
                        isMustCreate = false;
                        break;
                    }
                }
                if (isMustCreate) {
                    mustCreateVnf.add(newVnf);
                }
            } else {
                mustCreateVnf.add(newVnf);
            }

        }
        // MainServerInterface Part
        //****************************************************************************
        List<MainServerInterfaceVM> mustDeleteMainServerInterface = new ArrayList<>();
        List<MainServerInterfaceVM> mustCreateMainServerInterface = new ArrayList<>();
        List<MainServerInterfaceVM> mustUpdateKeepMainServerInterface = new ArrayList<>();
        // Find Must-Delete and Must-keep Vnf array list
        for (MainServerInterfaceVM oldMainServerInterface : oldTopo.getMainServerInterface()) {
            boolean isMustDelete = true;
            for (MainServerInterfaceVM newMainServerInterface : newTopo.getMainServerInterface()) {
                // old one is not same az all new one's so delete it
                if (oldMainServerInterface.getKey().equals(newMainServerInterface.getKey())) {
                    isMustDelete = false;
                    //Badan inja bayad shat bezaram ke ip avaz shod avaz beshe
                    mustUpdateKeepMainServerInterface.add(newMainServerInterface);
                    break;
                }
            }
            if (isMustDelete) {
                mustDeleteMainServerInterface.add(oldMainServerInterface);
            }
        }
        // Find Must-create Vnf array list
        for (MainServerInterfaceVM newMainServerInterface : newTopo.getMainServerInterface()) {
            if (oldTopo.getMainServerInterface().size() > 0) {
                boolean isMustCreate = true;
                for (MainServerInterfaceVM oldMainServerInterface : oldTopo.getMainServerInterface()) {
                    // new one is new so create it !!
                    if (oldMainServerInterface.getKey().equals(newMainServerInterface.getKey())) {
                        isMustCreate = false;
                        break;
                    }
                }
                if (isMustCreate) {
                    mustCreateMainServerInterface.add(newMainServerInterface);
                }
            } else {
                mustCreateMainServerInterface.add(newMainServerInterface);
            }

        }

        // PORT'S Part
        //****************************************************************************
        List<DiagramEntityConnectionsVM> mustDeletePort = new ArrayList<>();
        List<DiagramEntityConnectionsVM> mustCreatePort = new ArrayList<>();
        // i don't use it
        List<DiagramEntityConnectionsVM> mustUpdateKeepPort = new ArrayList<>();
        // Find Must-Delete and Must-keep Port array list
        for (DiagramEntityConnectionsVM oldPort : oldTopo.getDiagramEntityConnections()) {
            boolean isMustDelete = true;
            for (DiagramEntityConnectionsVM newPort : newTopo.getDiagramEntityConnections()) {
                // old one is not same az all new one's so delete it
                if (oldPort.getKey().equals(newPort.getKey())) {
                    isMustDelete = false;
                    break;
                }
            }
            if (isMustDelete) {
                mustDeletePort.add(oldPort);
            }
        }
        // Find Must-create Vnf array list
        for (DiagramEntityConnectionsVM newPort : newTopo.getDiagramEntityConnections()) {
            if (oldTopo.getVnfs().size() > 0) {
                boolean isMustCreate = true;
                for (DiagramEntityConnectionsVM oldPort : oldTopo.getDiagramEntityConnections()) {
                    // new one is new so create it !!
                    if (oldPort.getKey().equals(newPort.getKey())) {
                        isMustCreate = false;
                        break;
                    }
                }
                if (isMustCreate) {
                    mustCreatePort.add(newPort);
                }
            } else {
                mustCreatePort.add(newPort);
            }

        }
        //****************************************************************************

        TopologyVM mustCreateTopology = new TopologyVM(
            mustCreateControlles,
            mustCreateSwitch,
            mustCreateVnf,
            new ArrayList<SfcVM>(),
            mustCreateMainServerInterface,
            newTopo.getServerId(),
            mustCreatePort);
        TopologyVM mustDeleteTopology = new TopologyVM(
            mustDeleteControlles,
            mustDeleteSwitch,
            mustDeleteVnf,
            new ArrayList<SfcVM>(),
            mustDeleteMainServerInterface,
            newTopo.getServerId(),
            mustDeletePort);

        TopologyVM mustUpdateKeepTopology = new TopologyVM(
            mustUpdatekeepControlles,
            mustUpdateKeepSwitch,
            mustUpdateKeepVnf,
            new ArrayList<SfcVM>(),
            mustUpdateKeepMainServerInterface,
            newTopo.getServerId(),
            mustUpdateKeepPort);

        return new TopologyTrinityStats(
            mustCreateTopology,
            mustDeleteTopology,
            mustUpdateKeepTopology
        );
    }

}
