package com.ahghorab.xenonet.service.util.event;

import com.ahghorab.xenonet.service.NetworkDiagramService;
import com.ahghorab.xenonet.service.util.monitoring.MonitoredContainer;
import com.ahghorab.xenonet.web.rest.vm.SwitchVM;
import com.ahghorab.xenonet.web.rest.vm.TopologyVM;
import com.ahghorab.xenonet.web.rest.vm.VnfVM;
import com.ahghorab.xenonet.web.rest.vm.event.EventDiagramEntityConnectionsVM;
import com.ahghorab.xenonet.web.rest.vm.event.EventDiagramVM;
import com.ahghorab.xenonet.web.rest.vm.event.EventVM;
import com.ahghorab.xenonet.web.rest.vm.event.exec.*;
import com.ahghorab.xenonet.web.rest.vm.event.logic.LogicalAndVM;
import com.ahghorab.xenonet.web.rest.vm.event.logic.LogicalOrVM;
import com.ahghorab.xenonet.web.rest.vm.event.math.*;
import com.ahghorab.xenonet.web.rest.vm.event.notifier.NotifierExternalVM;
import com.ahghorab.xenonet.web.rest.vm.event.notifier.NotifierVNFVM;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class EventTask implements Runnable {

    private Thread worker;
    private EventVM eventVM;
    private long startingOffset;
    private ObjectMapper objMapper;
    private KafkaConsumer<String, String> consumer;
    private final NetworkDiagramService networkDiagramService;
    private final AtomicBoolean running = new AtomicBoolean(false);
    private Long numberOfOccure;

    /*
    I Created TOPIC Object coz in previous design i used Map<String(topic), String(value)> but assume
    when one Topic like CAT( cAdvisor topic) has multiple consumer like "cpu of VNF1" and "cpu of VNF2"
    so in that case multiple same Key adding to the MAP !!!!
     */
    private List<Topic> topicObjList = new ArrayList<>();

    public EventTask(EventVM eventVM, NetworkDiagramService networkDiagramService, long startingOffset) {
        this.networkDiagramService = networkDiagramService;
        this.eventVM = eventVM;
        this.startingOffset = startingOffset;
        this.consumer = new KafkaConsumer<>(config());
        objMapper = new ObjectMapper();
    }


    private List<String> findTopicList(EventDiagramVM eventDiagram) {
        List<String> topicList = new ArrayList<>();
        topicObjList.clear();
        for (NotifierExternalVM notifierExternalVM : eventDiagram.getNotifierExternal()) {
            Topic newTopicObj = new Topic(
                notifierExternalVM.getTopicName(),
                "",
                "",
                "",
                "",
                null
            );
            this.topicObjList.add(newTopicObj);
            if (topicList.indexOf(notifierExternalVM.getTopicName()) < 0) {
                topicList.add(notifierExternalVM.getTopicName());
            }
        }
//        for (NotifierSwitchVM notifierSwitchVM : eventDiagram.getNotifierSwitch()) {
//            topicList.add(notifierSwitchVM.getName());
//        }
        for (NotifierVNFVM notifierVNFVM : eventDiagram.getNotifierVnf()) {
            Topic newTopicObj = new Topic(
                notifierVNFVM.getTopicName() + "_" + notifierVNFVM.getContainerName(),
                notifierVNFVM.getContainerName(),
                notifierVNFVM.getMetric(),
                notifierVNFVM.getInterfaceName(),
                "",
                null
            );
            /*
            For VNF notifier the producer send message with "topicName(T-VNF)+ _ +containerName"
            like T-VNF_2365
             */
            this.topicObjList.add(newTopicObj);
            if (topicList.indexOf(notifierVNFVM.getTopicName() + "_" + notifierVNFVM.getContainerName()) < 0) {
                topicList.add(notifierVNFVM.getTopicName() + "_" + notifierVNFVM.getContainerName());
            }
        }
        return topicList;
    }

    public void start() {
        if (!running.get()) {
            worker = new Thread(this);
            running.set(true);
            worker.start();
        }
    }

    public void stop() {
        if (running.get()) {
            numberOfOccure = 0l;
            consumer.close();
            running.set(false);
            System.out.println("STOP Thread " + Thread.currentThread().getId());
        }
    }

    public void interrupt() {
        numberOfOccure = 0l;
        running.set(false);
        worker.interrupt();
    }

    @Override
    public void run() {
        this.consumer.subscribe(findTopicList(eventVM.getEventDiagram()), new ConsumerRebalanceListener() {
            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            }

            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                Iterator<TopicPartition> topicPartitionIterator = partitions.iterator();
                while (topicPartitionIterator.hasNext()) {
                    TopicPartition topicPartition = topicPartitionIterator.next();
                    if (startingOffset == 0) {
                        consumer.seekToBeginning(Collections.singletonList(topicPartition));
                    } else if (startingOffset == -1) {
                        consumer.seekToEnd(Collections.singletonList(topicPartition));
                    } else {
                        consumer.seek(topicPartition, startingOffset);
                    }
                }
            }
        });
        numberOfOccure = 0l;
        System.out.println("START Thread " + Thread.currentThread().getId());
        boolean isAllDataAvailable;
        while (running.get()) {
            System.out.println("im still running");
            isAllDataAvailable = true;
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
                /*
                Check if topic is inside the vnfNotifierTopicnotifierVNFVMMap
                 */
                topicObjList.forEach(topicObj -> {
                    if (topicObj.getName().equals(record.topic())) {
                        /*
                        record belong to external notifier, external notifier has "" networkEntity name
                         */
                        if (topicObj.getNetworkEntityName().equals("")) {
                            topicObj.setValue(record.value());
                            topicObj.setValue(null);
                        }
                        /*
                        record belong to VNF/SWITCH notifier
                         */
                        else {
                            topicObj.setValue("");
                            try {
                                topicObj.setMonitoredContainer(objMapper.readValue(record.value(), MonitoredContainer.class));
                            } catch (Exception e) {
                                System.out.println("Cant map record string to MonitoredContainer CLASS -> " + e);
                            }
                        }
                    }
                });
            }

            if (topicObjList.size() > 0) {
                for (Topic topicObj : topicObjList) {
                    if (topicObj.getValue().equals("") && topicObj.getMonitoredContainer() == null) {
                        isAllDataAvailable = false;
                        break;
                    }
                    //---------------
                    if (topicObj.getMonitoredContainer().getCpu() < 0.0) {
                        isAllDataAvailable = false;
                        break;
                    }
                    //---------------
                    if (topicObj.getMonitoredContainer().getRam() < 0.0) {
                        isAllDataAvailable = false;
                        break;
                    }
                    //---------------
                    if (topicObj.getMonitoredContainer().getTotalRx() == null) {
                        isAllDataAvailable = false;
                        break;
                    } else {
                        if (topicObj.getMonitoredContainer().getTotalRx().entrySet().size() < 1) {
                            isAllDataAvailable = false;
                            break;
                        }
                    }
                    //---------------
                    if (topicObj.getMonitoredContainer().getRxPerSec() == null) {
                        isAllDataAvailable = false;
                        break;
                    } else {
                        if (topicObj.getMonitoredContainer().getRxPerSec().entrySet().size() < 1) {
                            isAllDataAvailable = false;
                            break;
                        }
                    }
                    //---------------
                    if (topicObj.getMonitoredContainer().getTotalTx() == null) {
                        isAllDataAvailable = false;
                        break;
                    } else {
                        if (topicObj.getMonitoredContainer().getTotalTx().entrySet().size() < 1) {
                            isAllDataAvailable = false;
                            break;
                        }
                    }
                    //---------------
                    if (topicObj.getMonitoredContainer().getTxPerSec() == null) {
                        isAllDataAvailable = false;
                        break;
                    } else {
                        if (topicObj.getMonitoredContainer().getTxPerSec().entrySet().size() < 1) {
                            isAllDataAvailable = false;
                            break;
                        }
                    }
                }
            } else {
                isAllDataAvailable = false;
            }

            if (isAllDataAvailable) {
                System.out.println(Thread.currentThread().getId() + " All Data Is Available");
                /*
                Set the "0" priority connection values (Notifiers and Static Values)
                 */
                //********************************************************************************************************************************************
                /*
                Fill External Notifier Values
                 */
                for (NotifierExternalVM notifierExternal : eventVM.getEventDiagram().getNotifierExternal()) {
                    String externalVal = "";
                    for (Topic topicObj : topicObjList) {
                        if (topicObj.getName().equals(notifierExternal.getTopicName())) {
                            externalVal = topicObj.getValue();
                        }
                    }
                    for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                        // Find From's and Set the Value to them
                        if (notifierExternal.getKey().equals(eventDiagramConnection.getKey().split("%")[0])) {
                            eventDiagramConnection.setValue(externalVal);
                        }
                    }
                }

                /*
                Fill VNF notifier Values
                 */
                for (NotifierVNFVM notifierVNF : eventVM.getEventDiagram().getNotifierVnf()) {
                    MonitoredContainer monitoredContainer = null;
                    for (Topic topicObj : topicObjList) {
                        if (topicObj.getName().equals(notifierVNF.getTopicName() + "_" + notifierVNF.getContainerName())) {
                            monitoredContainer = topicObj.getMonitoredContainer();
                        }
                    }
                    if (monitoredContainer != null) {
                        String vnfVal = "";
                        if (notifierVNF.getMetric().equals("CPU")) {
                            vnfVal = String.valueOf(monitoredContainer.getCpu());
                            System.out.println(Thread.currentThread().getId() + " <---------------> " + notifierVNF.getMetric() + " <----> " + vnfVal);
                        } else if (notifierVNF.getMetric().equals("RAM")) {
                            vnfVal = String.valueOf(monitoredContainer.getRam());
                            System.out.println(Thread.currentThread().getId() + " <---------------> " + notifierVNF.getMetric() + " <----> " + vnfVal);
                        } else if (notifierVNF.getMetric().equals("RXB")) {
                            vnfVal = String.valueOf(monitoredContainer.getRxPerSec().get(notifierVNF.getInterfaceName()));
                            System.out.println(Thread.currentThread().getId() + " " + notifierVNF.getInterfaceName() + " <---------------> " + notifierVNF.getMetric() + " <----> " + vnfVal);
                        } else if (notifierVNF.getMetric().equals("RXT")) {
                            vnfVal = String.valueOf(monitoredContainer.getTotalRx().get(notifierVNF.getInterfaceName()));
                            System.out.println(Thread.currentThread().getId() + " " + notifierVNF.getInterfaceName() + " <---------------> " + notifierVNF.getMetric() + " <----> " + vnfVal);
                        } else if (notifierVNF.getMetric().equals("TXB")) {
                            vnfVal = String.valueOf(monitoredContainer.getTxPerSec().get(notifierVNF.getInterfaceName()));
                            System.out.println(Thread.currentThread().getId() + " " + notifierVNF.getInterfaceName() + " <---------------> " + notifierVNF.getMetric() + " <----> " + vnfVal);
                        } else if (notifierVNF.getMetric().equals("TXT")) {
                            vnfVal = String.valueOf(monitoredContainer.getTotalTx().get(notifierVNF.getInterfaceName()));
                            System.out.println(Thread.currentThread().getId() + " " + notifierVNF.getInterfaceName() + " <---------------> " + notifierVNF.getMetric() + " <----> " + vnfVal);
                        }

                        for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                            // Find From's and Set the Value to them
                            if (notifierVNF.getKey().equals(eventDiagramConnection.getKey().split("%")[0])) {
                                eventDiagramConnection.setValue(vnfVal);
                            }
                        }
                    } else {
                        System.out.println("Cant get Value From Notifier");
                    }
                }


                /*
                Fill Static Values
                 */
                for (MathStaticValVM mathStaticVal : eventVM.getEventDiagram().getMathStaticVal()) {
                    eventVM.getEventDiagram().getEventDiagramEntityConnection().forEach(eventDiagramConnection -> {
                        // Find From's and Set the Value to them
                        if (mathStaticVal.getKey().equals(eventDiagramConnection.getKey().split("%")[0])) {
                            eventDiagramConnection.setValue(mathStaticVal.getValue());
                        }
                    });
                }
                //********************************************************************************************************************************************

                // Looping Through ALL entities and execute the blocks by order
                for (int i = 1; i <= Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum()); i++) {
                    // LogicalAndVM
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getLogicAnd().size() > 0) {
                        for (LogicalAndVM logicAnd : eventVM.getEventDiagram().getLogicAnd()) {
                            if (logicAnd.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find From's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(logicAnd.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 2 values, NO MORE
                                    if (entityInputVal.size() == 2) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                if (entityInputVal.get(0).equals("1") && entityInputVal.get(1).equals("1")) {
                                    entityInputVal.add("1");
                                } else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(logicAnd.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(2));
                                    }
                                }


                            }
                        }
                    }
                    // LogicalOrVM
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getLogicOr().size() > 0) {
                        for (LogicalOrVM logicOr : eventVM.getEventDiagram().getLogicOr()) {
                            if (logicOr.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(logicOr.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 2 values, NO MORE
                                    if (entityInputVal.size() == 2) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                if (entityInputVal.get(0).equals("1") || entityInputVal.get(1).equals("1")) {
                                    entityInputVal.add("1");
                                } else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(logicOr.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(2));
                                    }
                                }
                            }
                        }
                    }
                    // MathAddVM
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getMathAdd().size() > 0) {
                        for (MathAddVM mathAdd : eventVM.getEventDiagram().getMathAdd()) {
                            if (mathAdd.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(mathAdd.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 2 values, NO MORE
                                    if (entityInputVal.size() == 2) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                entityInputVal.add(String.valueOf(Double.valueOf(entityInputVal.get(0)) + Double.valueOf(entityInputVal.get(1))));
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(mathAdd.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(2));
                                    }
                                }
                            }
                        }
                    }
                    // MathCompareVM
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getMathCompare().size() > 0) {
                        for (MathCompareVM mathCompare : eventVM.getEventDiagram().getMathCompare()) {
                            if (mathCompare.getPriorityNum().equals(String.valueOf(i))) {
                                System.out.println("Compare");
                                // Always Has 2 values, NO MORE
                                Map<String, String> entityInputValMap = new HashMap<>();
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(mathCompare.getKey())) {

                                            /*
                                            in order to comparing two value we want always  part "A" in index "0"
                                            and part "B" in index "1" (A == B, A >= B, A <= B).
                                             */
                                        if (eventDiagramConnection.getDestEventNodePartName().equals("A")) {
                                            entityInputValMap.put("A", eventDiagramConnection.getValue());
                                        } else {
                                            if (entityInputValMap.size() == 0) {
                                                entityInputValMap.put("A", "");
                                            }
                                            entityInputValMap.put("B", eventDiagramConnection.getValue());
                                        }
                                    }
                                    // Always Has 2 values, NO MORE
                                    /*
                                    In this part
                                    if (entityInputVal.size() == 0) {
                                                entityInputVal.add(0, "");
                                            }
                                            entityInputVal.add(1, eventDiagramConnection.getValue());
                                      We already create list with size two (even if the first index (that now is ''))
                                      so after that the break work and the first value never get value. so here i disable the
                                      size=2 checking
                                     */
//                                    if (entityInputVal.size() == 2) {
//                                        break;
//                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                entityInputVal.add(entityInputValMap.get("A"));
                                entityInputVal.add(entityInputValMap.get("B"));
                                if (mathCompare.getOperation().equals("==")) {
                                    if (entityInputVal.get(0).equals(entityInputVal.get(1))) {
                                        System.out.println(entityInputVal.get(0) + " == " + (entityInputVal.get(1)));
                                        entityInputVal.add("1");
                                    } else {
                                        entityInputVal.add("0");
                                    }
                                } else if (mathCompare.getOperation().equals(">")) {
                                    if (Double.valueOf(entityInputVal.get(0)) > Double.valueOf(entityInputVal.get(1))) {
                                        System.out.println(entityInputVal.get(0) + " > " + (entityInputVal.get(1)));
                                        entityInputVal.add("1");
                                    } else {
                                        entityInputVal.add("0");
                                    }
                                } else if (mathCompare.getOperation().equals("> =")) {
                                    if (Double.valueOf(entityInputVal.get(0)) >= Double.valueOf(entityInputVal.get(1))) {
                                        System.out.println(entityInputVal.get(0) + " >= " + (entityInputVal.get(1)));
                                        entityInputVal.add("1");
                                    } else {
                                        entityInputVal.add("0");
                                    }
                                } else if (mathCompare.getOperation().equals("<")) {
                                    if (Double.valueOf(entityInputVal.get(0)) < Double.valueOf(entityInputVal.get(1))) {
                                        System.out.println(entityInputVal.get(0) + " < " + (entityInputVal.get(1)));
                                        System.out.println(" EXEC Compare");
                                        entityInputVal.add("1");
                                    } else {
                                        entityInputVal.add("0");
                                    }
                                } else if (mathCompare.getOperation().equals("< =")) {
                                    if (Double.valueOf(entityInputVal.get(0)) <= Double.valueOf(entityInputVal.get(1))) {
                                        System.out.println(entityInputVal.get(0) + " <= " + (entityInputVal.get(1)));
                                        entityInputVal.add("1");
                                    } else {
                                        entityInputVal.add("0");
                                    }
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(mathCompare.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(2));
                                    }
                                }
                            }
                        }
                    }
                    // MathDivVM
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getMathDiv().size() > 0) {
                        for (MathDivVM mathDiv : eventVM.getEventDiagram().getMathDiv()) {
                            if (mathDiv.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(mathDiv.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 2 values, NO MORE
                                    if (entityInputVal.size() == 2) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                entityInputVal.add(String.valueOf(Double.valueOf(entityInputVal.get(0)) / Double.valueOf(entityInputVal.get(1))));
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(mathDiv.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(2));
                                    }
                                }
                            }
                        }
                    }
                    // MathMultipleVM
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getMathMultiple().size() > 0) {
                        for (MathMultipleVM mathMultiple : eventVM.getEventDiagram().getMathMultiple()) {
                            if (mathMultiple.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(mathMultiple.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 2 values, NO MORE
                                    if (entityInputVal.size() == 2) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                entityInputVal.add(String.valueOf(Double.valueOf(entityInputVal.get(0)) * Double.valueOf(entityInputVal.get(1))));
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(mathMultiple.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(2));
                                    }
                                }
                            }
                        }
                    }
                    // MathSubVM
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getMathSub().size() > 0) {
                        for (MathSubVM mathSub : eventVM.getEventDiagram().getMathSub()) {
                            if (mathSub.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(mathSub.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 2 values, NO MORE
                                    if (entityInputVal.size() == 2) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                entityInputVal.add(String.valueOf(Math.abs(Double.valueOf(entityInputVal.get(0)) - Double.valueOf(entityInputVal.get(1)))));
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(mathSub.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(2));
                                    }
                                }
                            }
                        }
                    }
                    // CreateSwitch
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getCreateSwitch().size() > 0) {
                        for (CreateSwitchVM createSwitch : eventVM.getEventDiagram().getCreateSwitch()) {
                            if (createSwitch.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(createSwitch.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 1 values, NO MORE
                                    if (entityInputVal.size() == 1) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                /*
                                Check If switch has already created or NOT
                                 */
                                boolean isEntityAlreadyExist = false;
                                for (SwitchVM swch : this.networkDiagramService.getTopologyByNodeIdFromAllCurrenttopologiesMap(createSwitch.getSwch().getServerId()).getSwitches()) {
                                    if (swch.getKey().equals(createSwitch.getSwch().getKey())) {
                                        isEntityAlreadyExist = true;
                                        break;
                                    }
                                }
                                if (!isEntityAlreadyExist) {
                                    if (entityInputVal.get(0).equals("1")) {
                                        Integer resStatus = this.networkDiagramService.createBridge(
                                            "ovsdb://HOST" + createSwitch.getSwch().getServerId(),
                                            createSwitch.getSwch().getDescribtion().split("#")[1],
                                            createSwitch.getSwch().getDescribtion().split("#")[1],
                                            createSwitch.getSwch().getControllerIp(),
                                            createSwitch.getSwch().getControllerPort(),
                                            "ovsdb:ovsdb-bridge-protocol-openflow-13"
                                        );
                                        if (resStatus < 300 && resStatus >= 200) {
                                            entityInputVal.add("1");
                                        /*
                                        Add new switch to Topology in NetworkDiagramService
                                         */
                                            TopologyVM tempTopo = this.networkDiagramService.getTopologyByNodeIdFromAllCurrenttopologiesMap(createSwitch.getSwch().getServerId());
                                            tempTopo.getSwitches().add(createSwitch.getSwch());
                                            this.networkDiagramService.setTopologyToAllCurrentTopologiesMap(createSwitch.getSwch().getServerId(), tempTopo);
                                            /*
                                              Check if this is the Last Job or not
                                              IF IT IS ->> increase "numberOfOccure" by one
                                             */
                                            if (i == Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum())) {
                                                numberOfOccure++;
                                            }
                                        } else {
                                            // If job has error set output to 0;
                                            entityInputVal.add("0");
                                        }
                                    } else {
                                        // if the input is '0' don't do the job and immediately set the output to '0'
                                        entityInputVal.add("0");
                                    }
                                } else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(createSwitch.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(1));
                                    }
                                }
                            }
                        }
                    }
                    // ActionCreateVNF
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getCreateVnf().size() > 0) {
                        for (CreateVnfVM createVnf : eventVM.getEventDiagram().getCreateVnf()) {
                            if (createVnf.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(createVnf.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 1 values, NO MORE
                                    if (entityInputVal.size() == 1) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                /*
                                Check If VNF has already created or NOT
                                 */
                                boolean isEntityAlreadyExist = false;
                                System.out.println("Create VNF");
                                for (VnfVM vnf : this.networkDiagramService.getTopologyByNodeIdFromAllCurrenttopologiesMap(createVnf.getVnf().getServerId()).getVnfs()) {
                                    if (vnf.getKey().equals(createVnf.getVnf().getKey())) {
                                        isEntityAlreadyExist = true;
                                        break;
                                    }
                                }
                                if (!isEntityAlreadyExist) {
                                    if (entityInputVal.get(0).equals("1")) {
                                        System.out.println("EXEC Create VNF");
                                        Integer resStatus = this.networkDiagramService.createDockerVnf(
                                            createVnf.getVnf().getServerId(),
                                            createVnf.getVnf().getImgName(),
                                            createVnf.getVnf().getDescribtion().split("#")[1],
                                            createVnf.getVnf().getExecutionCommand(),
                                            createVnf.getVnf().getCpu(),
                                            createVnf.getVnf().getRam(),
                                            createVnf.getVnf().getIsDedicatedRes()
                                        );
                                        if (true) {
                                            entityInputVal.add("1");
                                        /*
                                        Add new VNF to Topology in NetworkDiagramService
                                         */
                                            TopologyVM tempTopo = this.networkDiagramService.getTopologyByNodeIdFromAllCurrenttopologiesMap(createVnf.getVnf().getServerId());
                                            tempTopo.getVnfs().add(createVnf.getVnf());
                                            this.networkDiagramService.setTopologyToAllCurrentTopologiesMap(createVnf.getVnf().getServerId(), tempTopo);
                                            /*
                                              Check if this is the Last Job or not
                                              IF IT IS ->> increase "numberOfOccure" by one
                                             */
                                            if (i == Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum())) {
                                                numberOfOccure++;
                                            }
                                        } else {
                                            // If job has error set output to 0;
                                            entityInputVal.add("0");
                                        }
                                    }
                                    // if the input is '0' don't do the job and immediately set the output to '0'
                                    else {
                                        entityInputVal.add("0");
                                    }
                                } else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(createVnf.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(1));
                                    }
                                }
                            }
                        }
                    }
                    // ActionCreateSwitchPatchPeer
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getCreateSwitchPatchPeer().size() > 0) {
                        for (CreateSwitchPatchPeerVM createSwitchPatchPeer : eventVM.getEventDiagram().getCreateSwitchPatchPeer()) {
                            if (createSwitchPatchPeer.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(createSwitchPatchPeer.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 1 value, NO MORE
                                    if (entityInputVal.size() == 1) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                /*
                                Get current Topology of Specific Server by ID Then Get the 2 Bridges that have to connect
                                to each other
                                 */
                                List<SwitchVM> swchArray = new ArrayList<>();
                                if (entityInputVal.get(0).equals("1")) {
                                    for (SwitchVM swch : this.networkDiagramService.getAllCurrenttopologiesMap().get(createSwitchPatchPeer.getNodeId()).getSwitches()) {
                                        swchArray.add(swch);
                                    }
                                    // the number of Switches must be 2
                                    if (swchArray.size() == 2) {
                                        Integer resStatus = this.networkDiagramService.createBridgePort(
                                            "ovsdb://HOST" + createSwitchPatchPeer.getNodeId(),
                                            swchArray.get(0).getKey().split("#")[1].split("-")[1],
                                            swchArray.get(0).getKey().split("#")[1].split("-")[1] + "_" + swchArray.get(1).getKey().split("#")[1].split("-")[1],
                                            "peer",
                                            swchArray.get(1).getKey().split("#")[1].split("-")[1] + "_" + swchArray.get(0).getKey().split("#")[1].split("-")[1],
                                            "ovsdb:interface-type-patch"
                                        );
                                        if (resStatus < 300 && resStatus >= 200) {
                                            resStatus = this.networkDiagramService.createBridgePort(
                                                "ovsdb://HOST" + createSwitchPatchPeer.getNodeId(),
                                                swchArray.get(1).getKey().split("#")[1].split("-")[1],
                                                swchArray.get(1).getKey().split("#")[1].split("-")[1] + "_" + swchArray.get(0).getKey().split("#")[1].split("-")[1],
                                                "peer",
                                                swchArray.get(0).getKey().split("#")[1].split("-")[1] + "_" + swchArray.get(1).getKey().split("#")[1].split("-")[1],
                                                "ovsdb:interface-type-patch"
                                            );
                                        }
                                        if (resStatus < 300 && resStatus >= 200) {
                                            /*
                                              Check if this is the Last Job or not
                                              IF IT IS ->> increase "numberOfOccure" by one
                                             */
                                            if (i == Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum())) {
                                                numberOfOccure++;
                                            }
                                            entityInputVal.add("1");
                                        } else {
                                            // If job has error set output to 0;
                                            entityInputVal.add("0");
                                        }
                                    } else {
                                        entityInputVal.add("0");
                                    }

                                }
                                // if the input is '0' don't do the job and immediately set the output to '0'
                                else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(createSwitchPatchPeer.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(1));
                                    }
                                }
                            }
                        }
                    }
                    // ActionCreateVNFPort
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getCreateVnfPort().size() > 0) {
                        for (CreateVnfPortVM createVnfPort : eventVM.getEventDiagram().getCreateVnfPort()) {
                            if (createVnfPort.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(createVnfPort.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 1 value, NO MORE
                                    if (entityInputVal.size() == 1) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                if (entityInputVal.get(0).equals("1")) {
                                    Integer resStatus = this.networkDiagramService.createDockerVnfOvsPortByServerId(
                                        createVnfPort.getNodeId(),
                                        createVnfPort.getBridgeName(),
                                        createVnfPort.getContainerName(),
                                        createVnfPort.getInterfaceName(),
                                        createVnfPort.getIpAddress()
                                    );
                                    if (true) {
                                        /*
                                              Check if this is the Last Job or not
                                              IF IT IS ->> increase "numberOfOccure" by one
                                             */
                                        if (i == Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum())) {
                                            numberOfOccure++;
                                        }
                                        entityInputVal.add("1");
                                    } else {
                                        // If job has error set output to 0;
                                        entityInputVal.add("0");
                                    }
                                }
                                // if the input is '0' don't do the job and immediately set the output to '0'
                                else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(createVnfPort.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(1));
                                    }
                                }
                            }
                        }
                    }
                    // ActionUpdateVNF
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getUpdateVnf().size() > 0) {
                        for (UpdateVnfVM updateVnf : eventVM.getEventDiagram().getUpdateVnf()) {
                            if (updateVnf.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(updateVnf.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 1 value, NO MORE
                                    if (entityInputVal.size() == 1) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                if (entityInputVal.get(0).equals("1")) {
                                    Integer resStatus = this.networkDiagramService.updateDockerVnf(
                                        updateVnf.getNodeId(),
                                        updateVnf.getDescribtion().split("#")[1],
                                        updateVnf.getCpu(),
                                        updateVnf.getRam()
                                    );
                                    if (true) {
                                        /*
                                         Check if this is the Last Job or not
                                         IF IT IS ->> increase "numberOfOccure" by one
                                         */
                                        if (i == Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum())) {
                                            numberOfOccure++;
                                        }
                                        entityInputVal.add("1");
                                    } else {
                                        // If job has error set output to 0;
                                        entityInputVal.add("0");
                                    }
                                }
                                // if the input is '0' don't do the job and immediately set the output to '0'
                                else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(updateVnf.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(1));
                                    }
                                }
                            }
                        }
                    }
                    // ActionDeleteSwitch
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getDeleteSwitch().size() > 0) {
                        for (DeleteSwitchVM deleteSwitch : eventVM.getEventDiagram().getDeleteSwitch()) {
                            if (deleteSwitch.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(deleteSwitch.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 1 value, NO MORE
                                    if (entityInputVal.size() == 1) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                /*
                                Check If switch has already Deleted or NOT
                                 */
                                boolean isEntityAlreadyDelete = true;
                                for (SwitchVM swch : this.networkDiagramService.getTopologyByNodeIdFromAllCurrenttopologiesMap(deleteSwitch.getNodeId()).getSwitches()) {
                                    if (swch.getDescribtion().split("#")[1].equals(deleteSwitch.getBridgeName())) {
                                        isEntityAlreadyDelete = false;
                                        break;
                                    }
                                }
                                if (!isEntityAlreadyDelete) {
                                    if (entityInputVal.get(0).equals("1")) {
                                        Integer resStatus = this.networkDiagramService.deleteBridge(
                                            "ovsdb://HOST" + deleteSwitch.getNodeId(),
                                            deleteSwitch.getBridgeName()
                                        );
                                        if (resStatus < 300 && resStatus >= 200) {
                                            /*
                                            Delete switch from network diagram service
                                             */
                                            for (Iterator<SwitchVM> iter = (this.networkDiagramService.getTopologyByNodeIdFromAllCurrenttopologiesMap(deleteSwitch.getNodeId()).getSwitches()).listIterator(); iter.hasNext(); ) {
                                                SwitchVM swch = iter.next();
                                                if (swch.getDescribtion().split("#")[1].equals(deleteSwitch.getBridgeName())) {
                                                    iter.remove();
                                                }
                                            }
                                            /*
                                         Check if this is the Last Job or not
                                         IF IT IS ->> increase "numberOfOccure" by one
                                         */
                                            if (i == Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum())) {
                                                numberOfOccure++;
                                            }
                                            entityInputVal.add("1");

                                        } else {
                                            // If job has error set output to 0;
                                            entityInputVal.add("0");
                                        }
                                    }
                                    // if the input is '0' don't do the job and immediately set the output to '0'
                                    else {
                                        entityInputVal.add("0");
                                    }
                                } else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(deleteSwitch.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(1));
                                    }
                                }
                            }
                        }
                    }
                    // ActionDeleteVNF
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getDeleteVnf().size() > 0) {
                        for (DeleteVnfVM deleteVnf : eventVM.getEventDiagram().getDeleteVnf()) {
                            if (deleteVnf.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(deleteVnf.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 1 value, NO MORE
                                    if (entityInputVal.size() == 1) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                /*
                                Check If switch has already Deleted or NOT
                                 */
                                boolean isEntityAlreadyDelete = true;
                                for (VnfVM vnfVM : this.networkDiagramService.getTopologyByNodeIdFromAllCurrenttopologiesMap(deleteVnf.getNodeId()).getVnfs()) {
                                    if (vnfVM.getDescribtion().split("#")[1].equals(deleteVnf.getVnfName())) {
                                        isEntityAlreadyDelete = false;
                                        break;
                                    }
                                }
                                if (!isEntityAlreadyDelete) {
                                    if (entityInputVal.get(0).equals("1")) {
                                        System.out.println("Exex Delete VNF");
                                        Integer resStatus = this.networkDiagramService.deleteDockerVnf(
                                            deleteVnf.getNodeId(),
                                            deleteVnf.getVnfName()
                                        );
                                        if (true) {
                                            /*
                                            Delete switch from network diagram service
                                             */
                                            for (Iterator<VnfVM> iter = (this.networkDiagramService.getTopologyByNodeIdFromAllCurrenttopologiesMap(deleteVnf.getNodeId()).getVnfs()).listIterator(); iter.hasNext(); ) {
                                                VnfVM vnfVM = iter.next();
                                                if (vnfVM.getDescribtion().split("#")[1].equals(deleteVnf.getVnfName())) {
                                                    iter.remove();
                                                }
                                            }
                                            /*
                                         Check if this is the Last Job or not
                                         IF IT IS ->> increase "numberOfOccure" by one
                                         */
                                            if (i == Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum())) {
                                                numberOfOccure++;
                                            }
                                            entityInputVal.add("1");
                                        } else {
                                            // If job has error set output to 0;
                                            entityInputVal.add("0");
                                        }
                                    }
                                    // if the input is '0' don't do the job and immediately set the output to '0'
                                    else {
                                        entityInputVal.add("0");
                                    }
                                } else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(deleteVnf.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(1));
                                    }
                                }
                            }
                        }
                    }
                    // ActionDeleteSwitchPachPeer
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getDeleteSwitchPatchPeer().size() > 0) {
                        for (DeleteSwitchPatchPeerVM deleteSwitchPatchPeer : eventVM.getEventDiagram().getDeleteSwitchPatchPeer()) {
                            if (deleteSwitchPatchPeer.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(deleteSwitchPatchPeer.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 1 value, NO MORE
                                    if (entityInputVal.size() == 1) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                if (entityInputVal.get(0).equals("1")) {
                                    /*
                                    Get Switches entities from NetworkDiagram Service
                                     */
                                    Integer resStatus = this.networkDiagramService.deleteBridgePort(
                                        "ovsdb://HOST" + deleteSwitchPatchPeer.getNodeId(),
                                        deleteSwitchPatchPeer.getBridgeName1(),
                                        deleteSwitchPatchPeer.getBridgeName1() + "_" + deleteSwitchPatchPeer.getBridgeName2()
                                    );


                                    this.networkDiagramService.deleteBridgePort(
                                        "ovsdb://HOST" + deleteSwitchPatchPeer.getNodeId(),
                                        deleteSwitchPatchPeer.getBridgeName2(),
                                        deleteSwitchPatchPeer.getBridgeName2() + "_" + deleteSwitchPatchPeer.getBridgeName1()
                                    );


                                    if (resStatus < 300 && resStatus >= 200) {
                                        /*
                                         Check if this is the Last Job or not
                                         IF IT IS ->> increase "numberOfOccure" by one
                                         */
                                        if (i == Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum())) {
                                            numberOfOccure++;
                                        }
                                        entityInputVal.add("1");
                                    } else {
                                        // If job has error set output to 0;
                                        entityInputVal.add("0");
                                    }
                                }
                                // if the input is '0' don't do the job and immediately set the output to '0'
                                else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(deleteSwitchPatchPeer.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(1));
                                    }
                                }
                            }
                        }
                    }
                    // ActionDeleteVNFPort
                    // **************************************************************************************************************
                    if (eventVM.getEventDiagram().getDeleteVnfPort().size() > 0) {
                        for (DeleteVnfPortVM deleteVnfPort : eventVM.getEventDiagram().getDeleteVnfPort()) {
                            if (deleteVnfPort.getPriorityNum().equals(String.valueOf(i))) {
                                // Always Has 2 values, NO MORE
                                List<String> entityInputVal = new ArrayList<>();
                                //Fond ports that connected to the entity (input and output)
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find Froms's and Get the Value From them
                                    if (eventDiagramConnection.getKey().split("%")[1].equals(deleteVnfPort.getKey())) {
                                        entityInputVal.add(eventDiagramConnection.getValue());
                                    }
                                    // Always Has 1 value, NO MORE
                                    if (entityInputVal.size() == 1) {
                                        break;
                                    }
                                }
                                // Execute Entity job
                                //---------------------------------
                                if (entityInputVal.get(0).equals("1")) {
                                    /*
                                    Get Switches entities from NetworkDiagram Service
                                     */
                                    Integer resStatus = this.networkDiagramService.deleteDockerVnfOvsPortByServerId(
                                        deleteVnfPort.getNodeId(),
                                        deleteVnfPort.getBridgeName(),
                                        deleteVnfPort.getContainerName(),
                                        deleteVnfPort.getInterfaceName()
                                    );

                                    if (true) {
                                        /*
                                         Check if this is the Last Job or not
                                         IF IT IS ->> increase "numberOfOccure" by one
                                         */
                                        if (i == Integer.parseInt(eventVM.getEventDiagram().getMaxPriorityNum())) {
                                            numberOfOccure++;
                                        }
                                        entityInputVal.add("1");
                                    } else {
                                        // If job has error set output to 0;
                                        entityInputVal.add("0");
                                    }
                                }
                                // if the input is '0' don't do the job and immediately set the output to '0'
                                else {
                                    entityInputVal.add("0");
                                }
                                //---------------------------------
                                //Find output ports and execute command and Set value to them
                                for (EventDiagramEntityConnectionsVM eventDiagramConnection : eventVM.getEventDiagram().getEventDiagramEntityConnection()) {
                                    // Find TOOOO's and set the Value to them
                                    if (eventDiagramConnection.getKey().split("%")[0].equals(deleteVnfPort.getKey())) {
                                        eventDiagramConnection.setValue(entityInputVal.get(1));
                                    }
                                }
                            }
                        }
                    }
                }
            }


            try {
                Thread.sleep(3000);
                for (Topic topicObj : topicObjList) {
                    topicObj.setValue("");
                    topicObj.setMonitoredContainer(null);
                }
            } catch (Exception e) {

            }
        }
    }


    public Long getInterval(String currentTime, String prevTime) {
        try {
            Instant now = Instant.parse(currentTime);
            Instant before = Instant.parse(prevTime);
            // ms -> ns.
            long delta = Duration.between(before, now).toMillis() * 1000000;
            return delta;
        } catch (Exception e) {
            return 1L;
        }
    }

    public Map<String, Object> config() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.1.4:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "Xnet" + Thread.currentThread().getId());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return props;
    }

    public Long getNumberOfOccure() {
        return numberOfOccure;
    }

    public void setNumberOfOccure(Long numberOfOccure) {
        this.numberOfOccure = numberOfOccure;
    }

}
