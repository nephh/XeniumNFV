package com.ahghorab.xenonet.service.util.monitoring;

import java.util.Map;

public class MonitoredContainer {
    private String timestamp;
    private String machine_name;
    private String container_Name;
    private String container_Id;
    private Double cpu;
    private Double ram;
    private Double ramPercentage;
    /*
    Map Interface name to interface Metrics
     */
    private Map<String, Double> totalRx;
    private Map<String, Double> rxPerSec;
    private Map<String, Double> totalTx;
    private Map<String, Double> txPerSec;

    public MonitoredContainer() {
    }

    public MonitoredContainer(String timestamp, String machine_name, String container_Name, String container_Id, Double cpu, Double ram, Double ramPercentage, Map<String, Double> totalRx, Map<String, Double> rxPerSec, Map<String, Double> totalTx, Map<String, Double> txPerSec) {
        this.timestamp = timestamp;
        this.machine_name = machine_name;
        this.container_Name = container_Name;
        this.container_Id = container_Id;
        this.cpu = cpu;
        this.ram = ram;
        this.ramPercentage = ramPercentage;
        this.totalRx = totalRx;
        this.rxPerSec = rxPerSec;
        this.totalTx = totalTx;
        this.txPerSec = txPerSec;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMachine_name() {
        return machine_name;
    }

    public void setMachine_name(String machine_name) {
        this.machine_name = machine_name;
    }

    public String getContainer_Name() {
        return container_Name;
    }

    public void setContainer_Name(String container_Name) {
        this.container_Name = container_Name;
    }

    public String getContainer_Id() {
        return container_Id;
    }

    public void setContainer_Id(String container_Id) {
        this.container_Id = container_Id;
    }

    public Double getCpu() {
        return cpu;
    }

    public void setCpu(Double cpu) {
        this.cpu = cpu;
    }

    public Double getRam() {
        return ram;
    }

    public void setRam(Double ram) {
        this.ram = ram;
    }

    public Double getRamPercentage() {
        return ramPercentage;
    }

    public void setRamPercentage(Double ramPercentage) {
        this.ramPercentage = ramPercentage;
    }

    public Map<String, Double> getTotalRx() {
        return totalRx;
    }

    public void setTotalRx(Map<String, Double> totalRx) {
        this.totalRx = totalRx;
    }

    public Map<String, Double> getRxPerSec() {
        return rxPerSec;
    }

    public void setRxPerSec(Map<String, Double> rxPerSec) {
        this.rxPerSec = rxPerSec;
    }

    public Map<String, Double> getTotalTx() {
        return totalTx;
    }

    public void setTotalTx(Map<String, Double> totalTx) {
        this.totalTx = totalTx;
    }

    public Map<String, Double> getTxPerSec() {
        return txPerSec;
    }

    public void setTxPerSec(Map<String, Double> txPerSec) {
        this.txPerSec = txPerSec;
    }

    public void putTotalRx(String interfaceName, double value) {
        this.totalRx.put(interfaceName, value);
    }

    public void putRxPerSec(String interfaceName, double value) {
        this.rxPerSec.put(interfaceName, value);
    }

    public void putTotalTx(String interfaceName, double value) {
        this.totalTx.put(interfaceName, value);
    }

    public void putTxPerSec(String interfaceName, double value) {
        this.txPerSec.put(interfaceName, value);
    }
}
