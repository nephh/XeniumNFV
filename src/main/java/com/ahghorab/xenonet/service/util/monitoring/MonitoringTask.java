package com.ahghorab.xenonet.service.util.monitoring;

import com.ahghorab.xenonet.service.util.event.Topic;
import com.ahghorab.xenonet.service.util.kafka.MsgProducer;
import com.ahghorab.xenonet.web.rest.vm.cAdvisor.ContainerStatus;
import com.ahghorab.xenonet.web.rest.vm.cAdvisor.VnfNetworkInterface;
import com.ahghorab.xenonet.web.rest.vm.event.notifier.NotifierVNFVM;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class MonitoringTask implements Runnable {

    private Thread worker;
    private final float oneMegabyte = 1024 * 1024;
    private long startingOffset;
    private ObjectMapper objMapper;
    private ObjectWriter objWriter;
    private KafkaConsumer<String, String> consumer;
    private final KafkaProducer<String, String> producer;
    private final AtomicBoolean running = new AtomicBoolean(false);


    private Map<String, String> containerNamePrevTimeStampMap = new HashMap<>();
    private Map<String, Float> containerNamePrevCpuMap = new HashMap<>();
//    private Map<String, Float> containerNamePrevRxMap = new HashMap<>();
//    private Map<String, Float> containerNamePrevTxMap = new HashMap<>();

    /*
    Map<ContainerName, Map<InterfaceName, Value>>
     */
    private Map<String, Map<String, Float>> containerNamePrevRxMap = new HashMap<>();
    private Map<String, Map<String, Float>> containerNamePrevTxMap = new HashMap<>();

    private List<Topic> topicList = new ArrayList<>();

    public MonitoringTask(long startingOffset) {
        this.startingOffset = startingOffset;
        this.consumer = new KafkaConsumer<>(config());
        this.producer = new KafkaProducer<String, String>(producerConfig());
        objMapper = new ObjectMapper();
        objWriter = objMapper.writer();
    }


    public void start() {
        if (!running.get()) {
            worker = new Thread(this);
            running.set(true);
            worker.start();
        }
    }

    public void stop() {
        if (running.get()) {
            consumer.close();
            producer.close();
            running.set(false);
            System.out.println("STOP Thread " + Thread.currentThread().getId());
        }
    }

    public void interrupt() {
        running.set(false);
        consumer.close();
        producer.close();
        worker.interrupt();
    }

    @Override
    public void run() {
        this.consumer.subscribe(Arrays.asList("CAT"), new ConsumerRebalanceListener() {
            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            }

            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                Iterator<TopicPartition> topicPartitionIterator = partitions.iterator();
                while (topicPartitionIterator.hasNext()) {
                    TopicPartition topicPartition = topicPartitionIterator.next();
                    if (startingOffset == 0) {
                        consumer.seekToBeginning(Collections.singletonList(topicPartition));
                    } else if (startingOffset == -1) {
                        consumer.seekToEnd(Collections.singletonList(topicPartition));
                    } else {
                        consumer.seek(topicPartition, startingOffset);
                    }
                }
            }
        });
        System.out.println("START Monitoring Thread " + Thread.currentThread().getId());
        while (running.get()) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
                try {
                    ContainerStatus containerStatus = objMapper.readValue(record.value(), ContainerStatus.class);
                    /*
                    with containerID != null we remove these kind of data from calculation
                    Receive message: {"timestamp":"2018-05-20T05:30:07.304983647Z","machine_name":"a1763ca7c3f0","container_Name":"/system.slice/open-vm-tools.service","container_Id":null,"cpu":0.044675596291199327,"ram":null,"ramPersentage":null,"totalRx":0.0,"rxPerSec":0.0,"totalTx":0.0,"txPerSec":0.0}, Partition: 0, Offset: 389, by ThreadID: 33 TopicT_VNF
                    Receive message: {"timestamp":"2018-05-20T05:30:07.337762958Z","machine_name":"a1763ca7c3f0","container_Name":"/system.slice/iscsid.service","container_Id":null,"cpu":0.011996958346571773,"ram":null,"ramPersentage":null,"totalRx":0.0,"rxPerSec":0.0,"totalTx":0.0,"txPerSec":0.0}, Partition: 0, Offset: 390, by ThreadID: 33 TopicT_VNF
                    Receive message: {"timestamp":"2018-05-20T05:30:07.489806408Z","machine_name":"a1763ca7c3f0","container_Name":"/system.slice/accounts-daemon.service","container_Id":null,"cpu":0.0040790208004182205,"ram":null,"ramPersentage":null,"totalRx":0.0,"rxPerSec":0.0,"totalTx":0.0,"txPerSec":0.0}, Partition: 0, Offset: 391, by ThreadID: 33 TopicT_VNF
                    Receive message: {"timestamp":"2018-05-20T05:30:07.723034249Z","machine_name":"a1763ca7c3f0","container_Name":"/","container_Id":null,"cpu":12.995237112045288,"ram":null,"ramPersentage":null,"totalRx":6725.3984375,"rxPerSec":24544.0,"totalTx":40923.51171875,"txPerSec":47660.0}, Partition: 0, Offset: 392, by ThreadID: 33 TopicT_VNF
                     */
                    if (containerStatus.getContainer_statsObject().getCpuObject().getUsage().getTotal() > 0.0 && !(containerStatus.getContainer_Id() == null)) {
                        /*
                        Init MonitoredContainer object
                         */
                        MonitoredContainer monitoredContainer = new MonitoredContainer();
                        monitoredContainer.setTimestamp(containerStatus.getTimestamp());
                        monitoredContainer.setContainer_Name(containerStatus.getContainer_Name());
                        monitoredContainer.setMachine_name(containerStatus.getMachine_name());
                        monitoredContainer.setContainer_Id(containerStatus.getContainer_Id());
                        monitoredContainer.setTotalRx(new HashMap<>());
                        monitoredContainer.setRxPerSec(new HashMap<>());
                        monitoredContainer.setTotalTx(new HashMap<>());
                        monitoredContainer.setTxPerSec(new HashMap<>());

                        float interval = getInterval(containerStatus.getTimestamp(), containerNamePrevTimeStampMap.get(containerStatus.getContainer_Name()));

                        /*
                        CPU PART
                         */
                        float cpuDelta = -1;
                        if (containerNamePrevCpuMap.get(containerStatus.getContainer_Name()) != null) {
                            cpuDelta = containerStatus.getContainer_statsObject().getCpuObject().getUsage().getTotal() - containerNamePrevCpuMap.get(containerStatus.getContainer_Name());
                        }
                        if (cpuDelta >= 0.0) {
                            double tempCpu = (cpuDelta) / interval * 100.0;
                            /*
                             Cpu must less equal than number of cores * 100 -----> sometime at the beginning the cpu goes up like this 2.37431568E10
                             */
                            if (tempCpu <= containerStatus.getContainer_statsObject().getCpuObject().getUsage().getPer_cpu_usage().size() * 100) {
                                monitoredContainer.setCpu(tempCpu);
                            } else {
                                monitoredContainer.setCpu(-1.0);
                            }
                        } else {
                            monitoredContainer.setCpu(-1.0);
                        }

                        /*
                        RAM PART
                         */
                        if(containerStatus.getContainer_statsObject().getMemoryObject().getUsage() >= 0.0){
                            monitoredContainer.setRam((double) (containerStatus.getContainer_statsObject().getMemoryObject().getUsage() / oneMegabyte));
                        }
                        else{
                            monitoredContainer.setRam(-1.0);
                        }

                        /*
                        Network PART
                         */
                        if (containerStatus.getContainer_statsObject().getNetworkObject().getInterfaces().size() > 0) {
                            for (VnfNetworkInterface vnfNetworkInterface : containerStatus.getContainer_statsObject().getNetworkObject().getInterfaces()) {
                                /*
                                RX PART
                                 */
                                if (containerNamePrevRxMap.get(containerStatus.getContainer_Name()) != null) {
                                    if (containerNamePrevRxMap.get(containerStatus.getContainer_Name()).get(vnfNetworkInterface.getName()) != null) {
                                        monitoredContainer.putRxPerSec(vnfNetworkInterface.getName(),
                                            (double) (vnfNetworkInterface.getRx_bytes() - containerNamePrevRxMap.get(containerStatus.getContainer_Name()).get(vnfNetworkInterface.getName())) / (interval / 1000000000)
                                        );
                                        monitoredContainer.putTotalRx(vnfNetworkInterface.getName(), (double) (vnfNetworkInterface.getRx_bytes()));
                                    }
                                    containerNamePrevRxMap.get(containerStatus.getContainer_Name()).put(vnfNetworkInterface.getName(), vnfNetworkInterface.getRx_bytes());
                                } else {
                                    containerNamePrevRxMap.put(containerStatus.getContainer_Name(), new HashMap<>());
                                }

                                /*
                                TX PART
                                 */
                                if (containerNamePrevTxMap.get(containerStatus.getContainer_Name()) != null) {
                                    if (containerNamePrevTxMap.get(containerStatus.getContainer_Name()).get(vnfNetworkInterface.getName()) != null) {
                                        monitoredContainer.putTxPerSec(vnfNetworkInterface.getName(),
                                            (double) (vnfNetworkInterface.getTx_bytes() - containerNamePrevTxMap.get(containerStatus.getContainer_Name()).get(vnfNetworkInterface.getName())) / (interval / 1000000000)
                                        );
                                        monitoredContainer.putTotalTx(vnfNetworkInterface.getName(), (double) (containerStatus.getContainer_statsObject().getNetworkObject().getTx_bytes()));
                                    }
                                    containerNamePrevTxMap.get(containerStatus.getContainer_Name()).put(vnfNetworkInterface.getName(), vnfNetworkInterface.getTx_bytes());
                                } else {
                                    containerNamePrevTxMap.put(containerStatus.getContainer_Name(), new HashMap<>());
                                }

                            }
                        }

                        containerNamePrevCpuMap.put(containerStatus.getContainer_Name(), containerStatus.getContainer_statsObject().getCpuObject().getUsage().getTotal());
                        containerNamePrevTimeStampMap.put(containerStatus.getContainer_Name(), containerStatus.getTimestamp());

                        /*
                        Send message with "topicName(T_VNF)+_+containerName"
                        T_VNF <-> Topic_VNF
                        */
                        this.producer.send(new ProducerRecord<>("T_VNF" + "_" + containerStatus.getContainer_Name(), objWriter.writeValueAsString(monitoredContainer)));
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
    }


    private static float getInterval(String currentTime, String prevTime) {
        try {
            Instant now = Instant.parse(currentTime);
            Instant before = Instant.parse(prevTime);
            // ms -> ns.
            return Duration.between(before, now).toMillis() * 1000000;
        } catch (Exception e) {
            return 1.0f;
        }
    }

    public Map<String, Object> config() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.1.4:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "Xnet" + Thread.currentThread().getId());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return props;
    }

    public Map<String, Object> producerConfig() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.1.4:9092");
        props.put(ProducerConfig.RETRIES_CONFIG, "0");
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, "16384");
        props.put(ProducerConfig.LINGER_MS_CONFIG, "1");
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return props;
    }

}
