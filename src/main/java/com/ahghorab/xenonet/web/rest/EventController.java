package com.ahghorab.xenonet.web.rest;

import com.ahghorab.xenonet.service.EventService;
import com.ahghorab.xenonet.service.NetworkDiagramService;
import com.ahghorab.xenonet.web.rest.util.HeaderUtil;
import com.ahghorab.xenonet.web.rest.util.PaginationUtil;
import com.ahghorab.xenonet.web.rest.vm.EventDiagramVM;
import com.ahghorab.xenonet.web.rest.vm.ServerVM;
import com.ahghorab.xenonet.web.rest.vm.event.EventVM;
import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/event")
public class EventController {
    private final Logger log = LoggerFactory.getLogger(EventController.class);
    private static final String ENTITY_NAME = "networkDiagram";

    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping("/new")
    @Timed
    public ResponseEntity createTopology(@Valid @RequestBody EventVM event) throws URISyntaxException {
        Integer resInt = this.eventService.createEvent(event);

        if (resInt > -1) {
            return ResponseEntity
                .created(new URI("/api/event/new" + event)).headers(HeaderUtil
                    .createAlert("A Topology is created with identifier ", Long.toString(1)))
                .body(null);
        } else {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "ipMissing", "Server Prime Ip Address Missing"))
                .body(null);
        }

    }

    @PostMapping("/stop/{id}")
    @Timed
    public ResponseEntity stopEventById(@PathVariable Long id){
        this.eventService.stopEvent(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("A event stop with identifier ", Long.toString(id))).build();

    }

    @PostMapping("/start/{id}")
    @Timed
    public ResponseEntity startEventById(@PathVariable Long id) {
        this.eventService.startEvent(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("A event stop with identifier ", Long.toString(id))).build();

    }

    @GetMapping("/getAll")
    @Timed
    public ResponseEntity<List<EventVM>> getAllServer(@ApiParam Pageable pageable) {
        final Page<EventVM> eventVM = eventService.getAllEvents(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(eventVM, "/api/event/getAll");
        return new ResponseEntity<>(eventVM.getContent(), headers, HttpStatus.OK);
    }



    @DeleteMapping("/deleteTopo/{id}")
    @Timed
    public ResponseEntity<Void> deleteEvent(@PathVariable Long id) {
        log.debug("REST request to delete Topology: {}", id);
//        this.networkDiagramService.deleteTopology(id.toString());
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("A Topology is deleted with identifier ", Long.toString(id))).build();
    }

}
