package com.ahghorab.xenonet.web.rest.vm;

public class DiagramEntityConnectionsVM {
    private Integer id;
    private String key;
    private String srcEntityKey;
    private String destEntityKey;
    private String srcInterfaceName;
    private String destInterfaceName;

    public DiagramEntityConnectionsVM(){

    }

    public DiagramEntityConnectionsVM(Integer id, String key, String srcEntityKey, String destEntityKey, String srcInterfaceName, String destInterfaceName) {
        this.id = id;
        this.key = key;
        this.srcEntityKey = srcEntityKey;
        this.destEntityKey = destEntityKey;
        this.srcInterfaceName = srcInterfaceName;
        this.destInterfaceName = destInterfaceName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSrcEntityKey() {
        return srcEntityKey;
    }

    public void setSrcEntityKey(String srcEntityKey) {
        this.srcEntityKey = srcEntityKey;
    }

    public String getDestEntityKey() {
        return destEntityKey;
    }

    public void setDestEntityKey(String destEntityKey) {
        this.destEntityKey = destEntityKey;
    }

    public String getSrcInterfaceName() {
        return srcInterfaceName;
    }

    public void setSrcInterfaceName(String srcInterfaceName) {
        this.srcInterfaceName = srcInterfaceName;
    }

    public String getDestInterfaceName() {
        return destInterfaceName;
    }

    public void setDestInterfaceName(String destInterfaceName) {
        this.destInterfaceName = destInterfaceName;
    }
}
