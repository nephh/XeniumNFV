package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Container_labels {
    @JsonProperty("com.docker.compose.config-hash")
    private String com_docker_compose_config_hash;
    @JsonProperty("com.docker.compose.container-number")
    private String com_docker_compose_container_number;
    @JsonProperty("com.docker.compose.oneoff")
    private String com_docker_compose_oneoff;
    @JsonProperty("com.docker.compose.project")
    private String com_docker_compose_project;
    @JsonProperty("com.docker.compose.service")
    private String com_docker_compose_service;
    @JsonProperty("com.docker.compose.version")
    private String com_docker_compose_version;

    public String getCom_docker_compose_config_hash() {
        return com_docker_compose_config_hash;
    }

    public void setCom_docker_compose_config_hash(String com_docker_compose_config_hash) {
        this.com_docker_compose_config_hash = com_docker_compose_config_hash;
    }

    public String getCom_docker_compose_container_number() {
        return com_docker_compose_container_number;
    }

    public void setCom_docker_compose_container_number(String com_docker_compose_container_number) {
        this.com_docker_compose_container_number = com_docker_compose_container_number;
    }

    public String getCom_docker_compose_oneoff() {
        return com_docker_compose_oneoff;
    }

    public void setCom_docker_compose_oneoff(String com_docker_compose_oneoff) {
        this.com_docker_compose_oneoff = com_docker_compose_oneoff;
    }

    public String getCom_docker_compose_project() {
        return com_docker_compose_project;
    }

    public void setCom_docker_compose_project(String com_docker_compose_project) {
        this.com_docker_compose_project = com_docker_compose_project;
    }

    public String getCom_docker_compose_service() {
        return com_docker_compose_service;
    }

    public void setCom_docker_compose_service(String com_docker_compose_service) {
        this.com_docker_compose_service = com_docker_compose_service;
    }

    public String getCom_docker_compose_version() {
        return com_docker_compose_version;
    }

    public void setCom_docker_compose_version(String com_docker_compose_version) {
        this.com_docker_compose_version = com_docker_compose_version;
    }
}
