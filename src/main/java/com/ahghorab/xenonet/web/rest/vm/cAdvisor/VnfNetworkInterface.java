package com.ahghorab.xenonet.web.rest.vm.cAdvisor;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VnfNetworkInterface {
    @JsonProperty("name")
    private String name;
    @JsonProperty("rx_bytes")
    private float rx_bytes;
    @JsonProperty("rx_packets")
    private float rx_packets;
    @JsonProperty("rx_errors")
    private float rx_errors;
    @JsonProperty("rx_dropped")
    private float rx_dropped;
    @JsonProperty("tx_bytes")
    private float tx_bytes;
    @JsonProperty("tx_packets")
    private float tx_packets;
    @JsonProperty("tx_errors")
    private float tx_errors;
    @JsonProperty("tx_dropped")
    private float tx_dropped;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRx_bytes() {
        return rx_bytes;
    }

    public void setRx_bytes(float rx_bytes) {
        this.rx_bytes = rx_bytes;
    }

    public float getRx_packets() {
        return rx_packets;
    }

    public void setRx_packets(float rx_packets) {
        this.rx_packets = rx_packets;
    }

    public float getRx_errors() {
        return rx_errors;
    }

    public void setRx_errors(float rx_errors) {
        this.rx_errors = rx_errors;
    }

    public float getRx_dropped() {
        return rx_dropped;
    }

    public void setRx_dropped(float rx_dropped) {
        this.rx_dropped = rx_dropped;
    }

    public float getTx_bytes() {
        return tx_bytes;
    }

    public void setTx_bytes(float tx_bytes) {
        this.tx_bytes = tx_bytes;
    }

    public float getTx_packets() {
        return tx_packets;
    }

    public void setTx_packets(float tx_packets) {
        this.tx_packets = tx_packets;
    }

    public float getTx_errors() {
        return tx_errors;
    }

    public void setTx_errors(float tx_errors) {
        this.tx_errors = tx_errors;
    }

    public float getTx_dropped() {
        return tx_dropped;
    }

    public void setTx_dropped(float tx_dropped) {
        this.tx_dropped = tx_dropped;
    }
}
