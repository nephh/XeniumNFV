package com.ahghorab.xenonet.web.rest.vm.event;

public class EventDiagramEntityConnectionsVM {
    private Integer id;
    private String key;
    private String value;
    private String valueType;
    private String priorityNum;
    private String srcEventNodeKey;
    private String destEventNodeKey;
    private String srcEventNodePartName;
    private String destEventNodePartName;

    public EventDiagramEntityConnectionsVM(){

    }

    public EventDiagramEntityConnectionsVM(Integer id, String key, String value, String valueType, String priorityNum, String srcEventNodeKey, String destEventNodeKey, String srcEventNodePartName, String destEventNodePartName) {
        this.id = id;
        this.key = key;
        this.value = value;
        this.valueType = valueType;
        this.priorityNum = priorityNum;
        this.srcEventNodeKey = srcEventNodeKey;
        this.destEventNodeKey = destEventNodeKey;
        this.srcEventNodePartName = srcEventNodePartName;
        this.destEventNodePartName = destEventNodePartName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getPriorityNum() {
        return priorityNum;
    }

    public void setPriorityNum(String priorityNum) {
        this.priorityNum = priorityNum;
    }

    public String getSrcEventNodeKey() {
        return srcEventNodeKey;
    }

    public void setSrcEventNodeKey(String srcEventNodeKey) {
        this.srcEventNodeKey = srcEventNodeKey;
    }

    public String getDestEventNodeKey() {
        return destEventNodeKey;
    }

    public void setDestEventNodeKey(String destEventNodeKey) {
        this.destEventNodeKey = destEventNodeKey;
    }

    public String getSrcEventNodePartName() {
        return srcEventNodePartName;
    }

    public void setSrcEventNodePartName(String srcEventNodePartName) {
        this.srcEventNodePartName = srcEventNodePartName;
    }

    public String getDestEventNodePartName() {
        return destEventNodePartName;
    }

    public void setDestEventNodePartName(String destEventNodePartName) {
        this.destEventNodePartName = destEventNodePartName;
    }
}
