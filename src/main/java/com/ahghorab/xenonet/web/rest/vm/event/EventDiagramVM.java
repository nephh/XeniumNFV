package com.ahghorab.xenonet.web.rest.vm.event;

import com.ahghorab.xenonet.web.rest.vm.BaseEntityVM;
import com.ahghorab.xenonet.web.rest.vm.event.exec.*;
import com.ahghorab.xenonet.web.rest.vm.event.logic.LogicalAndVM;
import com.ahghorab.xenonet.web.rest.vm.event.logic.LogicalOrVM;
import com.ahghorab.xenonet.web.rest.vm.event.math.*;
import com.ahghorab.xenonet.web.rest.vm.event.notifier.NotifierExternalVM;
import com.ahghorab.xenonet.web.rest.vm.event.notifier.NotifierSwitchVM;
import com.ahghorab.xenonet.web.rest.vm.event.notifier.NotifierVNFVM;

import java.util.List;

public class EventDiagramVM extends BaseEntityVM<Integer> {
    private String maxPriorityNum;
    private List<NotifierExternalVM> notifierExternal;
    private List<NotifierSwitchVM> notifierSwitch;
    private List<NotifierVNFVM> notifierVnf;
    private List<LogicalAndVM> logicAnd;
    private List<LogicalOrVM> logicOr;
    private List<MathAddVM> mathAdd;
    private List<MathCompareVM> mathCompare;
    private List<MathDivVM> mathDiv;
    private List<MathMultipleVM> mathMultiple;
    private List<MathStaticValVM> mathStaticVal;
    private List<MathSubVM> mathSub;
    private List<CreateOdlVM> createOdl;
    private List<CreateSwitchVM> createSwitch;
    private List<CreateSwitchPatchPeerVM> createSwitchPatchPeer;
    private List<CreateVnfVM> createVnf;
    private List<CreateVnfPortVM> createVnfPort;
    private List<DeleteOdlVM> deleteOdl;
    private List<DeleteSwitchVM> deleteSwitch;
    private List<DeleteSwitchPatchPeerVM> deleteSwitchPatchPeer;
    private List<DeleteVnfVM> deleteVnf;
    private List<DeleteVnfPortVM> deleteVnfPort;
    private List<UpdateVnfVM> updateVnf;
    private List<EventDiagramEntityConnectionsVM> EventDiagramEntityConnection;

    public EventDiagramVM() {
    }

    public EventDiagramVM(String maxPriorityNum, List<NotifierExternalVM> notifierExternal, List<NotifierSwitchVM> notifierSwitch, List<NotifierVNFVM> notifierVnf, List<LogicalAndVM> logicAnd, List<LogicalOrVM> logicOr, List<MathAddVM> mathAdd, List<MathCompareVM> mathCompare, List<MathDivVM> mathDiv, List<MathMultipleVM> mathMultiple, List<MathStaticValVM> mathStaticVal, List<MathSubVM> mathSub, List<CreateOdlVM> createOdl, List<CreateSwitchVM> createSwitch, List<CreateSwitchPatchPeerVM> createSwitchPatchPeer, List<CreateVnfVM> createVnf, List<CreateVnfPortVM> createVnfPort, List<DeleteOdlVM> deleteOdl, List<DeleteSwitchVM> deleteSwitch, List<DeleteSwitchPatchPeerVM> deleteSwitchPatchPeer, List<DeleteVnfVM> deleteVnf, List<DeleteVnfPortVM> deleteVnfPort, List<UpdateVnfVM> updateVnf, List<EventDiagramEntityConnectionsVM> eventDiagramEntityConnection) {
        this.maxPriorityNum = maxPriorityNum;
        this.notifierExternal = notifierExternal;
        this.notifierSwitch = notifierSwitch;
        this.notifierVnf = notifierVnf;
        this.logicAnd = logicAnd;
        this.logicOr = logicOr;
        this.mathAdd = mathAdd;
        this.mathCompare = mathCompare;
        this.mathDiv = mathDiv;
        this.mathMultiple = mathMultiple;
        this.mathStaticVal = mathStaticVal;
        this.mathSub = mathSub;
        this.createOdl = createOdl;
        this.createSwitch = createSwitch;
        this.createSwitchPatchPeer = createSwitchPatchPeer;
        this.createVnf = createVnf;
        this.createVnfPort = createVnfPort;
        this.deleteOdl = deleteOdl;
        this.deleteSwitch = deleteSwitch;
        this.deleteSwitchPatchPeer = deleteSwitchPatchPeer;
        this.deleteVnf = deleteVnf;
        this.deleteVnfPort = deleteVnfPort;
        this.updateVnf = updateVnf;
        EventDiagramEntityConnection = eventDiagramEntityConnection;
    }

    public EventDiagramVM(Integer id, String createdDate, String updatedDate, String maxPriorityNum, List<NotifierExternalVM> notifierExternal, List<NotifierSwitchVM> notifierSwitch, List<NotifierVNFVM> notifierVnf, List<LogicalAndVM> logicAnd, List<LogicalOrVM> logicOr, List<MathAddVM> mathAdd, List<MathCompareVM> mathCompare, List<MathDivVM> mathDiv, List<MathMultipleVM> mathMultiple, List<MathStaticValVM> mathStaticVal, List<MathSubVM> mathSub, List<CreateOdlVM> createOdl, List<CreateSwitchVM> createSwitch, List<CreateSwitchPatchPeerVM> createSwitchPatchPeer, List<CreateVnfVM> createVnf, List<CreateVnfPortVM> createVnfPort, List<DeleteOdlVM> deleteOdl, List<DeleteSwitchVM> deleteSwitch, List<DeleteSwitchPatchPeerVM> deleteSwitchPatchPeer, List<DeleteVnfVM> deleteVnf, List<DeleteVnfPortVM> deleteVnfPort, List<UpdateVnfVM> updateVnf, List<EventDiagramEntityConnectionsVM> eventDiagramEntityConnection) {
        super(id, createdDate, updatedDate);
        this.maxPriorityNum = maxPriorityNum;
        this.notifierExternal = notifierExternal;
        this.notifierSwitch = notifierSwitch;
        this.notifierVnf = notifierVnf;
        this.logicAnd = logicAnd;
        this.logicOr = logicOr;
        this.mathAdd = mathAdd;
        this.mathCompare = mathCompare;
        this.mathDiv = mathDiv;
        this.mathMultiple = mathMultiple;
        this.mathStaticVal = mathStaticVal;
        this.mathSub = mathSub;
        this.createOdl = createOdl;
        this.createSwitch = createSwitch;
        this.createSwitchPatchPeer = createSwitchPatchPeer;
        this.createVnf = createVnf;
        this.createVnfPort = createVnfPort;
        this.deleteOdl = deleteOdl;
        this.deleteSwitch = deleteSwitch;
        this.deleteSwitchPatchPeer = deleteSwitchPatchPeer;
        this.deleteVnf = deleteVnf;
        this.deleteVnfPort = deleteVnfPort;
        this.updateVnf = updateVnf;
        EventDiagramEntityConnection = eventDiagramEntityConnection;
    }

    public String getMaxPriorityNum() {
        return maxPriorityNum;
    }

    public void setMaxPriorityNum(String maxPriorityNum) {
        this.maxPriorityNum = maxPriorityNum;
    }

    public List<NotifierExternalVM> getNotifierExternal() {
        return notifierExternal;
    }

    public void setNotifierExternal(List<NotifierExternalVM> notifierExternal) {
        this.notifierExternal = notifierExternal;
    }

    public List<NotifierSwitchVM> getNotifierSwitch() {
        return notifierSwitch;
    }

    public void setNotifierSwitch(List<NotifierSwitchVM> notifierSwitch) {
        this.notifierSwitch = notifierSwitch;
    }

    public List<NotifierVNFVM> getNotifierVnf() {
        return notifierVnf;
    }

    public void setNotifierVnf(List<NotifierVNFVM> notifierVnf) {
        this.notifierVnf = notifierVnf;
    }

    public List<LogicalAndVM> getLogicAnd() {
        return logicAnd;
    }

    public void setLogicAnd(List<LogicalAndVM> logicAnd) {
        this.logicAnd = logicAnd;
    }

    public List<LogicalOrVM> getLogicOr() {
        return logicOr;
    }

    public void setLogicOr(List<LogicalOrVM> logicOr) {
        this.logicOr = logicOr;
    }

    public List<MathAddVM> getMathAdd() {
        return mathAdd;
    }

    public void setMathAdd(List<MathAddVM> mathAdd) {
        this.mathAdd = mathAdd;
    }

    public List<MathCompareVM> getMathCompare() {
        return mathCompare;
    }

    public void setMathCompare(List<MathCompareVM> mathCompare) {
        this.mathCompare = mathCompare;
    }

    public List<MathDivVM> getMathDiv() {
        return mathDiv;
    }

    public void setMathDiv(List<MathDivVM> mathDiv) {
        this.mathDiv = mathDiv;
    }

    public List<MathMultipleVM> getMathMultiple() {
        return mathMultiple;
    }

    public void setMathMultiple(List<MathMultipleVM> mathMultiple) {
        this.mathMultiple = mathMultiple;
    }

    public List<MathStaticValVM> getMathStaticVal() {
        return mathStaticVal;
    }

    public void setMathStaticVal(List<MathStaticValVM> mathStaticVal) {
        this.mathStaticVal = mathStaticVal;
    }

    public List<MathSubVM> getMathSub() {
        return mathSub;
    }

    public void setMathSub(List<MathSubVM> mathSub) {
        this.mathSub = mathSub;
    }

    public List<CreateOdlVM> getCreateOdl() {
        return createOdl;
    }

    public void setCreateOdl(List<CreateOdlVM> createOdl) {
        this.createOdl = createOdl;
    }

    public List<CreateSwitchVM> getCreateSwitch() {
        return createSwitch;
    }

    public void setCreateSwitch(List<CreateSwitchVM> createSwitch) {
        this.createSwitch = createSwitch;
    }

    public List<CreateSwitchPatchPeerVM> getCreateSwitchPatchPeer() {
        return createSwitchPatchPeer;
    }

    public void setCreateSwitchPatchPeer(List<CreateSwitchPatchPeerVM> createSwitchPatchPeer) {
        this.createSwitchPatchPeer = createSwitchPatchPeer;
    }

    public List<CreateVnfVM> getCreateVnf() {
        return createVnf;
    }

    public void setCreateVnf(List<CreateVnfVM> createVnf) {
        this.createVnf = createVnf;
    }

    public List<CreateVnfPortVM> getCreateVnfPort() {
        return createVnfPort;
    }

    public void setCreateVnfPort(List<CreateVnfPortVM> createVnfPort) {
        this.createVnfPort = createVnfPort;
    }

    public List<DeleteOdlVM> getDeleteOdl() {
        return deleteOdl;
    }

    public void setDeleteOdl(List<DeleteOdlVM> deleteOdl) {
        this.deleteOdl = deleteOdl;
    }

    public List<DeleteSwitchVM> getDeleteSwitch() {
        return deleteSwitch;
    }

    public void setDeleteSwitch(List<DeleteSwitchVM> deleteSwitch) {
        this.deleteSwitch = deleteSwitch;
    }

    public List<DeleteSwitchPatchPeerVM> getDeleteSwitchPatchPeer() {
        return deleteSwitchPatchPeer;
    }

    public void setDeleteSwitchPatchPeer(List<DeleteSwitchPatchPeerVM> deleteSwitchPatchPeer) {
        this.deleteSwitchPatchPeer = deleteSwitchPatchPeer;
    }

    public List<DeleteVnfVM> getDeleteVnf() {
        return deleteVnf;
    }

    public void setDeleteVnf(List<DeleteVnfVM> deleteVnf) {
        this.deleteVnf = deleteVnf;
    }

    public List<DeleteVnfPortVM> getDeleteVnfPort() {
        return deleteVnfPort;
    }

    public void setDeleteVnfPort(List<DeleteVnfPortVM> deleteVnfPort) {
        this.deleteVnfPort = deleteVnfPort;
    }

    public List<UpdateVnfVM> getUpdateVnf() {
        return updateVnf;
    }

    public void setUpdateVnf(List<UpdateVnfVM> updateVnf) {
        this.updateVnf = updateVnf;
    }

    public List<EventDiagramEntityConnectionsVM> getEventDiagramEntityConnection() {
        return EventDiagramEntityConnection;
    }

    public void setEventDiagramEntityConnection(List<EventDiagramEntityConnectionsVM> eventDiagramEntityConnection) {
        EventDiagramEntityConnection = eventDiagramEntityConnection;
    }
}
