package com.ahghorab.xenonet.web.rest.vm.pojo.node;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DatapathTypeEntry {
    @JsonProperty("datapath-type")
    private String datapathType;

    public String getDatapathType() {
        return datapathType;
    }

    public void setDatapathType(String datapathType) {
        this.datapathType = datapathType;
    }
}
