package com.ahghorab.xenonet.web.rest.wrapper;

import java.util.Map;

public class ServerIdIp {
    String serverId;
    String serverPrimeIp;

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getServerPrimeIp() {
        return serverPrimeIp;
    }

    public void setServerPrimeIp(String serverPrimeIp) {
        this.serverPrimeIp = serverPrimeIp;
    }
}
