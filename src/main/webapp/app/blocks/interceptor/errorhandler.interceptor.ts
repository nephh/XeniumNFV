import {HttpInterceptor, EventManager} from 'ng-jhipster';
import {RequestOptionsArgs, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {HttpEvent, HttpResponse} from '@angular/common/http';
import {NotificationService} from '../../shared/utils/notification.service';
import {Injector} from '@angular/core';

// import { AlertToasterType } from '../../shared/alert/alert-toaster.enum'

export class ErrorHandlerInterceptor extends HttpInterceptor {
    private errorTxt: string;
    private notificationService: NotificationService;

    constructor(private eventManager: EventManager, private injector: Injector) {
        super();
        setTimeout(() => (this.notificationService = injector.get(NotificationService)));
    }


    requestIntercept(options?: RequestOptionsArgs): RequestOptionsArgs {
        return options;
    }

    responseIntercept(observable: Observable<Response>): Observable<Response> {
        return <Observable<Response>>observable.catch((event) => {
            if (!(event.status === 401)){
                const arr = event.headers.keys();
                let alertParams = null;
                arr.forEach(entry => {
                    if (entry.endsWith('-error')) {
                        this.errorTxt = event.headers.get(entry);
                    } else if (entry.endsWith('app-params')) {
                        alertParams = event.headers.get(entry);
                    }
                });
                this.notificationService.smallBox({
                    title: this.errorTxt ? this.errorTxt : 'Undefined Error',
                    content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
                    color: '#a00002',
                    iconSmall: 'fa fa-times  bounce animated',
                    timeout: 8000
                });

            }
            // this.eventManager.broadcast(
            //     {
            //         name: '',
            //         content: {
            //             message: error.text(),
            //             type: AlertToasterType.error
            //         }
            //     });
            return Observable.throw(event);
        });
    }
}
