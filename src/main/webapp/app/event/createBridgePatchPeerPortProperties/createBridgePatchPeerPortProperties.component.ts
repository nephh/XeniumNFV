import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Server} from '../../servers/server.model';
import {CreateBridgePatchPeerPortProperties} from './createBridgePatchPeerPortProperties.model';

@Component({
    selector: "Xnet-createBridgePatchPeerPortPropertiesComponent",
    styles: [],
    templateUrl: "./createBridgePatchPeerPortProperties.component.html"
})
export class  CreateBridgePatchPeerPortPropertiesComponent{
    @Input() createBridgePortProperties: CreateBridgePatchPeerPortProperties;
    @Input() servers: Server[];
    @Output() applyChange = new EventEmitter();


    constructor() {}

    emitApplyChange() {
        this.applyChange.emit(this.createBridgePortProperties);
    }
}
