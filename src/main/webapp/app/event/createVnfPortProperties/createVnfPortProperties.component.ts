import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Server} from '../../servers/server.model';
import {CreateVnfPortProperties} from './createVnfPortProperties.model';

@Component({
    selector: "Xnet-createVnfPortPropertiesComponent",
    styles: [],
    templateUrl: "./createVnfPortProperties.component.html"
})
export class  CreateVnfPortPropertiesComponent{
    @Input() createVnfPortProperties: CreateVnfPortProperties;
    @Input() servers: Server[];
    @Output() applyChange = new EventEmitter();


    constructor() {}

    emitApplyChange() {
        this.applyChange.emit(this.createVnfPortProperties);
    }
}
