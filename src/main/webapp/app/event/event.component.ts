import {Component, OnInit, AfterContentInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventManager, PaginationUtil, ParseLinks} from 'ng-jhipster';
import {Principal} from '../shared/auth/principal.service';
import {ResponseWrapper} from '../shared/model/response-wrapper.model';
import {PaginationConfig} from '../blocks/config/uib-pagination.config';
import {NotificationService} from '../shared/utils/notification.service';
import {Event} from './event.model';
import {EventService} from './event.service';

declare var $: any;

@Component({
    selector: 'app-server',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit, AfterContentInit {
    public events: Event[];
    currentAccount: any;
    error: any;
    success: any;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;


    constructor(private eventService: EventService,
                private parseLinks: ParseLinks,
                // private alertService: AlertService,
                private principal: Principal,
                private eventManager: EventManager,
                private paginationUtil: PaginationUtil,
                private paginationConfig: PaginationConfig,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private notificationService: NotificationService,
                // private terminalService: TerminalService
    ) {
        this.itemsPerPage = 10;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });

        // this.eventManager.subscribe('serverEditEvent', response=>{
        //     this.notificationService.smallBox({
        //         title: 'Server Registered successfully',
        //         content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
        //         color: '#16A085',
        //         iconSmall: 'fa fa-check bounce animated',
        //         timeout: 4000
        //     });
        //     this.loadAll();
        // });

    }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.currentAccount = account;
            this.loadAll();
        });
    }

    ngAfterContentInit() {
    }

    ngOnDestroy() {
        this.routeData.unsubscribe();
    }

    loadAll() {
        this.eventService
            .getAllFrequently({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
    }

    trackIdentity(index, item: Event) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/events'], {
            queryParams: {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    private onSuccess(events, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.events = events;
    }

    private onError(error) {
        // this.alertService.error(error.error, error.message, null);
    }

    viewServer(id: number) {
    }

    deleteServer(id: number) {
        this.notificationService.smartMessageBox(
            {
                title:
                '<i class=\'fa fa-sign-out txt-color-orangeDark\' style=\'font-family: \'IranSansNormal\' !important;font-size: 9px;\'></i> Delete This Server <span class=\'txt-color-orangeDark\'><strong>' +
                $('#show-shortcut').text() +
                '</strong></span> ?',
                content: '',
                buttons: '[No][Yes]'
            },
            ButtonPressed => {
                if (ButtonPressed == 'Yes') {
                    this.eventService.delete('/delete', id).subscribe(
                        (res: ResponseWrapper) => {
                            this.loadAll();
                        },
                        (res: ResponseWrapper) => {
                            console.log('Cant Delete Server');
                        }
                    );
                }
            }
        );
    }

    editServer(id: number) {
    }

    // onKeyUp($event){
    //     let charCode = String.fromCharCode($event.which).toLowerCase();
    //
    //     if ($event.ctrlKey && charCode === 'c') {
    //         console.log('Ctrl + C pressed');
    //     } else if ($event.ctrlKey && charCode === 'v') {
    //         console.log('Ctrl + V pressed');
    //     } else if ($event.ctrlKey && charCode === 's') {
    //         $event.preventDefault();
    //         console.log('Ctrl + S pressed');
    //     }
    // }
    toggleEventStatus(id: number) {

        if ($(document.getElementById('toggleEventStatus' + id)).hasClass('play')) {
            /*
            Stop Event
             */
            this.eventService.stopEvent(id).subscribe(
                (res: ResponseWrapper) => this.onSuccessEventStop(res.json, res.headers, id),
                (res: ResponseWrapper) => this.onErrorEventStop(res.json)
            );
        } else {
            /*
            Start Event
             */
            this.eventService.startEvent(id).subscribe(
                (res: ResponseWrapper) => this.onSuccessEventStart(res.json, res.headers, id),
                (res: ResponseWrapper) => this.onErrorEventStart(res.json)
            );
        }
    }

    onSuccessEventStop(res, resHeader, id) {
        console.log('on stop suc');
        $(document.getElementById('toggleEventStatus' + id)).removeClass('play');
        $(document.getElementById('toggleEventStatus' + id)).addClass('pause');
    }

    onErrorEventStop(res) {
    }

    onSuccessEventStart(res, resHeader, id) {
        console.log('on start suc');
        $(document.getElementById('toggleEventStatus' + id)).removeClass('pause');
        $(document.getElementById('toggleEventStatus' + id)).addClass('play');
    }

    onErrorEventStart(res) {
    }
}
