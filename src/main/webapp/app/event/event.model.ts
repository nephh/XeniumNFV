/*
*
*    @ AH.GHORAB
*
*/
import {BaseEntity} from '../shared/baseEntity/baseEntity.model';
import {EventDiagram} from './eventDiagram.model';

export class Event extends BaseEntity<number> {
    constructor(public name?: string,
                public eventDiagram ?: EventDiagram,
                public status?: string,
                public numberOfOccur?: string,
                public eventJson?: string) {
        super();
        this.name = name ? name : '';
        this.eventDiagram = eventDiagram ? eventDiagram : new EventDiagram();
        this.status = status ? status : '';
        this.numberOfOccur = numberOfOccur ? numberOfOccur : '';
        this.eventJson = eventJson ? eventJson : '';
    }
}
