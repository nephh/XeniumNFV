/*
*
*    @ AH.GHORAB
*
*/
import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ResolvePagingParams} from '../shared/paginationParamResolver/resolvePagingParams.service';
import {EventComponent} from './event.component';
import {EventEditComponent} from './event-edit.component';

const routes: Routes = [
    {
        path: "",
        component: EventComponent,
        resolve: {
            pagingParams: ResolvePagingParams
        },
        data: {},
        // canActivate: [UserCanActivate]
    },
    // {
    //     path: "servers/edit/:id",
    //     component: ServerPopupOpennerComponent,
    // },
    {
        path: "events/new",
        component: EventEditComponent,
        resolve: {
            pagingParams: ResolvePagingParams
        },
    }
];

export const EventRoute: ModuleWithProviders = RouterModule.forChild(routes);
