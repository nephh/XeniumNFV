export class EventDiagramEntityConnection {
    constructor(public id?: number,
                public key?: string,
                public value?: string,
                public valueType?: string,
                public priorityNum?: string,
                public srcEventNodeKey?: string,
                public destEventNodeKey?: string,
                public srcEventNodePartName?: string,
                public destEventNodePartName?: string) {
        this.id = id ? id : null;
        this.key = key ? key : '';
        this.srcEventNodeKey = srcEventNodeKey ? srcEventNodeKey : '';
        this.destEventNodeKey = destEventNodeKey ? destEventNodeKey : '';
        this.srcEventNodePartName = srcEventNodePartName ? srcEventNodePartName : '';
        this.destEventNodePartName = destEventNodePartName ? destEventNodePartName : '';
        this.value = value ? value : '';
        this.valueType = valueType ? valueType : '';
        this.priorityNum = priorityNum ? priorityNum : '';
    }
}
