/*
*
*    @ AH.GHORAB
*
*/
import {BaseEntity} from '../../../shared/baseEntity/baseEntity.model';
import {EventNodePort} from '../eventNodePort.model';
import {EventDiagramEntityConnection} from '../eventDiagramEntityConnection.model';

export class NotifierVNF extends BaseEntity<number> {
    constructor(
        public key?: string,
        public name?: string,
        public describtion?: string,
        public loc?: string,
        public leftArray?: EventNodePort[],
        public topArray?: EventNodePort[],
        public bottomArray?: EventNodePort[],
        public rightArray?: EventNodePort[],
        public group?: string,
        public category?: string,
        public priorityNum?: string,
        public topicName?: string,
        public containerName?: string,
        public metric?: string,
        public interfaceName?: string,
        public eventDiagramEntityConnections?: EventDiagramEntityConnection[]
    ) {
        super();
        this.id = -1;
        this.key = key ? key : '';
        this.name = name ? name : '';
        this.describtion = describtion ? describtion : '';
        this.loc = loc ? loc : '';
        this.leftArray = leftArray ? leftArray : [];
        this.topArray = topArray ? topArray : [];
        this.bottomArray = bottomArray ? bottomArray : [];
        this.rightArray = rightArray ? rightArray : [];
        this.group = group? group : '';
        this.category = category? category : '';
        this.priorityNum = priorityNum ? priorityNum : '';
        this.topicName = topicName ? topicName : '';
        this.containerName = containerName ? containerName : '';
        this.metric = metric ? metric : '';
        this.interfaceName = interfaceName ? interfaceName : '';
        this.eventDiagramEntityConnections = eventDiagramEntityConnections ? eventDiagramEntityConnections : new Array();
    }
}
