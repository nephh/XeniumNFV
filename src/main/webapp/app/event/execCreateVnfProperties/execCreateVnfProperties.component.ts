import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Vnf} from '../../networkDiagram/accessory-vnfs/vnf.model';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {EventManager} from 'ng-jhipster';
import {NodePort} from '../../networkDiagram/accessory-vnfs/nodePort.model';
import {Server} from '../../servers/server.model';

@Component({
    selector: 'Xnet-execCreateVnfPropertiesComponent',
    styles: [],
    templateUrl: './execCreateVnfProperties.component.html'
})
export class ExecCreateVnfPropertiesComponent implements OnInit {
    @Input() objVnf: Vnf;
    @Input() servers: Server[];
    @Output() applyChange = new EventEmitter();
    public isDedicatedres: boolean = false;
    public networkCards: FormGroup;
    knOptions = {
        readOnly: false,
        size: 80,
        unit: '',
        textColor: '#000000',
        fontSize: '32',
        fontWeigth: '700',
        fontFamily: 'Roboto',
        valueformat: 'percent',
        step: 0.1,
        value: 0,
        max: 10,
        trackWidth: 19,
        barWidth: 20,
        trackColor: '#D8D8D8',
        barColor: '#16A085',
        displayInput: false,
        displayPrevious: false
    };

    constructor(private formBuilder: FormBuilder,
                private eventManager: EventManager) {
    }

    ngOnInit() {
        this.isDedicatedres = this.objVnf.isDedicatedRes === "1" ? true : false;
        this.networkCards = this.formBuilder.group({
            itemRows: this.formBuilder.array([this.initItemRows()])
        });
        this.deleteRow(0);
        if (this.objVnf.leftArray.length > 0) {
            this.addNewRow(this.objVnf.leftArray, 'LEFT');
        }
        if (this.objVnf.topArray.length > 0) {
            this.addNewRow(this.objVnf.topArray, 'TOP');
        }
        if (this.objVnf.rightArray.length > 0) {
            this.addNewRow(this.objVnf.rightArray, 'RIGHT');
        }
        if (this.objVnf.bottomArray.length > 0) {
            this.addNewRow(this.objVnf.bottomArray, 'BOTTOM');
        }

    }

    initItemRows() {
        return this.formBuilder.group({
            itemname: [''],
            itemside: ['']
        });
    }

    addNewRow(nodePorts: NodePort[], side: string) {
        const control = <FormArray>this.networkCards.controls['itemRows'];
        if (nodePorts) {
            if (side === 'LEFT') {
                nodePorts.forEach(nodePort => {
                    control.push(
                        this.formBuilder.group({
                            itemname: nodePort.name,
                            itemside: '3'
                        })
                    );
                });
            }
            else if (side === 'TOP') {
                nodePorts.forEach(nodePort => {
                    control.push(
                        this.formBuilder.group({
                            itemname: nodePort.name,
                            itemside: '0'
                        })
                    );
                });
            }
            else if (side === 'RIGHT') {
                nodePorts.forEach(nodePort => {
                    control.push(
                        this.formBuilder.group({
                            itemname: nodePort.name,
                            itemside: '1'
                        })
                    );
                });
            }
            else if (side === 'BOTTOM') {
                nodePorts.forEach(nodePort => {
                    control.push(
                        this.formBuilder.group({
                            itemname: nodePort.name,
                            itemside: '2'
                        })
                    );
                });
            }
        } else {
            control.push(this.initItemRows());
        }

    }

    deleteRow(index: number) {
        const control = <FormArray>this.networkCards.controls['itemRows'];
        control.removeAt(index);
    }

    emitApplyChange() {
        this.objVnf.isDedicatedRes = this.isDedicatedres ? '1' : '0';
        if(!this.isDedicatedres){
            this.objVnf.cpu = '0';
            this.objVnf.ram = '0';
        }
        this.objVnf.topArray = [];
        this.objVnf.rightArray = [];
        this.objVnf.bottomArray = [];
        this.objVnf.leftArray = [];
        this.networkCards.value.itemRows.forEach(element => {
            // TOP
            if (element.itemside === '0') {
                let isCardNameDuplicated: boolean = false;
                this.objVnf.topArray.forEach(el => {
                    if (el.name === element.itemname) {
                        isCardNameDuplicated = true;
                    }
                });
                if (!isCardNameDuplicated && element.itemname !== null && element.itemname !== '') {
                    this.objVnf.topArray.push(new NodePort(
                        '#425e5c',
                        element.itemname,
                        element.itemname,
                        '',
                        ''
                        )
                    );
                }
            }
            // RIGHT
            else if (element.itemside === '1') {
                let isCardNameDuplicated: boolean = false;
                this.objVnf.rightArray.forEach(el => {
                    if (el.name === element.itemname) {
                        isCardNameDuplicated = true;
                    }
                });
                if (!isCardNameDuplicated && element.itemname !== null && element.itemname !== '') {
                    this.objVnf.rightArray.push(new NodePort(
                        '#425e5c',
                        element.itemname,
                        element.itemname,
                        '',
                        ''
                        )
                    );
                }
            }
            // BOTTOM
            else if (element.itemside === '2') {
                let isCardNameDuplicated: boolean = false;
                this.objVnf.bottomArray.forEach(el => {
                    if (el.name === element.itemname) {
                        isCardNameDuplicated = true;
                    }
                });
                if (!isCardNameDuplicated && element.itemname !== null && element.itemname !== '') {
                    this.objVnf.bottomArray.push(new NodePort(
                        '#425e5c',
                        element.itemname,
                        element.itemname,
                        '',
                        ''
                        )
                    );
                }
            }
            // LEFT
            else {
                let isCardNameDuplicated: boolean = false;
                this.objVnf.leftArray.forEach(el => {
                    if (el.name === element.itemname) {
                        isCardNameDuplicated = true;
                    }
                });
                if (!isCardNameDuplicated && element.itemname !== null && element.itemname !== '') {
                    this.objVnf.leftArray.push(new NodePort(
                        '#425e5c',
                        element.itemname,
                        element.itemname,
                        '',
                        ''
                        )
                    );
                }
            }
        });
        this.applyChange.emit(this.objVnf);
    }
}
