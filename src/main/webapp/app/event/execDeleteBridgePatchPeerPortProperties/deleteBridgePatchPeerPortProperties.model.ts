export class DeleteBridgePatchPeerPortProperties {
    constructor(public serverId?: string,
                public bridgeName1?: string,
                public bridgeName2?: string) {
        this.serverId = serverId ? serverId : '';
        this.bridgeName1 = bridgeName1 ? bridgeName1 : '';
        this.bridgeName2 = bridgeName2 ? bridgeName2 : '';
    }
}
