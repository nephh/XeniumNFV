import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Server} from '../../servers/server.model';
import {DeleteBridgePatchPeerPortProperties} from './deleteBridgePatchPeerPortProperties.model';

@Component({
    selector: "Xnet-execDeleteBridgePatchPeerPortPropertiesComponent",
    styles: [],
    templateUrl: "./execDeleteBridgePatchPeerPortProperties.component.html"
})
export class  ExecDeleteBridgePatchPeerPortPropertiesComponent{
    @Input() deleteBridgePatchPeerPortProperties: DeleteBridgePatchPeerPortProperties;
    @Input() servers: Server[];
    @Output() applyChange = new EventEmitter();


    constructor() {}

    emitApplyChange() {
        this.applyChange.emit(this.deleteBridgePatchPeerPortProperties);
    }
}
