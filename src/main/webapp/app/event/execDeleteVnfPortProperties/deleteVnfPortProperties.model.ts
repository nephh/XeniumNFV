export class DeleteVnfPortProperties{
    constructor(public serverId?: string,
                public containerName?: string,
                public bridgeName?: string,
                public interfaceName?: string,
    ) {
        this.serverId = serverId ? serverId : '';
        this.containerName = containerName ? containerName : '';
        this.bridgeName = bridgeName ? bridgeName : '';
        this.interfaceName = interfaceName ? interfaceName : '';
    }
}
