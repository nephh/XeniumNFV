import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MathStaticProperties} from './mathStaticProperties.model';

@Component({
    selector: "Xnet-mathStaticPropertiesComponent",
    styles: [],
    templateUrl: "./mathStaticProperties.component.html"
})
export class MathStaticPropertiesComponent {
    @Input()
    mathStaticProperties: MathStaticProperties;
    @Output() applyChange = new EventEmitter();


    constructor() {}

    emitApplyChange() {
        this.applyChange.emit(this.mathStaticProperties);
    }
}
