export class UpdateVnfProperties {
    constructor(public serverId?: string,
                public containerName?: string,
                public cpu?: string,
                public ram?: string,
    ) {
        this.serverId = serverId ? serverId : '';
        this.containerName = containerName ? containerName : '';
        this.cpu = cpu ? cpu : '';
        this.ram = ram ? ram : '';
    }
}
