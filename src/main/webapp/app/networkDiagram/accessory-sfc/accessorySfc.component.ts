import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    AfterViewInit
} from '@angular/core';

import * as go from 'gojs';

const $$ = go.GraphObject.make;

@Component({
    selector: 'accessory-sfc',
    templateUrl: './accessorySfc.component.html'
})
export class AccessorySfc extends go.Link implements OnInit, AfterViewInit {
    @ViewChild('plateSfcAccessory') plateSfcAccessoryDiv: ElementRef;

    public sampleSfc = require('../../../content/img/XenoNet/sampleSfc.png');
    private plateSfcAccessory : any;
    private selectedEntity;
    public __this = this;

    constructor() {
        super();
    }

    ngAfterViewInit() {
    }

    ngOnInit() {
        ///////////////////////////////////////////////////////
         this.plateSfcAccessory = $$(
            go.Palette,
            this.plateSfcAccessoryDiv.nativeElement.id,
            {});

        this.plateSfcAccessory.addDiagramListener('BackgroundSingleClicked', e => {
            this.selectedEntity = null;
        });

        this.plateSfcAccessory.addDiagramListener('ObjectSingleClicked', e => {
            var part = e.subject.part;
            if (!(part instanceof go.Link)) {
                this.selectedEntity = part;
            }
        });


        this.plateSfcAccessory.groupTemplate =
            $$(go.Group, go.Panel.Auto,
                {
                    isSubGraphExpanded: false,  // only show the Group itself, not any of its members
                    ungroupable: true,
                    layout:
                        $$(go.ForceDirectedLayout,
                            {
                                isInitial: false,
                            }),

                },  // allow the ExternalObjectsDropped event handler to ungroup
                // the members to be top-level parts, via a command
                $$(
                    go.Panel,
                    'Auto',
                    {
                        row: 1,
                        column: 1,
                        name: 'BODY',
                        stretch: go.GraphObject.Fill
                    },

                    $$(go.Shape, 'Rectangle', {
                        fill: '#ffffff',
                        strokeWidth: 1,
                        minSize: new go.Size(65, 75)
                    }),

                    $$(
                        go.Picture,this.sampleSfc,
                        {
                            // margin: 20,
                            width: 60,
                            height: 70,
                        },
                        // new go.Binding('source')
                    ),
                    $$(
                        go.TextBlock,
                        {
                            margin: 2,
                            verticalAlignment: go.Spot.Bottom,
                            font: '14px  Segoe UI,sans-serif',
                            stroke: 'black',
                            textAlign: 'center',
                            width: 65,
                            height: 110,
                            editable: true
                        },
                        new go.Binding('text', 'text').makeTwoWay()
                    )
                ) // end Auto Panel body



            );


        this.plateSfcAccessory.nodeTemplate = $$(
            go.Node,
            'Horizontal',
            // the body
            $$(
                go.Panel,
                'Auto',
                {
                    row: 1,
                    column: 1,
                    name: 'BODY',
                    stretch: go.GraphObject.Fill
                },

                $$(go.Shape, 'Rectangle', {
                    fill: '#ffffff',
                    strokeWidth: 1,
                    minSize: new go.Size(65, 75)
                }),

                $$(
                    go.Picture,this.sampleSfc,
                    {
                        // margin: 20,
                        width: 60,
                        height: 70,
                    },
                    // new go.Binding('source')
                ),
                $$(
                    go.TextBlock,
                    {
                        margin: 2,
                        verticalAlignment: go.Spot.Top,
                        font: '14px  Segoe UI,sans-serif',
                        stroke: 'black',
                        textAlign: 'center',
                        width: 65,
                        height: 110,
                        editable: true
                    },
                    new go.Binding('text', 'name').makeTwoWay()
                ),
                $$(
                    go.TextBlock,
                    {
                        margin: 2,
                        verticalAlignment: go.Spot.Bottom,
                        font: '14px  Segoe UI,sans-serif',
                        stroke: 'black',
                        textAlign: 'center',
                        width: 65,
                        height: 110,
                        editable: true
                    },
                    new go.Binding('text', 'describtion').makeTwoWay()
                )
            ) // end Auto Panel body
        );

        // this.plateSfcAccessory.model.nodeDataArray = [
        //     {
        //         source: this.sampleSfc,
        //         key: 0,
        //         name: 'Controller',
        //         describtion: 'ODL',
        //         loc: '0 0',
        //         ipAddress: '',
        //         cpu: '0',
        //         ram: '0',
        //         isDedicatedRes: '0',
        //         leftArray: [],
        //         topArray: [],
        //         bottomArray: [{portColor: '#316571', portId: 'bottom0'}],
        //         rightArray: []
        //     },
        // ];

        ////////////////////////////////////////////////////////
    }

    loadSfc(event){
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('uploadFile', file, file.name);
            let reader = new FileReader();
            reader.onloadend = (e) => {
                this.plateSfcAccessory.startTransaction("make new group");
                this.plateSfcAccessory.model.addNodeDataCollection(JSON.parse(reader.result).nodeDataArray);
                this.plateSfcAccessory.model.addLinkDataCollection(JSON.parse(reader.result).linkDataArray);
                this.plateSfcAccessory.commitTransaction("make new group");
            };
            reader.readAsText(file);
        }
    }

    deleteSfc(){
        if(this.selectedEntity !== null){
            this.plateSfcAccessory.startTransaction();
            this.plateSfcAccessory.remove(this.selectedEntity);
            this.plateSfcAccessory.commitTransaction("deleted node");
        }
    }
}
