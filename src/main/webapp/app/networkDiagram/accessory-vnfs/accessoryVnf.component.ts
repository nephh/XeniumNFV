import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    AfterViewInit
} from '@angular/core';

import * as go from 'gojs';
import {NetworkDiagramService} from '../networkDiagram.service';
import {AccessoryVnfPopupComponent} from './accessoryVnf-edit.component';
import {EventManager} from 'ng-jhipster';
import {Vnf} from './vnf.model';

const $$ = go.GraphObject.make;

@Component({
    selector: 'accessory-vnf',
    templateUrl: './accessoryVnf.component.html'
})
export class AccessoryVnf extends go.Link implements OnInit, AfterViewInit {
    @ViewChild('plateVnfAccessory') plateVnfAccessoryEl: ElementRef;

    public __this = this;
    public sampleVnf = require('../../../content/img/XenoNet/sampleVnf.png');
    public plateVnfAccessory :any;
    private selectedEntity;

    constructor(private networkDiagramService: NetworkDiagramService,
                private eventManager: EventManager) {
        super();

        this.eventManager.subscribe('accessoryVnfEditEvent', response=>{
            console.log(response.content);
            let objVnf: Vnf = response.content;
            // Setting VNF source image
            this.plateVnfAccessory.model.addNodeData(
                {
                    source: this.sampleVnf,
                    key: 'VNF',
                    name: 'VNF',
                    imgName: objVnf.imgName,
                    executionCommand: objVnf.executionCommand,
                    describtion: objVnf.name? objVnf.name: 'VNF',
                    loc: '0 0',
                    cpu: objVnf.cpu,
                    ram: objVnf.ram,
                    isDedicatedRes: objVnf.isDedicatedRes,
                    serverId : '',
                    category : objVnf.category,
                    leftArray: objVnf.leftArray,
                    topArray: objVnf.topArray,
                    bottomArray: objVnf.bottomArray,
                    rightArray: objVnf.rightArray
                }
            )
        })
    }

    ngAfterViewInit() {
    }

    ngOnInit() {
        ///////////////////////////////////////////////////////
        this.plateVnfAccessory = $$(
            go.Palette,
            this.plateVnfAccessoryEl.nativeElement.id
        );

        this.plateVnfAccessory.addDiagramListener('BackgroundSingleClicked', e => {
            this.selectedEntity = null;
        });

        this.plateVnfAccessory.addDiagramListener('ObjectSingleClicked', e => {
            var part = e.subject.part;
            if (!(part instanceof go.Link)) {
                this.selectedEntity = part;
            }
        });

        this.plateVnfAccessory.nodeTemplate = $$(
            go.Node,
            'Horizontal',

            // the body
            $$(
                go.Panel,
                'Auto',
                {
                    row: 1,
                    column: 1,
                    name: 'BODY',
                    stretch: go.GraphObject.Fill
                },

                $$(go.Shape, 'Rectangle', {
                    fill: '#ffffff',
                    strokeWidth: 1,
                    minSize: new go.Size(60, 75)
                }),

                $$(
                    go.Picture,

                    {
                        // margin: 20,
                        width: 58,
                        height: 70
                    },
                    new go.Binding('source')
                ),
                $$(
                    go.TextBlock,
                    {
                        margin: 2,
                        verticalAlignment: go.Spot.Top,
                        font: '14px  Segoe UI,sans-serif',
                        stroke: 'black',
                        textAlign: 'center',
                        width: 60,
                        height: 110,
                        editable: true
                    },
                    new go.Binding('text', 'imgName').makeTwoWay()
                ),
                $$(
                    go.TextBlock,
                    {
                        margin: 2,
                        verticalAlignment: go.Spot.Bottom,
                        font: '14px  Segoe UI,sans-serif',
                        stroke: 'black',
                        textAlign: 'center',
                        width: 60,
                        height: 110,
                        editable: true
                    },
                    new go.Binding('text', 'describtion').makeTwoWay()
                )
            ) // end Auto Panel body
        );
        this.plateVnfAccessory.model.nodeDataArray = [
            {
                source: this.sampleVnf,
                key: 'VNF',
                name: 'VNF',
                imgName: 'bvnf4',
                executionCommand: 'while true; do sleep 1000; done',
                describtion: 'BVnf4',
                loc: '0 0',
                cpu: '0',
                ram: '0',
                isDedicatedRes: '0',
                serverId: '',
                category: 'Node',
                leftArray: [],
                topArray: [],
                bottomArray: [{portColor: '#316571', portId: 'eth0', name: 'eth0', ipAddress: '', gateway:''}],
                rightArray: []
            }
        ];

        ////////////////////////////////////////////////////////
    }

    createVnf() {
        this.networkDiagramService.openModal(AccessoryVnfPopupComponent as Component, null);
    }

    deleteVnf(){
        if(this.selectedEntity !== null){
            this.plateVnfAccessory.startTransaction("deleted node");
            this.plateVnfAccessory.remove(this.selectedEntity);
            this.plateVnfAccessory.commitTransaction("deleted node");
        }
    }
}
