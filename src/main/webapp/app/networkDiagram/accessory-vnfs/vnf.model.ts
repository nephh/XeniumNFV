/*
*
*    @ AH.GHORAB
*
*/
import {BaseEntity} from '../../shared/baseEntity/baseEntity.model';
import {DiagramEntityConnection} from '../tab-diagram-drawer/diagramEntityConnection.model';
import {NodePort} from './nodePort.model';

export class Vnf extends BaseEntity<number> {
    constructor(public source?: string,
                public key?: string,
                public name?: string,
                public describtion?: string,
                public loc?: string,
                public cpu?: string,
                public ram?: string,
                public isDedicatedRes?: string,
                public leftArray?: NodePort[],
                public topArray?: NodePort[],
                public bottomArray?: NodePort[],
                public rightArray?: NodePort[],
                public group?: string,
                public imgName?: string,
                public executionCommand?: string,
                public serverId?: string,
                public category?: string,
                public diagramEntityConnections?: DiagramEntityConnection[]) {
        super();
        this.id = -1;
        this.source = source ? source : '';
        this.key = key ? key : '';
        this.name = name ? name : '';
        this.describtion = describtion ? describtion : '';
        this.loc = loc ? loc : '';
        this.cpu = cpu ? cpu : '';
        this.ram = ram ? ram : '';
        this.isDedicatedRes = isDedicatedRes ? isDedicatedRes : '0';
        this.leftArray = leftArray ? leftArray : [];
        this.topArray = topArray ? topArray : [];
        this.bottomArray = bottomArray ? bottomArray : [];
        this.rightArray = rightArray ? rightArray : [];
        this.group = group? group : '';
        this.imgName = imgName? imgName : '';
        this.executionCommand = executionCommand? executionCommand : '';
        this.serverId = serverId? serverId : '';
        this.category = category? category : '';
        this.diagramEntityConnections = diagramEntityConnections ? diagramEntityConnections : new Array();
    }
}
