import {Server} from '../servers/server.model';
import {OveralDiagramConnection} from '../servers/overalDiagramConnection.model';
import {Topology} from './tab-diagram-drawer/topology.model';
import {Switch} from './accessory-switch/swicth.model';
import {Vnf} from './accessory-vnfs/vnf.model';
import {Controller} from './accessory-controller/controller.model';
import {DiagramEntityConnection} from './tab-diagram-drawer/diagramEntityConnection.model';
import {Group} from './accessory-sfc/group.model';
import {NodePort} from './accessory-vnfs/nodePort.model';
import {MainServerInterface} from './tab-diagram-drawer/MainServerInterface';

const uuidv4 = require('uuid/v4');

export class JsonToNetworkDiagramParser {
    constructor() {
    }

    parsJson(servers: Server[], jSonStr: any) {
        console.log(JSON.parse(jSonStr));
        // Clear overalDiagramConnection inside each server
        console.log(servers);
        servers.forEach(server => {
            server.overalDiagramConnections.splice(0, server.overalDiagramConnections.length)
        });
        JSON.parse(jSonStr).linkDataArray.forEach(element => {
            servers.forEach(server => {
                if (server.name === element.from) {
                    server.overalDiagramConnections.push(new OveralDiagramConnection(null, element.from, element.to, element.fromPort, element.toPort))
                }
            });
        });
        return servers;
    }

    getListOfMustCreateSfc(jSonStr: any) {
        let mustCreateSfcKey = [];
        let alreadyCheckedSfcs = [];
        JSON.parse(jSonStr).nodeDataArray.forEach(element => {
            if (element.group !== null && element.group !== undefined && element.group !== '' && alreadyCheckedSfcs.indexOf(element.group) < 0) {
                alreadyCheckedSfcs.push(element.group);
                let allEmentWitchSameSfcKey = [];

                /*
                Find All Node With Same SFC Group Key
                 */
                JSON.parse(jSonStr).nodeDataArray.forEach(elementWithKey => {
                    if (elementWithKey.group === element.group) {
                        allEmentWitchSameSfcKey.push(elementWithKey.key);
                    }
                });

                /*
                Find External Port If Exist So that we can Create the SFC
                 */
                // let port: any;
                // JSON.parse(jSonStr).linkDataArray.forEach(port=>{
                for (let port of JSON.parse(jSonStr).linkDataArray) {
                    /*
                    "element.from" is in allEmentWitchSameSfcKey array
                    and "element.From" is not in allEmentWitchSameSfcKey array
                    so this is an External Port
                     */
                    if (allEmentWitchSameSfcKey.indexOf(port.from) > -1) {
                        if (allEmentWitchSameSfcKey.indexOf(port.to) < 0) {
                            mustCreateSfcKey.push(element.group);
                            break;
                        }
                    }
                    /*
                    "element.to" is in allEmentWitchSameSfcKey array
                    and "element.from" is not in allEmentWitchSameSfcKey array
                    so this is an External Port
                     */
                    if (allEmentWitchSameSfcKey.indexOf(port.to) > -1) {
                        if (allEmentWitchSameSfcKey.indexOf(port.from) < 0) {
                            mustCreateSfcKey.push(element.group);
                            break;
                        }
                    }
                }
                // );
            }
        });

        return mustCreateSfcKey;
    }

    /*
    For Saving SFC individual -> We save sfc az new topology GOJS JSON
     */
    getSfcJsonFromDiagramJson(jSonStr: any, sfcId: any) {
        let sfcTopologyJson: any = {
            'class': 'go.GraphLinksModel',
            'copiesArrays': true,
            'copiesArrayObjects': true,
            'linkFromPortIdProperty': 'fromPort',
            'linkToPortIdProperty': 'toPort',
        };
        let nodeDataArray = [];
        let linkDataArray = [];
        sfcTopologyJson.nodeDataArray = nodeDataArray;
        sfcTopologyJson.linkDataArray = linkDataArray;

        if (sfcId !== null && sfcId !== undefined) {

            /*
            For isolate all sfc port from topology ports
             */
            let allSfcNodeKey: string[] = [];

            var rand = uuidv4();
            JSON.parse(jSonStr).nodeDataArray.forEach(element => {
                if ((element.isGroup === 'true' || element.isGroup === true) && element.key === sfcId) {
                    sfcTopologyJson.nodeDataArray.push(
                        {
                            'text': element.text,
                            'isGroup': element.isGroup,
                            'category': element.category,
                            'key': rand
                        }
                    )
                }
                if (element.group === sfcId) {
                    if (element.name === 'Switch') {
                        allSfcNodeKey.push(element.key);
                        sfcTopologyJson.nodeDataArray.push({
                            'source': element.source,
                            'key': element.key,
                            'name': element.name,
                            'describtion': element.describtion,
                            'loc': element.loc,
                            'leftArray': element.leftArray,
                            'topArray': element.topArray,
                            'bottomArray': element.bottomArray,
                            'rightArray': element.rightArray,
                            'group': rand,
                            'serverId': element.serverId,
                            'category': element.category,
                            'controllerIp': element.controllerIp,
                            'controllerPort': element.controllerPort
                        });
                    }
                    else if (element.name === 'VNF') {
                        allSfcNodeKey.push(element.key);
                        sfcTopologyJson.nodeDataArray.push({
                            'source': element.source,
                            'key': element.key,
                            'name': element.name,
                            'imgName': element.imgName,
                            'executionCommand': element.executionCommand,
                            'describtion': element.describtion,
                            'loc': element.loc,
                            'cpu': element.cpu,
                            'ram': element.ram,
                            'isDedicatedRes': element.isDedicatedRes,
                            'leftArray': element.leftArray,
                            'topArray': element.topArray,
                            'bottomArray': element.bottomArray,
                            'rightArray': element.rightArray,
                            'group': rand,
                            'serverId': element.serverId,
                            'category': element.category,
                        });
                    }
                    else if (element.name === 'Controller') {
                        allSfcNodeKey.push(element.key);
                        sfcTopologyJson.nodeDataArray.push({
                            'source': element.source,
                            'key': element.key,
                            'name': element.name,
                            'describtion': element.describtion,
                            'loc': element.loc,
                            'ipAddress': element.ipAddress,
                            'cpu': element.cpu,
                            'ram': element.ram,
                            'isDedicatedRes': element.isDedicatedRes,
                            'leftArray': element.leftArray,
                            'topArray': element.topArray,
                            'bottomArray': element.bottomArray,
                            'rightArray': element.rightArray,
                            'group': rand,
                            'serverId': element.serverId,
                            'category': element.category,
                        });
                    }
                }
            });
            JSON.parse(jSonStr).linkDataArray.forEach(element => {
                for (let i = 0; i < allSfcNodeKey.length; i++) {
                    if (allSfcNodeKey[i] === element.from) {
                        for (let j = 0; j < allSfcNodeKey.length; j++) {
                            if (allSfcNodeKey[j] === element.to) {
                                sfcTopologyJson.linkDataArray.push({
                                    'from': element.from,
                                    'to': element.to,
                                    'fromPort': element.fromPort,
                                    'toPort': element.toPort,
                                    'points': element.points
                                })
                            }
                        }
                    }
                }
            });
        }
        return JSON.stringify(sfcTopologyJson);
    }

    convertDiagramJsonToTopology(jSonStr: any, serverId: string) {
        let mustCreateSfcKey = this.getListOfMustCreateSfc(jSonStr);
        console.log('mustCreateSfcKey');
        console.log(mustCreateSfcKey);
        let topology: Topology = new Topology();
        topology.serverId = serverId;
        JSON.parse(jSonStr).nodeDataArray.forEach(element => {
            if (element.name === 'Switch') {
                /*
                Checking for groups(sfc) , if sfc key does'nt in mustCreateSfcKey so the entity should be ignore
                 */
                if (element.group !== null && element.group !== undefined && element.group !== '') {
                    if (mustCreateSfcKey.indexOf(element.group) > -1) {
                        topology.switches.push(new Switch(
                            element.source,
                            element.key,
                            element.name,
                            element.describtion,
                            element.loc,
                            [],
                            [],
                            [],
                            [],
                            element.group,
                            element.serverId,
                            element.category,
                            element.controllerIp,
                            element.controllerPort,
                            new Array()
                            )
                        )
                    }
                }
                else {
                    topology.switches.push(new Switch(
                        element.source,
                        element.key,
                        element.name,
                        element.describtion,
                        element.loc,
                        [],
                        [],
                        [],
                        [],
                        element.group,
                        element.serverId,
                        element.category,
                        element.controllerIp,
                        element.controllerPort,
                        new Array()
                        )
                    )
                }
            }
            else if (element.name === 'VNF') {
                if (element.group !== null && element.group !== undefined && element.group !== '') {
                    if (mustCreateSfcKey.indexOf(element.group) > -1) {

                        // Fill The side port arrays
                        let leftArray = [];
                        element.leftArray.forEach(leftArr => {
                           leftArray.push(new NodePort(
                               leftArr.portColor,
                               leftArr.portId,
                               leftArr.name,
                               leftArr.ipAddress,
                               leftArr.gateway
                             )
                           );
                        });
                        let topArray = [];
                        element.topArray.forEach(topArr => {
                            topArray.push(new NodePort(
                                topArr.portColor,
                                topArr.portId,
                                topArr.name,
                                topArr.ipAddress,
                                topArr.gateway
                                )
                            );
                        });
                        let bottomArray = [];
                        element.bottomArray.forEach(bottomArr => {
                            bottomArray.push(new NodePort(
                                bottomArr.portColor,
                                bottomArr.portId,
                                bottomArr.name,
                                bottomArr.ipAddress,
                                bottomArr.gateway
                                )
                            );
                        });
                        let rightArray = [];
                        element.rightArray.forEach(rightArr => {
                            rightArray.push(new NodePort(
                                rightArr.portColor,
                                rightArr.portId,
                                rightArr.name,
                                rightArr.ipAddress,
                                rightArr.gateway
                                )
                            );
                        });


                        topology.vnfs.push(new Vnf(
                            element.source,
                            element.key,
                            element.name,
                            element.describtion,
                            element.loc,
                            element.cpu,
                            element.ram,
                            element.isDedicatedRes,
                            leftArray,
                            topArray,
                            bottomArray,
                            rightArray,
                            element.group,
                            element.imgName,
                            element.executionCommand,
                            element.serverId,
                            element.category,
                            new Array()
                        ));
                    }
                }
                else {
                    // Fill The side port arrays
                    let leftArray = [];
                    element.leftArray.forEach(leftArr => {
                        leftArray.push(new NodePort(
                            leftArr.portColor,
                            leftArr.portId,
                            leftArr.name,
                            leftArr.ipAddress,
                            leftArr.gateway
                            )
                        );
                    });
                    let topArray = [];
                    element.topArray.forEach(topArr => {
                        topArray.push(new NodePort(
                            topArr.portColor,
                            topArr.portId,
                            topArr.name,
                            topArr.ipAddress,
                            topArr.gateway
                            )
                        );
                    });
                    let bottomArray = [];
                    element.bottomArray.forEach(bottomArr => {
                        bottomArray.push(new NodePort(
                            bottomArr.portColor,
                            bottomArr.portId,
                            bottomArr.name,
                            bottomArr.ipAddress,
                            bottomArr.gateway
                            )
                        );
                    });
                    let rightArray = [];
                    element.rightArray.forEach(rightArr => {
                        rightArray.push(new NodePort(
                            rightArr.portColor,
                            rightArr.portId,
                            rightArr.name,
                            rightArr.ipAddress,
                            rightArr.gateway
                            )
                        );
                    });
                    topology.vnfs.push(new Vnf(
                        element.source,
                        element.key,
                        element.name,
                        element.describtion,
                        element.loc,
                        element.cpu,
                        element.ram,
                        element.isDedicatedRes,
                        leftArray,
                        topArray,
                        bottomArray,
                        rightArray,
                        element.group,
                        element.imgName,
                        element.executionCommand,
                        element.serverId,
                        element.category,
                        new Array()
                    ));
                }

            }
            else if (element.name === 'Controller') {
                if (element.group !== null && element.group !== undefined && element.group !== '') {
                    if (mustCreateSfcKey.indexOf(element.group) > -1) {
                        topology.controllers.push(new Controller(
                            element.source,
                            element.key,
                            element.name,
                            element.describtion,
                            element.loc,
                            element.ipAddress,
                            element.cpu,
                            element.ram,
                            element.isDedicatedRes,
                            [],
                            [],
                            [],
                            [],
                            element.group,
                            element.serverId,
                            element.category,
                            new Array()
                        ));
                    }
                }
                else {
                    topology.controllers.push(new Controller(
                        element.source,
                        element.key,
                        element.name,
                        element.describtion,
                        element.loc,
                        element.ipAddress,
                        element.cpu,
                        element.ram,
                        element.isDedicatedRes,
                        [],
                        [],
                        [],
                        [],
                        element.group,
                        element.serverId,
                        element.category,
                        new Array()
                    ));
                }

            }
            else if (element.isGroup === 'true' || element.isGroup === true) {
                topology.groups.push(new Group(
                    element.text,
                    'true',
                    element.category,
                    element.key
                ));
            }
            else if(element.name === 'MainServerInterface'){

                let rightArray = [];
                element.rightArray.forEach(rightArr => {
                    rightArray.push(new NodePort(
                        rightArr.portColor,
                        rightArr.portId,
                        rightArr.name,
                        rightArr.ipAddress,
                        rightArr.gateway
                        )
                    );
                });

                topology.mainServerInterface.push(new MainServerInterface(
                    element.source,
                    element.key,
                    element.name,
                    element.describtion,
                    '0 0',
                    [],
                    [],
                    [],
                    // MainServerInterface just has 'rightArray'
                    rightArray,
                    element.group,
                    element.serverId,
                    element.category,
                    new Array()
                ));
            }
        });
        JSON.parse(jSonStr).linkDataArray.forEach(element => {
            // CHECK FROM 'S

            /*
            we do not need to check the  mustCreateSfcKey here coz in every port we checking with topology entity
            that topology already crowded with mustCreateSfcKey and regular entity so "must-NOT-CreateSfcKey"
            is not there so we don't need to check again
             */
            if (element.from.split('#')[0] === 'VNF') {
                topology.vnfs.forEach(vnf => {
                    if (vnf.key === element.from) {
                        vnf.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        topology.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'Switch') {
                topology.switches.forEach(swtch => {
                    if (swtch.key === element.from) {
                        swtch.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        topology.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            else if (element.from.split('#')[0] === 'Controller') {
                topology.controllers.forEach(ctr => {
                    if (ctr.key === element.from) {
                        ctr.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        topology.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            // MainServerInterface FROM azash biron namiad vali man gozashtam va3 badan
            else if (element.from.split('#')[0] === 'MainServerInterface') {
                topology.mainServerInterface.forEach(msi => {
                    if (msi.key === element.from) {
                        msi.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                        topology.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ));
                    }
                })
            }
            // CHECK TOOOO 'S
            if (element.to.split('#')[0] === 'VNF') {
                topology.vnfs.forEach(vnf => {
                    if (vnf.key === element.to) {
                        vnf.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ))
                    }
                })
            }
            else if (element.to.split('#')[0] === 'Switch') {
                topology.switches.forEach(swtch => {
                    if (swtch.key === element.to) {
                        swtch.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ))
                    }
                })
            }
            else if (element.to.split('#')[0] === 'Controller') {
                topology.controllers.forEach(ctr => {
                    if (ctr.key === element.to) {
                        ctr.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ))
                    }
                })
            }
            else if (element.to.split('#')[0] === 'MainServerInterface') {
                topology.mainServerInterface.forEach(msi => {
                    if (msi.key === element.to) {
                        msi.diagramEntityConnections.push(new DiagramEntityConnection(
                            -1,
                            element.from + '%' + element.to,
                            element.from,
                            element.to,
                            element.fromPort,
                            element.toPort
                        ))
                    }
                })
            }


        });
        return topology;
    }
}


export class NetworkDiagramToJsonParser {

    constructor() {
    }

    parsNetworkDiagram(servers: Server[]) {

    }

}
