import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NetworkDiagramComponentProperties} from './networkDiagramComponentProperties.model';

@Component({
    selector: "Xnet-networkDiagramEntityPropertiesComponent",
    styles: [],
    templateUrl: "./networkDiagramComponentProperties.component.html"
})
export class NetworkDiagramComponentPropertiesComponent {
    @Input()
    networkDiagramComponentProperties: NetworkDiagramComponentProperties;
    @Output() applyChangeToComponent = new EventEmitter();

    knOptions = {
        readOnly: false,
        size: 60,
        unit: '',
        textColor: '#000000',
        fontSize: '32',
        fontWeigth: '700',
        fontFamily: 'Roboto',
        valueformat: 'percent',
        step: 0.1,
        value: 0,
        max: 10,
        trackWidth: 9,
        barWidth: 10,
        trackColor: '#D8D8D8',
        barColor: '#16A085',
        displayInput:false,
        displayPrevious: false,
        // subText: {
        //     enabled: false,
        //     fontFamily: 'Verdana',
        //     font: '14',
        //     fontWeight: 'bold',
        //     text: 'Overall',
        //     color: '#000000',
        //     offset: 7
        // },
    };
    constructor() {}

    applyChange() {
        this.applyChangeToComponent.emit(this.networkDiagramComponentProperties);
    }

    trackByIndex(index: number, obj: any): any {
        return index;
    }
}
