import {Component, EventEmitter, Input, Output} from '@angular/core';
import { NetworkDiagramSwitchProperties} from './networkDiagramSwitchProperties.model';

@Component({
    selector: "Xnet-networkDiagramSwitchPropertiesComponent",
    styles: [],
    templateUrl: "./networkDiagramSwitchProperties.component.html"
})
export class NetworkDiagramSwitchPropertiesComponent {
    @Input()
    networkDiagramSwitchProperties: NetworkDiagramSwitchProperties;
    @Output() applyChangeToSwitch = new EventEmitter();


    constructor() {}

    applyChange() {
        this.applyChangeToSwitch.emit(this.networkDiagramSwitchProperties);
    }
}
