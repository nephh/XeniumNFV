import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {Server} from '../../servers/server.model';
import * as go from 'gojs';
import {NetworkDiagramComponentProperties} from '../networkDiagramEntityProperties/networkDiagramComponentProperties.model';
import {JsonToNetworkDiagramParser} from '../netWorkDiagramParser.util';
import {NetworkDiagramService} from '../networkDiagram.service';
import {NotificationService} from '../../shared/utils/notification.service';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {NodePort} from '../accessory-vnfs/nodePort.model';
import {NetworkCard} from '../../servers/networkCard.model';
import {NetworkDiagramSwitchProperties} from '../networkDiagramSwitchProperties/networkDiagramSwitchProperties.model';
import {NetworkDiagramMainServerInterfaceProperties} from '../networkDiagramMainServerInterfaceProperties/networkDiagramMainServerInterfaceProperties.model';
import {NetworkDiagramMainServerInterfacePropertiesComponent} from '../networkDiagramMainServerInterfaceProperties/networkDiagramMainServerInterfaceProperties.component';
import {DiagramSubjectService} from '../../shared/diagramSubject/diagramSubject.service';

const $$ = go.GraphObject.make;
const uuidv4 = require('uuid/v4');
const FileSaver = require('file-saver');

@Component({
    selector: 'diagram-drawer',
    templateUrl: './tab-diagram-drawer.component.html',
    styleUrls: ['../utils/DataInspector.css']
})
export class TabDiagramDrawer extends go.Link implements OnInit, AfterViewInit {
    @Input() server: Server;
    public sampleMainServerInterface = require('../../../content/img/XenoNet/sampleNic.png');
    public networkDiagramComponentProperties: NetworkDiagramComponentProperties = new NetworkDiagramComponentProperties();
    public networkDiagramSwitchProperties: NetworkDiagramSwitchProperties = new NetworkDiagramSwitchProperties();
    public networkDiagramMainServerInterfaceProperties: NetworkDiagramMainServerInterfaceProperties = new NetworkDiagramMainServerInterfaceProperties();
    public myDiagram: any;
    public __this = this;
    private selectedPart: any;
    private ipCounter = 0;
    private controllerIp = '';
    private controllerPort = '6633';
    private selectedSfcId;
    private selectedEntytType: string = '';
    private tabSpinnerText = '';
    public serverIdDiagraramMap: Map<string, any>;

    constructor(private networkDiagramService: NetworkDiagramService,
                private notificationService: NotificationService,
                private ng4LoadingSpinnerService: Ng4LoadingSpinnerService,
                private diagramSubjectService: DiagramSubjectService) {
        super();
    }

    ngOnInit() {
        // this.diagramSubjectService.serverIdDiagraramSubject.subscribe(
        //     (serverIdDiagraramMap) => {
        //         this.serverIdDiagraramMap = serverIdDiagraramMap;
        //         console.log('ssuuuuuuuubjjjjjjeeeeetccc');
        //         console.log(this.serverIdDiagraramMap);
        //     }
        // );
    }

    ngAfterViewInit() {
        ///////////////////////////////////////////////////////
        this.myDiagram = $$(
            go.Diagram,
            // this.tabdiagramDrawer.nativeElement.id, //Diagram refers to its DIV HTML element by id
            document.getElementById(this.server.name),
            {
                mouseDrop: (e) => {
                    this.finishDrop(e, null);
                },
                initialContentAlignment: go.Spot.Center,
                'undoManager.isEnabled': true,
                'commandHandler.archetypeGroupData': {text: 'SFC', isGroup: true, category: 'OfNodes'},
                allowDrop: true
            }
        );


        this.myDiagram.addDiagramListener('ObjectSingleClicked', e => {
            var part = e.subject.part;
            if (!(part instanceof go.Link)) {
                console.log('my part IS');
                console.log(part.data);
                if (part.data.isGroup === 'true' || part.data.isGroup == true) {
                    this.selectedSfcId = part.data.key;
                }
                this.selectedPart = part;
                if (part.data.name !== 'Switch' && part.data.name !== 'MainServerInterface') {
                    this.selectedEntytType = 'VNF';
                    /*
                    Reset all property and set it again coz maybe someone click on element and instead of clicking on background (to reset the property)
                    click on another element so that conflict may happen. that's why i reset the properties on each click
                     */
                    this.networkDiagramComponentProperties.name = '';
                    this.networkDiagramComponentProperties.nodePort = [];
                    this.networkDiagramComponentProperties.cpu = '0';
                    this.networkDiagramComponentProperties.ram = '0';
                    this.networkDiagramComponentProperties.isDedicatedRes = false;

                    // Find NodePort of each top/right/bottom/left Arrays and send them to networkDiagramComponentProperties component
                    part.data.topArray.forEach(el => {
                        this.networkDiagramComponentProperties.nodePort.push(
                            new NodePort(
                                el.portColor,
                                el.portId,
                                el.name,
                                el.ipAddress,
                                el.gateway
                            )
                        );
                    });
                    part.data.rightArray.forEach(el => {
                        this.networkDiagramComponentProperties.nodePort.push(
                            new NodePort(
                                el.portColor,
                                el.portId,
                                el.name,
                                el.ipAddress,
                                el.gateway
                            )
                        );
                    });
                    part.data.bottomArray.forEach(el => {
                        this.networkDiagramComponentProperties.nodePort.push(
                            new NodePort(
                                el.portColor,
                                el.portId,
                                el.name,
                                el.ipAddress,
                                el.gateway
                            )
                        );
                    });
                    part.data.leftArray.forEach(el => {
                        this.networkDiagramComponentProperties.nodePort.push(
                            new NodePort(
                                el.portColor,
                                el.portId,
                                el.name,
                                el.ipAddress,
                                el.gateway
                            )
                        );
                    });
                    this.networkDiagramComponentProperties.name =
                        part.data.describtion;
                    this.networkDiagramComponentProperties.cpu = part.data.cpu;
                    this.networkDiagramComponentProperties.ram = part.data.ram;
                    this.networkDiagramComponentProperties.isDedicatedRes =
                        part.data.isDedicatedRes == '1' ? true : false;
                } else {
                    this.networkDiagramComponentProperties.name = '';
                    this.networkDiagramComponentProperties.nodePort = [];
                    this.networkDiagramComponentProperties.cpu = '0';
                    this.networkDiagramComponentProperties.ram = '0';
                    this.networkDiagramComponentProperties.isDedicatedRes = false;

                    if (part.data.name === 'Switch') {
                        this.selectedEntytType = 'Switch';
                        this.networkDiagramSwitchProperties.controllerIp = '';
                        this.networkDiagramSwitchProperties.controllerPort = '';
                        this.networkDiagramSwitchProperties.controllerIp = part.data.controllerIp;
                        this.networkDiagramSwitchProperties.controllerPort = part.data.controllerPort;

                    }
                    else if (part.data.name === 'MainServerInterface') {
                        /*
                            age ino nemizashtam vaghti az ye 'mainServerInterface' ro yeki dge click mikardim
                            megdare propeties taghir nemikard (inam faghat inja in etefagh miofte chon to property ha darim
                            dynamic gre tunnel haro misazim
                         */
                        this.selectedEntytType = '';
                        setTimeout(() => {
                            this.selectedEntytType = 'MainServerInterface';
                        }, 100);

                        this.networkDiagramMainServerInterfaceProperties.mainServerInterFaceIp = '';
                        this.networkDiagramMainServerInterfaceProperties.nodePort = [];

                        for (let nic of this.server.networkCards) {
                            if (nic.name === part.data.describtion.split('#')[0]) {
                                /*
                                 i want this format xxx.xxx.xxx not the last part coz in "NetworkDiagramMainServerInterfacePropertiesComponent"
                                 i want to show xxx.xxx.xxx/??
                                  */
                                this.networkDiagramMainServerInterfaceProperties.mainServerInterFaceIp = nic.ipAddress;

                                part.data.rightArray.forEach(el => {
                                    this.networkDiagramMainServerInterfaceProperties.nodePort.push(
                                        new NodePort(
                                            el.portColor,
                                            el.portId,
                                            el.name,
                                            el.ipAddress,
                                            el.gateway
                                        )
                                    );
                                });
                                break;
                            }
                        }


                    }
                }
            }
        });

        this.myDiagram.addDiagramListener('BackgroundSingleClicked', e => {
            this.selectedEntytType = ''
        });

        this.myDiagram.addDiagramListener('ExternalObjectsDropped', (e) => {
            e.subject.each((node) => {
                var model = e.diagram.model;
                var rand = uuidv4();
                node.data.serverId = this.server.id;
                if (node.data.name === 'Switch') {
                    model.setDataProperty(node.data, 'key', 'Switch#' + rand);
                    model.setDataProperty(node.data, 'describtion', 'S#' + rand.split('-')[1]);
                    model.setDataProperty(node.data, 'controllerIp', this.controllerIp);
                    model.setDataProperty(node.data, 'controllerPort', this.controllerPort);

                }
                else if (node.data.name === 'VNF') {
                    model.setDataProperty(node.data, 'key', 'VNF#' + rand);
                    model.setDataProperty(node.data, 'describtion', 'V#' + rand.split('-')[1]);
                    if (node.data.imgName === 'bvnf4') {
                        this.ipCounter++;
                        model.setDataProperty(node.data.bottomArray[0], 'ipAddress', '192.168.10.' + this.ipCounter)
                    }
                }
                else if (node.data.name === 'Controller') {
                    model.setDataProperty(node.data, 'key', 'Controller#' + rand);
                    model.setDataProperty(node.data, 'describtion', 'C#' + rand.split('-')[1]);
                }
            });
        });
        this.myDiagram.addDiagramListener('Modified', (e) => {
            this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
        });


        // when the document is modified, add a "*" to the title and enable the "Save" button
        // this.myDiagram.addDiagramListener("Modified", function (e) {
        // var button = document.getElementById("SaveButton");
        // // if (button) button.disabled = !this.myDiagram.isModified;
        // var idx = document.title.indexOf("*");
        // if (this.myDiagram.isModified) {
        // if (idx < 0) document.title += "*";
        // } else {
        // if (idx >= 0) document.title = document.title.substr(0, idx);
        // }
        // });

        // To simplify this code we define a function for creating a context menu button:

        var nodeMenu = $$(
            // context menu for each Node
            go.Adornment,
            'Vertical',
            this.makeButton('Copy', (e, obj) => {
                e.diagram.commandHandler.copySelection();
            }),
            this.makeButton('Delete', (e, obj) => {
                e.diagram.commandHandler.deleteSelection();
            }),
            $$(go.Shape, 'LineH', {
                strokeWidth: 2,
                height: 1,
                stretch: go.GraphObject.Horizontal
            }),
            this.makeButton('Add top port', (e, obj) => {
                this.addPort('top');
            }),
            this.makeButton('Add left port', (e, obj) => {
                this.addPort('left');
            }),
            this.makeButton('Add right port', (e, obj) => {
                this.addPort('right');
            }),
            this.makeButton('Add bottom port', (e, obj) => {
                this.addPort('bottom');
            })
        );

        var portSize = new go.Size(8, 8);

        var portMenu = $$(
            // context menu for each port
            go.Adornment,
            'Vertical',
            this.makeButton(
                'Remove port',
                // in the click event handler, the obj.part is the Adornment;
                // its adornedObject is the port
                function(e, obj) {
                    this.removePort(obj.part.adornedObject);
                }
            ),
            this.makeButton('Change color', (e, obj) => {
                this.changeColor(obj.part.adornedObject);
            }),
            this.makeButton('Remove side ports', (e, obj) => {
                this.removeAll(obj.part.adornedObject);
            })
        );

        // the node template
        // includes a panel on each side with an itemArray of panels containing ports
        // ADD Node category
        this.myDiagram.nodeTemplateMap.add('Node', $$(
            go.Node,
            'Table',
            {
                locationObjectName: 'BODY',
                locationSpot: go.Spot.Center,
                selectionObjectName: 'BODY',
                contextMenu: nodeMenu
            },
            new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(
                go.Point.stringify
            ),

            // the body
            $$(
                go.Panel,
                'Auto',
                {
                    row: 1,
                    column: 1,
                    name: 'BODY',
                    stretch: go.GraphObject.Fill
                },
                $$(go.Shape, 'Rectangle', {
                    fill: '#ffffff',
                    strokeWidth: 1,
                    minSize: new go.Size(85, 135)
                }),

                $$(
                    go.Picture,

                    {
                        // margin: 20,
                        width: 58,
                        height: 70
                    },
                    new go.Binding('source')
                ),
                $$(
                    go.TextBlock,
                    {
                        margin: 2,
                        verticalAlignment: go.Spot.Top,
                        font: '14px  Segoe UI,sans-serif',
                        stroke: 'black',
                        textAlign: 'center',
                        width: 65,
                        height: 110,
                        editable: true
                    },
                    new go.Binding('text', 'name').makeTwoWay()
                ),
                $$(
                    go.TextBlock,
                    {
                        margin: 2,
                        verticalAlignment: go.Spot.Bottom,
                        font: '11px  Segoe UI,sans-serif',
                        stroke: 'black',
                        textAlign: 'center',
                        width: 75,
                        height: 110,
                        editable: false
                    },
                    new go.Binding('text', 'imgName').makeTwoWay()
                ),
                $$(
                    go.TextBlock,
                    {
                        margin: 2,
                        verticalAlignment: go.Spot.Bottom,
                        font: '12px  Segoe UI,sans-serif',
                        stroke: 'black',
                        textAlign: 'center',
                        width: 84,
                        height: 135,
                        editable: false
                    },
                    new go.Binding('text', 'describtion').makeTwoWay()
                )
            ),

            // end Auto Panel body

            // the Panel holding the left port elements, which are themselves Panels,
            // created for each item in the itemArray, bound to data.leftArray
            $$(go.Panel, 'Vertical', new go.Binding('itemArray', 'leftArray'), {
                row: 1,
                column: 0,
                itemTemplate: $$(
                    go.Panel,
                    {
                        _side: 'left', // internal property to make it easier to tell which side it's on
                        fromSpot: go.Spot.Left,
                        toSpot: go.Spot.Left,
                        fromLinkable: true,
                        toLinkable: true,
                        cursor: 'pointer'
                        // contextMenu: portMenu
                    },
                    new go.Binding('portId', 'portId'),
                    $$(go.Shape, {
                        figure: 'DoubleEndArrow',
                        stroke: null,
                        strokeWidth: 1,
                        // angle: "90",
                        fill: '#2a1b51',
                        margin: new go.Margin(20, 0),
                        desiredSize: new go.Size(60, 5),
                    }),
                    $$(
                        go.TextBlock,
                        {
                            margin: 1,
                            verticalAlignment: go.Spot.Top,
                            font: '10px  Segoe UI,sans-serif',
                            stroke: 'black',
                            // angle: "270",
                            textAlign: 'center',
                            width: 60,
                            height: 20
                        },
                        new go.Binding('text', 'name').makeTwoWay()
                    ),
                    $$(
                        go.TextBlock,
                        {
                            margin: 1,
                            verticalAlignment: go.Spot.Bottom,
                            font: '8px  Segoe UI,sans-serif',
                            stroke: 'black',
                            // angle: "270",
                            textAlign: 'center',
                            width: 60,
                            height: 20
                        },
                        new go.Binding('text', 'ipAddress').makeTwoWay()
                    ),
                ) // end itemTemplate
            }), // end Vertical Panel

            // the Panel holding the top port elements, which are themselves Panels,
            // created for each item in the itemArray, bound to data.topArray
            $$(
                go.Panel,
                'Horizontal',
                new go.Binding('itemArray', 'topArray'),
                {
                    row: 0,
                    column: 1,
                    itemTemplate: $$(
                        go.Panel,
                        {
                            _side: 'top',
                            fromSpot: go.Spot.Top,
                            toSpot: go.Spot.Top,
                            fromLinkable: true,
                            toLinkable: true,
                            cursor: 'pointer'
                            // contextMenu: portMenu
                        },
                        new go.Binding('portId', 'portId'),
                        new go.Binding('portId', 'portId'),
                        $$(go.Shape, {
                            figure: 'DoubleEndArrow',
                            stroke: null,
                            strokeWidth: 1,
                            angle: '90',
                            fill: '#2a1b51',
                            margin: new go.Margin(0, 20),
                            desiredSize: new go.Size(60, 5),
                        }),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Top,
                                font: '10px  Segoe UI,sans-serif',
                                stroke: 'black',
                                angle: '270',
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'name').makeTwoWay()
                        ),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Bottom,
                                font: '8px  Segoe UI,sans-serif',
                                stroke: 'black',
                                angle: '270',
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'ipAddress').makeTwoWay()
                        ),
                    ) // end itemTemplate
                }
            ), // end Horizontal Panel

            // the Panel holding the right port elements, which are themselves Panels,
            // created for each item in the itemArray, bound to data.rightArray
            $$(
                go.Panel,
                'Vertical',
                new go.Binding('itemArray', 'rightArray'),
                {
                    row: 1,
                    column: 2,
                    itemTemplate: $$(
                        go.Panel,
                        {
                            _side: 'right',
                            fromSpot: go.Spot.Right,
                            toSpot: go.Spot.Right,
                            fromLinkable: true,
                            toLinkable: true,
                            cursor: 'pointer',
                            contextMenu: portMenu
                        },
                        new go.Binding('portId', 'portId'),
                        new go.Binding('portId', 'portId'),
                        $$(go.Shape, {
                            figure: 'DoubleEndArrow',
                            stroke: null,
                            strokeWidth: 1,
                            // angle: "90",
                            fill: '#2a1b51',
                            margin: new go.Margin(20, 0),
                            desiredSize: new go.Size(60, 5),
                        }),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Top,
                                font: '10px  Segoe UI,sans-serif',
                                stroke: 'black',
                                // angle: "270",
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'name').makeTwoWay()
                        ),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Bottom,
                                font: '8px  Segoe UI,sans-serif',
                                stroke: 'black',
                                // angle: "270",
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'ipAddress').makeTwoWay()
                        )
                    ) // end itemTemplate
                }
            ), // end Vertical Panel

            // the Panel holding the bottom port elements, which are themselves Panels,
            // created for each item in the itemArray, bound to data.bottomArray
            $$(
                go.Panel,
                'Horizontal',
                new go.Binding('itemArray', 'bottomArray'),
                {
                    row: 2,
                    column: 1,
                    itemTemplate: $$(
                        go.Panel,
                        {
                            _side: 'bottom',
                            fromSpot: go.Spot.Bottom,
                            toSpot: go.Spot.Bottom,
                            fromLinkable: true,
                            toLinkable: true,
                            cursor: 'pointer'
                            // contextMenu: portMenu
                        },
                        new go.Binding('portId', 'portId'),
                        $$(go.Shape, {
                            figure: 'DoubleEndArrow',
                            stroke: null,
                            strokeWidth: 1,
                            angle: '90',
                            fill: '#2a1b51',
                            margin: new go.Margin(0, 20),
                            desiredSize: new go.Size(60, 5),
                        }),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Top,
                                font: '10px  Segoe UI,sans-serif',
                                stroke: 'black',
                                angle: '270',
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'name').makeTwoWay()
                        ),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Bottom,
                                font: '8px  Segoe UI,sans-serif',
                                stroke: 'black',
                                angle: '270',
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'ipAddress').makeTwoWay()
                        )
                    ) // end itemTemplate
                }
            ) // end Horizontal Panel
        )); // end Node


        // ADD Nic category, for prevent to delete and grouping the Nic interface's
        this.myDiagram.nodeTemplateMap.add('Nic', $$(
            go.Node,
            'Table',
            {
                locationObjectName: 'BODY',
                locationSpot: go.Spot.Center,
                selectionObjectName: 'BODY',
                contextMenu: nodeMenu,
                copyable: false,
                deletable: false,
                groupable: false
            },
            new go.Binding('location', 'loc'),

            // the body
            $$(
                go.Panel,
                'Auto',
                {
                    row: 1,
                    column: 1,
                    name: 'BODY',
                    stretch: go.GraphObject.Fill
                },
                $$(go.Shape, 'Rectangle', {
                    fill: '#ffffff',
                    strokeWidth: 1,
                    minSize: new go.Size(170, 125)
                }),

                $$(
                    go.Picture,

                    {
                        // margin: 20,
                        width: 140,
                        height: 80
                    },
                    new go.Binding('source')
                ),
                $$(
                    go.TextBlock,
                    {
                        margin: 2,
                        verticalAlignment: go.Spot.Top,
                        font: '14px  Segoe UI,sans-serif',
                        stroke: 'black',
                        textAlign: 'center',
                        width: 150,
                        height: 120,
                        editable: true
                    },
                    new go.Binding('text', 'name').makeTwoWay()
                ),

                $$(
                    go.TextBlock,
                    {
                        margin: 2,
                        verticalAlignment: go.Spot.Bottom,
                        font: '12px  Segoe UI,sans-serif',
                        stroke: 'black',
                        textAlign: 'center',
                        width: 150,
                        height: 115,
                        editable: false
                    },
                    new go.Binding('text', 'describtion').makeTwoWay()
                )
            ),

            // end Auto Panel body

            // the Panel holding the left port elements, which are themselves Panels,
            // created for each item in the itemArray, bound to data.leftArray
            $$(go.Panel, 'Vertical', new go.Binding('itemArray', 'leftArray'), {
                row: 1,
                column: 0,
                itemTemplate: $$(
                    go.Panel,
                    {
                        _side: 'left', // internal property to make it easier to tell which side it's on
                        fromSpot: go.Spot.Left,
                        toSpot: go.Spot.Left,
                        fromLinkable: true,
                        toLinkable: true,
                        cursor: 'pointer'
                        // contextMenu: portMenu
                    },
                    new go.Binding('portId', 'portId'),
                    $$(go.Shape, {
                        figure: 'DoubleEndArrow',
                        stroke: null,
                        strokeWidth: 1,
                        // angle: "90",
                        fill: '#2a1b51',
                        margin: new go.Margin(20, 0),
                        desiredSize: new go.Size(60, 5),
                    }),
                    $$(
                        go.TextBlock,
                        {
                            margin: 1,
                            verticalAlignment: go.Spot.Top,
                            font: '10px  Segoe UI,sans-serif',
                            stroke: 'black',
                            // angle: "270",
                            textAlign: 'center',
                            width: 60,
                            height: 20
                        },
                        new go.Binding('text', 'name').makeTwoWay()
                    ),
                    $$(
                        go.TextBlock,
                        {
                            margin: 1,
                            verticalAlignment: go.Spot.Bottom,
                            font: '8px  Segoe UI,sans-serif',
                            stroke: 'black',
                            // angle: "270",
                            textAlign: 'center',
                            width: 60,
                            height: 20
                        },
                        new go.Binding('text', 'ipAddress').makeTwoWay()
                    ),
                ) // end itemTemplate
            }), // end Vertical Panel

            // the Panel holding the top port elements, which are themselves Panels,
            // created for each item in the itemArray, bound to data.topArray
            $$(
                go.Panel,
                'Horizontal',
                new go.Binding('itemArray', 'topArray'),
                {
                    row: 0,
                    column: 1,
                    itemTemplate: $$(
                        go.Panel,
                        {
                            _side: 'top',
                            fromSpot: go.Spot.Top,
                            toSpot: go.Spot.Top,
                            fromLinkable: true,
                            toLinkable: true,
                            cursor: 'pointer'
                            // contextMenu: portMenu
                        },
                        new go.Binding('portId', 'portId'),
                        new go.Binding('portId', 'portId'),
                        $$(go.Shape, {
                            figure: 'DoubleEndArrow',
                            stroke: null,
                            strokeWidth: 1,
                            angle: '90',
                            fill: '#2a1b51',
                            margin: new go.Margin(0, 20),
                            desiredSize: new go.Size(60, 5),
                        }),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Top,
                                font: '10px  Segoe UI,sans-serif',
                                stroke: 'black',
                                angle: '270',
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'name').makeTwoWay()
                        ),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Bottom,
                                font: '8px  Segoe UI,sans-serif',
                                stroke: 'black',
                                angle: '270',
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'ipAddress').makeTwoWay()
                        ),
                    ) // end itemTemplate
                }
            ), // end Horizontal Panel

            // the Panel holding the right port elements, which are themselves Panels,
            // created for each item in the itemArray, bound to data.rightArray
            $$(
                go.Panel,
                'Vertical',
                new go.Binding('itemArray', 'rightArray'),
                {
                    row: 1,
                    column: 2,
                    itemTemplate: $$(
                        go.Panel,
                        {
                            _side: 'right',
                            fromSpot: go.Spot.Right,
                            toSpot: go.Spot.Right,
                            fromLinkable: false,
                            toLinkable: true,
                            fromLinkableDuplicates: false,
                            toLinkableDuplicates: false,
                            cursor: 'pointer',
                            contextMenu: portMenu,
                            toMaxLinks: 1
                        },
                        new go.Binding('portId', 'portId'),
                        $$(go.Shape, {
                            figure: 'DoubleEndArrow',
                            stroke: null,
                            strokeWidth: 1,
                            // angle: "90",
                            fill: '#2a1b51',
                            margin: new go.Margin(20, 0),
                            desiredSize: new go.Size(60, 5),
                        }),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Top,
                                font: '10px  Segoe UI,sans-serif',
                                stroke: 'black',
                                // angle: "270",
                                textAlign: 'center',
                                width: 60,
                                height: 20,
                                editable: false
                            },
                            new go.Binding('text', 'name').makeTwoWay()
                        ),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Bottom,
                                font: '8px  Segoe UI,sans-serif',
                                stroke: 'black',
                                // angle: "270",
                                textAlign: 'center',
                                width: 60,
                                height: 20,
                                editable: false
                            },
                            new go.Binding('text', 'ipAddress').makeTwoWay()
                        )
                    ) // end itemTemplate
                }
            ), // end Vertical Panel

            // the Panel holding the bottom port elements, which are themselves Panels,
            // created for each item in the itemArray, bound to data.bottomArray
            $$(
                go.Panel,
                'Horizontal',
                new go.Binding('itemArray', 'bottomArray'),
                {
                    row: 2,
                    column: 1,
                    itemTemplate: $$(
                        go.Panel,
                        {
                            _side: 'bottom',
                            fromSpot: go.Spot.Bottom,
                            toSpot: go.Spot.Bottom,
                            fromLinkable: true,
                            toLinkable: true,
                            cursor: 'pointer'
                            // contextMenu: portMenu
                        },
                        new go.Binding('portId', 'portId'),
                        $$(go.Shape, {
                            figure: 'DoubleEndArrow',
                            stroke: null,
                            strokeWidth: 1,
                            angle: '90',
                            fill: '#2a1b51',
                            margin: new go.Margin(0, 20),
                            desiredSize: new go.Size(60, 5),
                        }),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Top,
                                font: '10px  Segoe UI,sans-serif',
                                stroke: 'black',
                                angle: '270',
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'name').makeTwoWay()
                        ),
                        $$(
                            go.TextBlock,
                            {
                                margin: 1,
                                verticalAlignment: go.Spot.Bottom,
                                font: '8px  Segoe UI,sans-serif',
                                stroke: 'black',
                                angle: '270',
                                textAlign: 'center',
                                width: 60,
                                height: 20
                            },
                            new go.Binding('text', 'ipAddress').makeTwoWay()
                        )
                    ) // end itemTemplate
                }
            ) // end Horizontal Panel
        )); // end Node

        // this.myDiagram.toolManager.mouseDownTools.add(new ResizeMultipleTool());


        //Group Template
        this.myDiagram.groupTemplateMap.add('OfNodes',
            $$(go.Group, go.Panel.Auto,
                {
                    // resizeObjectName: "SHAPE",
                    background: 'rgba(128,128,128,0.2)',
                    ungroupable: true,
                    resizable: true,
                    // highlight when dragging into the Group
                    mouseDragEnter: (e, grp, prev) => {
                        this.highlightGroup(e, grp, true);
                    },
                    mouseDragLeave: (e, grp, next) => {
                        this.highlightGroup(e, grp, false);
                    },
                    computesBoundsAfterDrag: true,
                    // when the selection is dropped into a Group, add the selected Parts into that Group;
                    // if it fails, cancel the tool, rolling back any changes
                    mouseDrop: (a, b) => {
                        this.finishDrop(a, b)
                    },
                    isLayoutPositioned: false,
                    handlesDragDropForMembers: true,  // don't need to define handlers on member Nodes and Links

                    // layout:
                    //     $$(go.TreeLayout,
                    //         { angle: 90, nodeSpacing: 10, layerSpacing: 30 }),

                    // layout:
                    //     $$(go.ForceDirectedLayout,
                    //         {
                    //             isInitial: false,
                    //         }),
                    // Groups containing Nodes lay out their members vertically
                    // layout:
                    //     $$(go.GridLayout,
                    //         { wrappingWidth: Infinity,
                    //             alignment: go.GridLayout.Position,
                    //             cellSize: new go.Size(10, 10),
                    //             spacing: new go.Size(20, 20),
                    //             isInitial: false,
                    //             // isOngoing: false
                    //         }),
                    // layout: $$(go.ForceDirectedLayout,
                    //     {
                    //          isRealtime: false, epsilonDistance: 0.01, infinityDistance: 1
                    //     })
                    //     { angle: 90, arrangement: go.TreeLayout.ArrangementHorizontal, isRealtime: false }),
                },
                { // automatically adjust the scale of the HEADER, depending on the nesting depth
                    // containingGroupChanged:
                    //     function(part, oldgroup, newgroup) {
                    //         part.findObject('HEADER').scale = 1 / (1 + part.findSubGraphLevel());
                    //     }
                },
                new go.Binding('background', 'isHighlighted', function(h) {
                    return h ? 'rgba(255,0,0,0.2)' : 'rgba(128,128,128,0.2)';
                }).ofObject(),
                $$(go.Shape, 'Rectangle',
                    {fill: null, stroke: '#0099CC', strokeWidth: 2}),
                $$(go.TextBlock,
                    {
                        alignment: go.Spot.Top,
                        textAlign: 'center',
                        // alignment: go.Spot.Left,
                        margin: 5,
                        editable: true,
                        font: 'bold 16px sans-serif',
                        stroke: '#006080'
                    },
                    new go.Binding('text', 'text').makeTwoWay()),
                $$(go.Panel, go.Panel.Vertical,  // title above Placeholder
                    $$(go.Panel, go.Panel.Horizontal,  // button next to TextBlock
                        {name: 'HEADER'},
                        // {stretch: go.GraphObject.Horizontal, background: '#33D3E5', margin: 1},
                        $$(go.TextBlock, '',
                            {
                                alignment: go.Spot.Left,
                                editable: true,
                                margin: 5,
                                font: 'bold 16px sans-serif',
                                stroke: '#006080'
                            }),
                        // new go.Binding("text", "text").makeTwoWay())
                        // new go.Binding('text', 'text').makeTwoWay())
                    ),  // end Horizontal Panel
                    $$(go.Placeholder,
                        {padding: 5, alignment: go.Spot.TopLeft})
                )  // end Vertical Panel
            ));  // end Group and call to add to template Map


        let CustomLink: any = function(): any {
            go.Link.call(this);
        };

        // an orthogonal link template, reshapable and relinkable
        // this.myDiagram.linkTemplate =
        //     $$(go.Link, go.Link.Bezier,
        //         // when using fromSpot/toSpot:
        //         {  corner: 4 },
        //         // new go.Binding("fromEndSegmentLength", "curviness"),
        //         // new go.Binding("toEndSegmentLength", "curviness"),
        //
        //         // if not using fromSpot/toSpot, use a binding to curviness instead:
        //         //new go.Binding("curviness", "curviness"),
        //
        //         $$(go.Shape,  // the link shape
        //             { stroke: "black", strokeWidth: 2 }),
        //         new go.Binding('points').makeTwoWay(),
        //         // $(go.Shape,  // the arrowhead, at the mid point of the link
        //         //   { toArrow: "OpenTriangle", segmentIndex: -Infinity })
        //     );
        this.myDiagram.linkTemplate = $$(
            go.Link, // defined below
            // ParallelRouteLink,
            {
                // curve: go.Link.Bezier,
                routing: go.Link.AvoidsNodes,
                corner: 4,
                isTreeLink: true,
                curve: go.Link.JumpGap,
                reshapable: true,
                resegmentable: true,
                relinkableFrom: true,
                relinkableTo: true
            },
            new go.Binding('points').makeTwoWay(),

            $$(go.Shape, {stroke: '#2F4F4F', strokeWidth: 2})
        );

        // support double-clicking in the background to add a copy of this data as a node
        this.myDiagram.toolManager.clickCreatingTool.archetypeNodeData = {
            name: 'Unit',
            leftArray: [],
            rightArray: [],
            topArray: [],
            bottomArray: []
        };

        this.myDiagram.contextMenu = $$(
            go.Adornment,
            'Vertical',
            this.makeButton(
                'Paste',
                function(e, obj) {
                    e.diagram.commandHandler.pasteSelection(
                        e.diagram.lastInput.documentPoint
                    );
                },
                function(o) {
                    return o.diagram.commandHandler.canPasteSelection();
                }
            ),
            this.makeButton(
                'Undo',
                function(e, obj) {
                    e.diagram.commandHandler.undo();
                },
                function(o) {
                    return o.diagram.commandHandler.canUndo();
                }
            ),
            this.makeButton(
                'Redo',
                function(e, obj) {
                    e.diagram.commandHandler.redo();
                },
                function(o) {
                    return o.diagram.commandHandler.canRedo();
                }
            )
        );

        // load the diagram from JSON data
        this.load();

        // This custom-routing Link class tries to separate parallel links from each other.
        // This assumes that ports are lined up in a row/column on a side of the node.

        go.Diagram.inherit(CustomLink, go.Link);

        CustomLink.prototype.findSidePortIndexAndCount = function(node, port) {
            var nodedata = node.data;
            if (nodedata !== null) {
                var portdata = port.data;
                var side = port._side;
                var arr = nodedata[side + 'Array'];
                var len = arr.length;
                for (var i = 0; i < len; i++) {
                    if (arr[i] === portdata) return [i, len];
                }
            }
            return [-1, len];
        };

        /** @override */
        CustomLink.prototype.computeEndSegmentLength = function(node,
                                                                port,
                                                                spot,
                                                                from) {
            // var esl = go.Link.prototype.computeEndSegmentLength.call(this, node, port, spot, from);
            var esl = this.__this.computeEndSegmentLength.call(
                this,
                node,
                port,
                spot,
                from
            );
            var other = this.getOtherPort(port);
            if (port !== null && other !== null) {
                var thispt = port.getDocumentPoint(this.computeSpot(from));
                var otherpt = other.getDocumentPoint(this.computeSpot(!from));
                if (
                    Math.abs(thispt.x - otherpt.x) > 20 ||
                    Math.abs(thispt.y - otherpt.y) > 20
                ) {
                    var info = this.findSidePortIndexAndCount(node, port);
                    var idx = info[0];
                    var count = info[1];
                    if (port._side == 'top' || port._side == 'bottom') {
                        if (otherpt.x < thispt.x) {
                            return esl + 4 + idx * 8;
                        } else {
                            return esl + (count - idx - 1) * 8;
                        }
                    } else {
                        // left or right
                        if (otherpt.y < thispt.y) {
                            return esl + 4 + idx * 8;
                        } else {
                            return esl + (count - idx - 1) * 8;
                        }
                    }
                }
            }
            return esl;
        };

        /** @override */
        CustomLink.prototype.hasCurviness = function() {
            if (isNaN(this.curviness)) return true;
            return this.__this.hasCurviness.call(this);
        };

        /** @override */
        CustomLink.prototype.computeCurviness = function() {
            if (isNaN(this.curviness)) {
                var fromnode = this.fromNode;
                var fromport = this.fromPort;
                var fromspot = this.computeSpot(true);
                var frompt = fromport.getDocumentPoint(fromspot);
                var tonode = this.toNode;
                var toport = this.toPort;
                var tospot = this.computeSpot(false);
                var topt = toport.getDocumentPoint(tospot);
                if (
                    Math.abs(frompt.x - topt.x) > 20 ||
                    Math.abs(frompt.y - topt.y) > 20
                ) {
                    if (
                        (fromspot.equals(go.Spot.Left) ||
                            fromspot.equals(go.Spot.Right)) &&
                        (tospot.equals(go.Spot.Left) ||
                            tospot.equals(go.Spot.Right))
                    ) {
                        var fromseglen = this.computeEndSegmentLength(
                            fromnode,
                            fromport,
                            fromspot,
                            true
                        );
                        var toseglen = this.computeEndSegmentLength(
                            tonode,
                            toport,
                            tospot,
                            false
                        );
                        var c = (fromseglen - toseglen) / 2;
                        if (frompt.x + fromseglen >= topt.x - toseglen) {
                            if (frompt.y < topt.y) return c;
                            if (frompt.y > topt.y) return -c;
                        }
                    } else if (
                        (fromspot.equals(go.Spot.Top) ||
                            fromspot.equals(go.Spot.Bottom)) &&
                        (tospot.equals(go.Spot.Top) ||
                            tospot.equals(go.Spot.Bottom))
                    ) {
                        var fromseglen = this.computeEndSegmentLength(
                            fromnode,
                            fromport,
                            fromspot,
                            true
                        );
                        var toseglen = this.computeEndSegmentLength(
                            tonode,
                            toport,
                            tospot,
                            false
                        );
                        var c = (fromseglen - toseglen) / 2;
                        if (frompt.x + fromseglen >= topt.x - toseglen) {
                            if (frompt.y < topt.y) return c;
                            if (frompt.y > topt.y) return -c;
                        }
                    }
                }
            }
            return this.__this.computeCurviness.call(this);
        };

        ////////////////////////////////////////////////////////
    }

    //Group Manipulation
    highlightGroup(e, grp, show) {
        if (!grp) return;
        e.handled = true;
        if (show) {
            // cannot depend on the grp.diagram.selection in the case of external drag-and-drops;
            // instead depend on the DraggingTool.draggedParts or .copiedParts
            var tool = grp.diagram.toolManager.draggingTool;
            var map = tool.draggedParts || tool.copiedParts;  // this is a Map
            // now we can check to see if the Group will accept membership of the dragged Parts
            if (grp.canAddMembers(map.toKeySet())) {
                grp.isHighlighted = true;
                return;
            }
        }
        grp.isHighlighted = false;
    }

    finishDrop(e, grp) {
        var ok = (grp !== null
            ? grp.addMembers(grp.diagram.selection, true)
            : e.diagram.commandHandler.addTopLevelParts(e.diagram.selection, true));
        console.log(ok);
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
        if (!ok) e.diagram.currentTool.doCancel();
    }


    makeButton(text, action, visiblePredicate ?) {
        return $$(
            'ContextMenuButton',
            $$(go.TextBlock, text),
            {click: action},
            // don't bother with binding GraphObject.visible if there's no predicate
            visiblePredicate
                ? new go.Binding('visible', '', function(o, e) {
                    return o.diagram ? visiblePredicate(o, e) : false;
                }).ofObject()
                : {}
        );
    }

    addPort(side) {
        this.myDiagram.startTransaction('addPort');
        let __this = this.myDiagram;
        this.myDiagram.selection.each(function(node) {
            if (!(node instanceof go.Node)) return;
            var i = 0;
            while (node.findPort(side + i.toString()) !== node) i++;
            var name = side + i.toString();
            var arr = node.data[side + 'Array'];
            if (arr) {
                var newportdata = {
                    portId: name,
                    portColor: go.Brush.randomColor()
                };
                __this.model.insertArrayItem(arr, -1, newportdata);
            }
        });
        this.myDiagram = __this;
        this.myDiagram.commitTransaction('addPort');
    }

    removePort(port) {
        this.myDiagram.startTransaction('removePort');
        var pid = port.portId;
        var arr = port.panel.itemArray;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].portId === pid) {
                this.myDiagram.model.removeArrayItem(arr, i);
                break;
            }
        }
        this.myDiagram.commitTransaction('removePort');
    }

    removeAll(port) {
        this.myDiagram.startTransaction('removePorts');
        var nodedata = port.part.data;
        var side = port._side; // there are four property names, all ending in "Array"
        this.myDiagram.model.setDataProperty(nodedata, side + 'Array', []); // an empty Array
        this.myDiagram.commitTransaction('removePorts');
    }

    changeColor(port) {
        this.myDiagram.startTransaction('colorPort');
        var data = port.data;
        this.myDiagram.model.setDataProperty(
            data,
            'portColor',
            go.Brush.randomColor()
        );
        this.myDiagram.commitTransaction('colorPort');
    }


    load() {
        // this.myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);

        if(this.diagramSubjectService.serverIdDiagraramMap.get(this.server.id.toString())){
            this.myDiagram.model = this.diagramSubjectService.serverIdDiagraramMap.get(this.server.id.toString()).model;
        }
        else {

            this.myDiagram.model = go.Model.fromJson({
                class: 'go.GraphLinksModel',
                copiesArrays: true,
                copiesArrayObjects: true,
                linkFromPortIdProperty: 'fromPort',
                linkToPortIdProperty: 'toPort',
                nodeDataArray: [],
                linkDataArray: []
            });

            this.server.networkCards.forEach((nic: NetworkCard, index) => {
                var rand = uuidv4();
                this.myDiagram.model.addNodeData(
                    {
                        source: this.sampleMainServerInterface,
                        key: 'MainServerInterface#' + rand,
                        name: 'MainServerInterface',
                        describtion: nic.name + '#' + nic.ipAddress,
                        loc: new go.Point(0, 0 + (index * 160)),
                        serverId: '',
                        category: 'Nic',
                        leftArray: [],
                        topArray: [],
                        bottomArray: [],
                        rightArray: []
                    }
                );
            });
        }
    }

    applyChangeToComponent(networkDiagramComponentProperties: NetworkDiagramComponentProperties) {
        if (this.selectedPart) {
            this.myDiagram.startTransaction('set all properties');
            console.log('set all properties');
            console.log(networkDiagramComponentProperties);
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'describtion',
                networkDiagramComponentProperties.name
            );
            //Set property of all side Arrays
            networkDiagramComponentProperties.nodePort.forEach(nodePort => {
                this.selectedPart.data.topArray.forEach((el, index) => {
                    if (el.name === nodePort.name) {
                        this.myDiagram.model.setDataProperty(
                            this.selectedPart.data.topArray[index],
                            'ipAddress',
                            nodePort.ipAddress
                        );
                    }
                });
                this.selectedPart.data.rightArray.forEach((el, index) => {
                    if (el.name === nodePort.name) {
                        this.myDiagram.model.setDataProperty(
                            this.selectedPart.data.rightArray[index],
                            'ipAddress',
                            nodePort.ipAddress
                        );
                    }
                });
                this.selectedPart.data.bottomArray.forEach((el, index) => {
                    if (el.name === nodePort.name) {
                        this.myDiagram.model.setDataProperty(
                            this.selectedPart.data.bottomArray[index],
                            'ipAddress',
                            nodePort.ipAddress
                        );
                    }
                });
                this.selectedPart.data.leftArray.forEach((el, index) => {
                    if (el.name === nodePort.name) {
                        this.myDiagram.model.setDataProperty(
                            this.selectedPart.data.leftArray[index],
                            'ipAddress',
                            nodePort.ipAddress
                        );
                    }
                });
            });

            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'cpu',
                networkDiagramComponentProperties.cpu
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'ram',
                networkDiagramComponentProperties.ram
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'isDedicatedRes',
                networkDiagramComponentProperties.isDedicatedRes == true
                    ? '1'
                    : '0'
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    applyChangeToSwitch(networkDiagramSwitchProperties: NetworkDiagramSwitchProperties) {
        if (this.selectedPart) {
            this.controllerIp = networkDiagramSwitchProperties.controllerIp;
            this.controllerPort = networkDiagramSwitchProperties.controllerPort;
            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'controllerIp',
                networkDiagramSwitchProperties.controllerIp
            );
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'controllerPort',
                networkDiagramSwitchProperties.controllerPort
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    applyChangeToMainServerInterface(networkDiagramMainServerInterfaceProperties: NetworkDiagramMainServerInterfaceProperties) {
        if (this.selectedPart) {
            let allRightPorts = [];
            networkDiagramMainServerInterfaceProperties.nodePort.forEach(nodePort => {
                allRightPorts.push(
                    {
                        'portColor': nodePort.portColor,
                        'portId': nodePort.portId,
                        'name': nodePort.name,
                        'ipAddress': nodePort.ipAddress,
                        'gateway': nodePort.gateway
                    }
                );
            });

            this.myDiagram.startTransaction('set all properties');
            this.myDiagram.model.setDataProperty(
                this.selectedPart.data,
                'rightArray',
                allRightPorts
            );
            this.myDiagram.commitTransaction('set all properties');
        }
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    zoomIn() {
        this.myDiagram.commandHandler.increaseZoom()
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }


    zoomOut() {
        this.myDiagram.commandHandler.decreaseZoom()
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    fitToView() {
        this.myDiagram.zoomToRect(this.myDiagram.documentBounds)
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    undo() {
        this.myDiagram.commandHandler.undo()
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    redo() {
        this.myDiagram.commandHandler.redo()
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    deployDiagram() {
        console.log(this.myDiagram.model.toJson());
        console.log(new JsonToNetworkDiagramParser().getListOfMustCreateSfc(this.myDiagram.model.toJson()));
        console.log(new JsonToNetworkDiagramParser().convertDiagramJsonToTopology(this.myDiagram.model.toJson(), this.server.id.toString()));

        this.ng4LoadingSpinnerService.show();
        this.tabSpinnerText = 'Deploying Diagram on ' + this.server.name + ' ...';
        this.networkDiagramService.create('/new', new JsonToNetworkDiagramParser().convertDiagramJsonToTopology(this.myDiagram.model.toJson(), this.server.id.toString())).subscribe((res: any) => {
            this.ng4LoadingSpinnerService.hide();
            this.tabSpinnerText = '';
            this.notificationService.smallBox({
                title: 'Topology Created successfully',
                content: '<i class=\'fa fa-clock-o\'></i> <i>2 seconds ago...</i>',
                color: '#16A085',
                iconSmall: 'fa fa-check bounce animated',
                timeout: 4000
            });
        }, (err) => {
            this.ng4LoadingSpinnerService.hide();
            this.tabSpinnerText = '';
            this.notificationService.smallBox({
                title: err ? err : 'Undefined Error',
                content: '<i class=\'fa fa-clock-o\'></i> <i>1 seconds ago...</i>',
                color: '#a00002',
                iconSmall: 'fa fa-times  bounce animated',
                timeout: 8000
            });
            console.log(err);
        });

    }

    createSfc() {
        this.myDiagram.commandHandler.groupSelection({});
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    saveDiagram() {
        var blob = new Blob([this.myDiagram.model.toJson()], {type: 'text/plain'});
        FileSaver.saveAs(blob, this.server.name + '.xnet');
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    saveSelectedSfc() {
        if (this.selectedSfcId !== null && this.selectedSfcId !== undefined && this.selectedSfcId !== '') {
            var blob = new Blob([new JsonToNetworkDiagramParser().getSfcJsonFromDiagramJson(this.myDiagram.model.toJson(), this.selectedSfcId)], {type: 'text/plain'});
            FileSaver.saveAs(blob, this.server.name + 'SFC' + '.xnetsfc');
        }
        else {
            this.notificationService.smallBox({
                title: 'No SFC is selected',
                content: '',
                color: '#FF9900',
                iconSmall: 'fa fa-exclamation-triangle  bounce animated',
                timeout: 4000
            });
        }
    }

    clearDiagram() {
        this.myDiagram.clear();
        this.ipCounter = 0;
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    loadDiagram(event) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('uploadFile', file, file.name);
            let reader = new FileReader();
            reader.onloadend = (e) => {
                this.clearDiagram();
                this.myDiagram.model = go.Model.fromJson(reader.result);
                //Find Max Ip Address of loaded topology and replace it with current one
                let maxIpAddress: number = 0;
                this.myDiagram.nodes.each((node) => {
                    if (node.data.name === 'VNF') {
                        if (maxIpAddress < Number((node.data.ipAddress).split('.')[3])) {
                            maxIpAddress = Number((node.data.ipAddress).split('.')[3]);
                        }
                    }
                });
                this.ipCounter = maxIpAddress;
            };
            reader.readAsText(file);
        }
        this.diagramSubjectService.updateDiagrams(this.server.id.toString(), this.myDiagram);
    }

    removeDiagram() {
        this.notificationService.smartMessageBox(
            {
                title:
                '<i class=\'fa fa-sign-out txt-color-orangeDark\' style=\'font-family: \'IranSansNormal\' !important;font-size: 9px;\'></i> Remove Deployed Diagram From Current Server <span class=\'txt-color-orangeDark\'><strong>' +
                $('#show-shortcut').text() +
                '</strong></span> ?',
                content: '',
                buttons: '[No][Yes]'
            },
            ButtonPressed => {
                if (ButtonPressed == 'Yes') {
                    this.ng4LoadingSpinnerService.show();
                    this.tabSpinnerText = 'Removing Diagram From ' + this.server.name + ' ...';
                    this.networkDiagramService.removeDiagram('/deleteTopo', this.server.id).subscribe((res: any) => {
                        console.log('Topology Deleted');
                        this.ng4LoadingSpinnerService.hide();
                        this.tabSpinnerText = '';
                        this.notificationService.smallBox({
                            title: 'Topology Delete successfully',
                            content: '<i class=\'fa fa-clock-o\'></i> <i>2 seconds ago...</i>',
                            color: '#16A085',
                            iconSmall: 'fa fa-check bounce animated',
                            timeout: 4000
                        });
                    });
                }
            }
        );
    }

}
